const Service = require('egg').Service;

class CommonService extends Service {
    async checklogin(a, b) {
        return a && b ? a === b : false
    }
    async getAdmin() {
        const that = this
        let res = await this.ctx.service.sqlService.query('SELECT * FROM base WHERE admin = "admin" ')
        return {
            success: 1,
            ...res[0],
            openId: this.ctx.session.openId
        }
    }
    async commentfilter(comments) {
        const that = this
        let arr = []
        for (let item of comments) {
            const tmp = item
            if (item.disabled > 0) {
                tmp.comment = ""
            }
            arr.push(tmp)
        }
        return arr
    }
    async artWeighting(view_count, comment_count, user_plates, art_plates) {
        function toFixed2(n) {
            return Math.round(n * 100) / 100
        }
        //文章的热度积分和文章与用户关注的匹配度加权
        let t = 0
        let u_n = user_plates.length
        let a_n = art_plates.length
        if (u_n == a_n && u_n == 0) t = 1
        else { //杰卡德相似系数（Jaccard距离）
            /**
             * 
                p：样本A与B都是1的维度的个数
                q：样本A是1而B是0的维度的个数
                r：样本A是0而B是1的维度的个数
                s：样本A与B都是0的维度的个数 =0
             *  j = p/(p+q+r)
             * 
             */
            let m = Array.from(new Set([...user_plates, ...art_plates]))
            let u = m.map(item => { //user样本
                return user_plates.indexOf(item) > -1 ? 1 : 0
            })
            let a = m.map(item => { //art样本
                return art_plates.indexOf(item) > -1 ? 1 : 0
            })
            let len = m.length
            let p = 0
            let q = 0
            let r = 0
            for (let i = 0; i < len; i++) {
                if (u[i] == a[i]) {
                    if (u[i] == 1) p++
                } else {
                    if (u[i] == 1) q++
                    if (u[i] == 0) r++
                }
            }
            let Jaccard = toFixed2((p / (p + q + r))) //满分1分，两位小数
            t = Jaccard
        }
        //热度用y = 1-1/x 标准化
        let x = view_count + comment_count * 10
        let y = ((x == 0) ? 0 : toFixed2(1 - (1 / x)))
        return (y + t) //区间 [0,2)
    }
}

module.exports = CommonService;