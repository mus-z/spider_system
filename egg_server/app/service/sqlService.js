const Service = require('egg').Service;

class QueryService extends Service {
    async query(queryStr, args) {
        const result = await this.app.mysql.query(queryStr, args)
        return result;
    }
    async f5s7total(wheres) {
        const result = (await this.app.mysql.query("SELECT COUNT(*) as c FROM f5s7 WHERE 1 = 1 " + wheres))[0]['c']
        return result;
    }
    async listedtotal(wheres) {
        const result = (await this.app.mysql.query("SELECT COUNT(*) as c FROM listed WHERE 1 = 1 " + wheres))[0]['c']
        return result;
    }
    async listedbycode(code) {
        const result = (await this.app.mysql.query('SELECT stock_plate FROM plates WHERE stock_code = ' + code + ' AND plate_valid = 1'))
        return result.map(item => item.stock_plate);

    }
    async listedbyplate(stock_plate) {
        let result = (await this.app.mysql.query(`
        SELECT stock_code,company 
        FROM plates JOIN listed 
        WHERE stock_plate = "${stock_plate}"  
        AND plate_valid = 1 
        AND stock_code = code`))
        return result
    }
    async usersbyplate(stock_plate) {
        let result = (await this.app.mysql.query(`
        SELECT user_attention_plate_info.* FROM user_attention_plate_info join plate_info WHERE user_attention_plate_info.plate_info_id = plate_info.Id
        AND plate_name = "${stock_plate}"
        `))
        return result
    }
    async platesbyuser(user_id) {
        let result = (await this.app.mysql.query(`
        SELECT user_attention_plate_info.*,plate_info.plate_name FROM user_attention_plate_info join plate_info WHERE user_attention_plate_info.plate_info_id = plate_info.Id
        AND user_id = "${user_id}"
        `))
        return result
    }
    async viewsumbyartcode(art_code) {
        if (!Array.isArray(art_code)) {
            let viewSql = `SELECT SUM(view_count) as c from user_view_art WHERE art_code = "${art_code}" `
            let view = (await this.ctx.service.sqlService.query(viewSql))[0].c || 0
            return view
        } else {
            let viewSql = `SELECT ifnull(SUM(view_count),0) c,f5s7.art_code  
            from user_view_art RIGHT JOIN f5s7 ON user_view_art.art_code=f5s7.art_code 
            WHERE f5s7.art_code 
            IN ("${art_code.join('","')}") 
            GROUP BY f5s7.art_code 
            ORDER BY f5s7.art_code`
            let view = (await this.ctx.service.sqlService.query(viewSql))
            return view.map(item => item.c)
        }

    }
    async commentsumbyartcode(art_code) {
        if (!Array.isArray(art_code)) {
            let viewSql = `SELECT COUNT(*) as c from user_comments WHERE art_code = "${art_code}" `
            let view = (await this.ctx.service.sqlService.query(viewSql))[0].c || 0
            return view
        } else {

            let viewSql = `SELECT ifnull(COUNT(c),0) c,f5s7.art_code  
            from user_comments RIGHT JOIN f5s7 ON user_comments.art_code=f5s7.art_code 
            WHERE f5s7.art_code 
            IN ("${art_code.join('","')}") 
            GROUP BY f5s7.art_code 
            ORDER BY f5s7.art_code`
            let view = (await this.ctx.service.sqlService.query(viewSql))
            return view.map(item => item.c)
        }

    }
    async artbyplates(plate_names) {
        //通过版块名拿到板块下所有文章,先拿板块下所有公司,和其他特征，然后加权排序
        const plimit = require('p-limit')
        let limit = plimit(100)//限制mysql连接数避免太拥挤导致服务堵塞
        let arr = []
        let sql = `SELECT art_code,stock_code,title,plates FROM f5s7 JOIN listed ON f5s7.stock_code = listed.code
        WHERE stock_code IN 
        (SELECT DISTINCT (stock_code) FROM plates WHERE stock_plate IN ("${plate_names.join('","')}"))
        ORDER BY art_code `
        // console.log(sql)
        console.time("a")
        let art_codes = (await this.ctx.service.sqlService.query(sql)) //这些板块对应的所有文章编号
        let view_counts = await this.service.sqlService.viewsumbyartcode(art_codes.map(item => item.art_code))
        let comment_counts = await this.service.sqlService.commentsumbyartcode(art_codes.map(item => item.art_code))
        let plateslist = art_codes.map(item=>(item.plates).split(';'))
        console.timeEnd('a')
        let tmp = art_codes.map((item, i) => {
            return limit(async ()=>{
                const {
                    art_code,
                    stock_code,
                    title
                } = item
                const view_count = view_counts[i]
                const comment_count = comment_counts[i]
                // console.time('b1'+i)
                const plates = plateslist[i]
                // console.timeEnd('b1'+i)
                // console.time('b2'+i)
                const obj = {
                    art_code,
                    title,
                    view_count,
                    comment_count,
                    plates,
                    artWeighting: await this.service.common.artWeighting(view_count, comment_count, plate_names, plates),
                }
                // console.timeEnd('b2'+i)
                // console.log(obj)
                return obj
            })
        })
        console.time('b')
        arr = (await Promise.all(tmp))
        console.timeEnd('b')
        console.time('c')
        arr = arr.sort((a, b) => b.artWeighting - a.artWeighting)
        console.timeEnd('c')
        return arr
    }
}

module.exports = QueryService;