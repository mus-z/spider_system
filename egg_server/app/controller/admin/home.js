const {
    Controller
} = require('egg')
const request = require('superagent')
class AdminController extends Controller {
    async index() {
        // this.ctx.body = 'admin'
        // console.log(this.ctx.service.sqlService)
        const that = this
        let res = await this.ctx.service.sqlService.query('select * from base')
        this.app.events.emit('test', res) // 测试events注册
        let t
        // const p = require('../../utils/test')
        // await p.call(this)

        // const set_new_art = require('../../socket_utils/new_art').set_new_art
        // t=await set_new_art.call(that,{
        //     newarr: ['AN202104191486427397',"AN202104191486453515","AN202104191486433560","AN202104191486452799","AN202104191486454037","AN202104191486415117"],
        // })
        this.ctx.body = {
            res,
            t
        }
    }
    async changeTimer() { //管理员输入一个hours参数修改爬虫定时器，最低0.17(1min)
        const that = this
        //console.log(this.ctx.params.hours)
        let hours = +(this.ctx.params.hours)
        let msg = ''
        if (hours && !Number.isNaN(hours) && hours > 0) {
            clearInterval(this.app.timer)
            console.log('已清除之前定时')
            if (hours < 1 / 60) {
                hours = 1 / 60
                msg = '间隔最少大于1min，即0.017hours'
            }
            try {
                //更新成功，修改定时器
                let setTimer = require('../../utils/setTimer')
                await setTimer.call(that, hours)
                msg = '修改成功'
                this.ctx.body = {
                    success: 1,
                    msg
                }
            } catch (e) {
                msg = e
                this.ctx.body = {
                    success: 0,
                    msg
                }
            }

        } else {
            msg = '参数不合法，请输入正实数小时'
            this.ctx.body = {
                success: 0,
                msg
            }
        }

    }
    async switchTimer() { //管理员开关定时器
        const that = this
        //console.log(this.ctx.params.hours)
        let state = +(this.ctx.params.state)
        let msg = ''
        //分情况 开-开 开-关 关-开 关-关 （base-state）
        //1开-开则提示已开启
        //2开-关直接关闭
        //3关-开则用上次用的时间开启
        //4关-关则提示已关闭
        try {
            let base = await this.ctx.service.sqlService.query('SELECT timer_state From base WHERE admin = "admin"')
            if (base.length != 1) throw Error('用户检索错误')
            let b_state = +(base[0].timer_state)
            // console.log(b_state, state) //新状态，旧状态
            if (b_state == state) { //1、4
                msg = '不能重复' + (b_state ? '开启' : '关闭') + '爬虫'
                this.ctx.body = {
                    success: 0,
                    msg,
                }
            } else {
                if (state) { //3
                    msg = '开启成功'
                    let setTimer = require('../../utils/setTimer')
                    await setTimer.call(that, null, 1)
                    this.ctx.body = {
                        success: 1,
                        msg,
                    }
                } else { //2
                    msg = '关闭成功'
                    let setTimer = require('../../utils/setTimer')
                    await setTimer.call(that, null, 0)
                    this.ctx.body = {
                        success: 1,
                        msg,
                    }
                }

            }

        } catch (e) {
            console.log(e)
            msg = e
            this.ctx.body = {
                success: 0,
                msg
            }
        }
    }
    async spiderOnce() { //一次爬虫
        const that = this
        let msg = ''
        let getList = require('../../utils/getList')
        try {
            let arr = await getList.call(that)
            //console.log(arr)
            let res = await this.ctx.service.common.getAdmin()
            this.ctx.body = {
                success: 1,
                msg,
                arr,
                admin: res
            }
        } catch (e) {
            msg = e
            this.ctx.body = {
                success: 0,
                msg
            }
        }
    }
    async adminLogin() { //admin登陆
        const that = this
        let body = this.ctx.request.body;
        let {
            admin,
            password
        } = body
        let res = await this.app.mysql.query('SELECT * FROM base WHERE admin = ? AND password = ? ', [admin, password])

        if (res.length == 1) {
            //登陆通过
            this.ctx.session.openId = new Date().valueOf() + 'admin'
            this.ctx.body = {
                success: 1,
                msg: '登陆成功',
                openId: this.ctx.session.openId
            }
        } else {
            this.ctx.session.openId = null
            this.ctx.body = {
                success: 0,
                msg: '密码错误'
            }
        }
    }
    async adminLogout() { //admin登出

        this.ctx.session.openId = null
        this.ctx.body = {
            success: 1,
            msg: '登出成功'
        }
    }
    async adminLogCheck() { //admin检查
        let result = await this.service.common.checklogin(this.ctx.request.body.openId, this.ctx.session.openId)
        let body = this.ctx.request.body;
        let openId = body.openId;
        //console.log(result,this.ctx.request.body.openId,this.ctx.session.openId)
        if (!result) {
            this.ctx.body = {
                success: 0,
                msg: 'openid无效'
            }
        } else {
            this.ctx.body = {
                success: 1,
                openId,
                msg: ''
            }
        }
    }
    async adminQuery() { //拿到管理员
        const that = this
        let res = await this.ctx.service.common.getAdmin()
        this.ctx.body = res
    }
    async adminUpdate() { //管理员修改状态
        const that = this
        let body = this.ctx.request.body;
        let result = await this.service.common.checklogin(this.ctx.request.body.openId, this.ctx.session.openId)
        // console.log({
        //     body,
        //     result
        // })
        if (result) {
            //openid验证成功
            let res = await this.app.mysql.update('base', body.options)
            // console.log(res)
            if (res.affectedRows == 1) {
                //修改成功
                let admin = (await this.service.common.getAdmin())
                this.ctx.body = {
                    success: 1,
                    admin,
                    msg: '修改成功'
                }
                let setTimer = require('../../utils/setTimer')
                setTimer.call(that)
            } else {
                this.ctx.body = {
                    success: 0,
                    msg: '修改失败，请检查adminUpdate'
                }
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: 'openid无效'
            }
        }
    }
    async adminGetList() {
        //拿到的是文章列表
        //拿到列表,按art_code倒序算的
        //为了保证数据的时效性写的分页，因为每页都要请求，可能会比较卡顿
        //body拿到请求数据，option则是where检索的状态数据
        const that = this
        let body = this.ctx.request.body;
        let {
            option
        } = body
        let page = body.page
        let pagesize = body.pagesize
        let sqlString = `SELECT art_code,title,short_name,notice_date,crawl_time,Id,stock_code From f5s7 WHERE 1 = 1 `
        let wheres = ' '
        if (option['searchValue']) {
            //模糊匹配
            const value = option['searchValue']
            wheres += `AND (title LIKE '%${value}%' OR art_code LIKE '%${value}%' OR short_name LIKE '%${value}%' OR stock_code LIKE '%${value}%') `
        }
        if (option['dateRange']) {
            //日期范围
            const [pre, next] = option['dateRange']
            // console.log({pre,next})
            wheres += `AND notice_date >= ${pre} AND  notice_date <= ${next} `
        }
        if (option['stock']) {
            //公司页的属性筛选
            let stock = option['stock']
            wheres += `AND stock_code = ${stock} `
        }
        sqlString += wheres
        sqlString += `ORDER BY art_code desc LIMIT ${page*pagesize},${pagesize};`
        let total = await this.ctx.service.sqlService.f5s7total(wheres)
        let pageMax = Math.ceil((total) / pagesize)
        let res = await this.ctx.service.sqlService.query(sqlString)
        // console.log({body,option,wheres})
        this.ctx.body = {
            success: 1,
            arr: res,
            page,
            pagesize,
            pageMax,
            total,
            md5: that.app.md5
        }
    }
    async adminGetCompanyList() {
        //获取公司列表
        const that = this
        let body = this.ctx.request.body;
        let {
            option
        } = body
        let page = body.page
        let pagesize = body.pagesize
        let sqlString = `SELECT Id,code,company,type From listed WHERE 1 = 1 `
        let wheres = ' '
        if (option['searchValue']) {
            //模糊匹配
            const value = option['searchValue']
            wheres += `AND (code LIKE '%${value}%' OR company LIKE '%${value}%' OR type LIKE '%${value}%' OR code IN (SELECT stock_code FROM plates WHERE stock_plate LIKE '%${value}%' ) ) `
        }
        if (option['plateId']) {
            wheres += `AND code IN (SELECT stock_code FROM (SELECT plate_name FROM plate_info WHERE Id = '${option['plateId']}' )a ,plates WHERE a.plate_name = plates.stock_plate) `
        }
        sqlString += wheres + ` LIMIT ${page*pagesize},${pagesize};`
        let total = await this.ctx.service.sqlService.listedtotal(wheres)
        let pageMax = Math.ceil((total) / pagesize)
        let res = await this.ctx.service.sqlService.query(sqlString)
        let tmp = []
        let findplates_promise = (res) => {
            return res.map(item => {
                return new Promise(async (resolve) => {
                    let plates = await this.ctx.service.sqlService.listedbycode(item.code)
                    resolve({
                        ...item,
                        plates,
                    })
                })
            })
        }
        tmp = await Promise.all(findplates_promise(res))
        // console.log({
        //     tmp,
        //     body,
        //     option,
        //     wheres
        // })
        this.ctx.body = {
            success: 1,
            arr: tmp,
            page,
            pagesize,
            pageMax,
            total,
            md5: that.app.md5
        }
    }
    async adminCheckmd5() {
        const that = this
        this.ctx.body = {
            success: 1,
            md5: that.app.md5
        }
    }
    async admingetcomponybystock() {
        const that = this
        const stock = this.ctx.params.stock //6位公司上市码
        let sqlString = `SELECT * FROM listed WHERE code = ${stock} ; `
        let res = await this.ctx.service.sqlService.query(sqlString)
        if (res.length == 1) {
            const listed = res[0]
            this.ctx.body = {
                success: 1,
                listed,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没查到'
            }
        }
    }
    async admingetplatesbystock() {
        const that = this
        const stock = this.ctx.params.stock //6位公司上市码
        let sqlString = `SELECT * FROM plates WHERE stock_code = ${stock} AND plate_valid = 1 ; `
        let res = await this.ctx.service.sqlService.query(sqlString)
        this.ctx.body = {
            success: 1,
            plates: res
        }
    }
    async adminaddoneplate() {
        const that = this
        let body = this.ctx.request.body;
        let {
            value,
            stockcode,
        } = body
        let resofinfo = await that.app.mysql.get('plate_info', {
            plate_name: value
        })
        if (!resofinfo) {
            try {
                resofinfo = await that.app.mysql.insert('plate_info', {
                    plate_name: value
                })
            } catch (e) {
                console.log(e)
            }
        }
        let result = await this.app.mysql.get('plates', {
            stock_code: stockcode,
            stock_plate: value,
        })
        let res
        if (result) {
            //更新被删过的
            res = await this.app.mysql.update('plates', {
                id: result.Id,
                stock_code: stockcode,
                stock_plate: value,
                plate_valid: 1,
            })
        } else {
            //插入新的
            try {
                res = await this.app.mysql.insert('plates', {
                    stock_code: stockcode,
                    stock_plate: value,
                })
            } catch (e) {
                console.log(e)
            }

        }
        let Id = result ? result.Id : res.insertId
        // console.log({
        //     Id,
        //     result,
        //     res
        // })
        if (res.affectedRows == 1) {
            //增加或更新成功
            this.ctx.body = {
                success: 1,
                Id,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: 'err',
                res,
            }
        }
    }
    async admindeloneplate() {
        let body = this.ctx.request.body;
        const {
            Id: id
        } = body
        let res = await this.app.mysql.update('plates', {
            id,
            plate_valid: 0,
        })
        // console.log(res)
        if (res.affectedRows == 1) {
            this.ctx.body = {
                success: 1,
                Id: id,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: 'err',
                res,
            }
        }
    }
    async admingetarticlebycode() {
        const that = this
        const artcode = this.ctx.params.artcode
        let sqlString = `SELECT * FROM f5s7 WHERE art_code = '${artcode}' ; `
        let res = await this.ctx.service.sqlService.query(sqlString)
        let view = await this.service.sqlService.viewsumbyartcode(artcode)
        if (res.length == 1) {
            const art = res[0]
            this.ctx.body = {
                success: 1,
                art,
                view
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没查到'
            }
        }
    }
    async admingetallplates() {
        let sqlstr = `SELECT * FROM plate_info WHERE 1=1 `
        const {
            plateId,
            userId,
        } = this.ctx.query
        if (plateId) {
            sqlstr += `AND Id = '${plateId}' `
        }
        if (userId) {
            sqlstr += `AND Id IN (SELECT plate_info_id FROM user_attention_plate_info WHERE user_id = "${userId}" ) `
        }
        let res = await this.ctx.service.sqlService.query(sqlstr)
        this.ctx.body = {
            success: 1,
            plates: res
        }
    }
    async admingetallusers() {
        let sqlstr = `SELECT Id,user_name,user_email,user_nickname,user_creattime FROM users`
        let res = await this.ctx.service.sqlService.query(sqlstr)
        this.ctx.body = {
            success: 1,
            users: res,
        }
    }
    async admingetuserbyid() {
        let query = this.ctx.query
        const {
            Id
        } = query
        let res = await this.app.mysql.get("users", {
            Id
        })
        this.ctx.body = {
            success: 1,
            user: res,
        }
    }
    async usergetviewarthistory() {
        const that = this
        let query = this.ctx.query
        const {
            user_id,
        } = query
        let sql = `SELECT a.art_code,a.last_view_time,f5s7.title,f5s7.short_name,f5s7.stock_code,f5s7.notice_date,f5s7.url 
        FROM (SELECT art_code,user_id,max(last_view_time) as last_view_time  FROM user_view_art GROUP BY art_code,user_id )a
		  join f5s7 ON a.art_code = f5s7.art_code 
		  WHERE user_id = "${user_id}" 
        ORDER BY  last_view_time desc
        `
        let res = await this.service.sqlService.query(sql)
        this.ctx.body = {
            success: 1,
            views: res,
        }
        // console.log(res)
    }
    async admingetartcommentbycode() {
        const that = this
        let query = this.ctx.query
        const {
            art_code,
        } = query
        if (art_code) {
            let sqlStr = `SELECT a.*,b.user_nickname as to_name FROM 
            (SELECT u_c.*,u.user_nickname,u.user_avator FROM user_comments u_c JOIN users u ON  u_c.user_id=u.Id)a 
            LEFT JOIN users b ON a.to_id=b.Id WHERE art_code = ?  `
            let res = await that.service.sqlService.query(sqlStr, [art_code])
            this.ctx.body = {
                success: 1,
                comments: (res),
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '无传参'
            }
        }
    }
    async admindeletecomment() {
        const that = this
        let query = this.ctx.query
        const {
            comment_id,
            user_id,
            comment_u_id,
            art_code
        } = query
        if (user_id !== comment_u_id && user_id != "admin") {
            this.ctx.body = {
                success: 0,
                msg: '不是你的不能删除哦~'
            }
        } else {
            let result = await that.app.mysql.update("user_comments", {
                id: comment_id,
                disabled: 2,
            })
            if (result.affectedRows == 1) {
                let sqlStr = `SELECT a.*,b.user_nickname as to_name FROM 
                (SELECT u_c.*,u.user_nickname,u.user_avator FROM user_comments u_c JOIN users u ON  u_c.user_id=u.Id)a 
                LEFT JOIN users b ON a.to_id=b.Id WHERE art_code = ?  `
                let res = await that.service.sqlService.query(sqlStr, [art_code])
                this.ctx.body = {
                    success: 1,
                    comments: (res),
                }
            } else {
                this.ctx.body = {
                    success: 0,
                    msg: '删除失败'
                }
            }
        }
    }
    async admingetdata() {

        const that = this
        let query = this.ctx.query
        async function art_time_date() {
            //1.近期公告统计，按爬虫日期切分
            let sql = `
            SELECT COUNT(*) c,FROM_UNIXTIME(substring(crawl_time,1,10),'%Y-%m-%d') crawl_date FROM f5s7 
            GROUP BY FROM_UNIXTIME(substring(crawl_time,1,10),'%Y-%m-%d') 
            ORDER BY crawl_date `
            let res = await that.service.sqlService.query(sql)
            let arr = []
            res.reduce((pre, next) => {
                let obj = {
                    ...next,
                    c: next.c + pre.c,
                    diff: next.c
                }
                arr.push(obj)
                return obj
            }, {
                c: 0
            })
            return arr
        }
        async function user_time_date() {
            //2.用户注册，按注册日期切分
            let sql = `
            SELECT COUNT(*) c,FROM_UNIXTIME(substring(user_creattime,1,10),'%Y-%m-%d') creat_date FROM users 
            GROUP BY FROM_UNIXTIME(substring(user_creattime,1,10),'%Y-%m-%d') 
            ORDER BY creat_date `
            let res = await that.service.sqlService.query(sql)
            let arr = []
            res.reduce((pre, next) => {
                let obj = {
                    ...next,
                    c: next.c + pre.c,
                    diff: next.c
                }
                arr.push(obj)
                return obj
            }, {
                c: 0
            })
            return arr
        }
        async function user_plates() {
            //3.板块热度和板块总数
            let sql = `SELECT user_count as value,plate_name as name FROM plate_info ORDER BY value DESC LIMIT 0,20`
            let res = await that.service.sqlService.query(sql)
            return res
        }
        async function company_plates() {
            //4.板块下公司数
            let sql = ` SELECT company_count as value,plate_name as name FROM plate_info ORDER BY value DESC LIMIT 0,20`
            let res = await that.service.sqlService.query(sql)
            return res
        }
        async function user_activity() {
            //5.用户浏览和评论
            let sql = `SELECT a.view_date,IFNULL(a.view_c,0) view_c,IFNULL(b.comment_c,0) comment_c FROM 
            (SELECT FROM_UNIXTIME(substring(last_view_time,1,10),'%Y-%m-%d') view_date, COUNT(*) view_c  FROM user_view_art
            GROUP BY FROM_UNIXTIME(substring(last_view_time,1,10),'%Y-%m-%d'))a LEFT JOIN
            (SELECT FROM_UNIXTIME(substring(comment_time,1,10),'%Y-%m-%d') comment_date,COUNT(*) comment_c  FROM user_comments
            GROUP BY FROM_UNIXTIME(substring(comment_time,1,10),'%Y-%m-%d'))b
            ON  a.view_date = b.comment_date
            ORDER BY view_date DESC LIMIT 0, 7`
            let res = (await that.service.sqlService.query(sql)).reverse()
            return res
        }
        let art_time_date_data = await art_time_date()
        let user_time_date_data = await user_time_date()
        let user_plates_data = await user_plates()
        let company_plates_data = await company_plates()
        let plate_count = (await that.service.sqlService.query(`SELECT COUNT(*) as c FROM plate_info`))[0].c
        let stock_count = (await that.service.sqlService.query(`SELECT COUNT(*) as c FROM listed`))[0].c
        let view_count = (await that.service.sqlService.query(`SELECT COUNT(*) as c FROM user_view_art`))[0].c
        let comment_count = (await that.service.sqlService.query(`SELECT COUNT(*) as c FROM user_comments`))[0].c
        let user_activity_data = await user_activity()
        this.ctx.body = {
            success: 1,
            plate_count,
            stock_count,
            view_count,
            comment_count,
            art_time_date_data,
            user_time_date_data,
            user_plates_data,
            company_plates_data,
            user_activity_data,
        }
    }
}
module.exports = AdminController;