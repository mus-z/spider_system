const {
    Controller
} = require('egg')
class VisitorController extends Controller {
    async index() {
        this.ctx.body = 'visitor'
    }
    async userRegister() { //用户注册
        let body = this.ctx.request.body;
        let label = ''
        let result = await this.app.mysql.get('users', {
            user_name: body.user_name
        })
        if (result) label += 'user_name '
        result = await this.app.mysql.get('users', {
            user_email: body.user_email
        })
        if (result) label += 'user_email '
        if (label) {
            this.ctx.body = {
                success: 0,
                msg: '重复的' + label
            }
        } else {
            let res = await this.app.mysql.insert('users', body)
            if (res.affectedRows == 1) {
                this.ctx.body = {
                    success: 1,
                    data: body,
                }
            } else {
                this.ctx.body = {
                    success: 0,
                    msg: res
                }
            }
        }
    }
    async userLogin() { //用户登录
        let body = this.ctx.request.body;
        let res = await this.app.mysql.get('users', body)
        // console.log(res)
        if (res) {
            this.ctx.session.userId = res.Id
            this.ctx.body = {
                success: 1,
                data: res,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '用户名或密码错误'
            }
        }
    }
    async userCkLogin() {
        // console.log(this.ctx.session.user)
        //cookie不区分端口号不能和admin一样用openId
        let userId = this.ctx.session.userId

        if (userId) {
            let res = await this.app.mysql.get('users', {
                id: userId
            })
            this.ctx.body = {
                success: 1,
                data: res,
            }
        } else {
            this.ctx.body = {
                success: 0,
            }
        }
    }
    async userLogout() {
        this.ctx.session.userId = null
        this.ctx.body = {
            success: 1,
        }
    }
    async userUpdate() {
        let body = this.ctx.request.body;
        let res = await this.app.mysql.update('users', body)
        // console.log(body, res)
        if (res.affectedRows == 1) {
            this.ctx.body = {
                success: 1,
                data: body,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: res,
            }
        }
    }
    async usergetArtList() {
        const pagesize = 5
        let query = this.ctx.query
        const {
            last_tail_code,
            searchStr,
            stockcode,
            platename
        } = query
        let stockcodeQuery = ''
        let platenameQuery = ''
        if (stockcode) { //公司页的列表携带的参数
            stockcodeQuery = `AND stock_code = ${stockcode} `
        }
        if (platename) { //板块页携带的参数
            platenameQuery = `AND stock_code IN (SELECT stock_code FROM plates WHERE stock_plate = "${platename}" ) `
        }
        let last_crawler_time = (await this.ctx.service.sqlService.query('select last_crawler_time from base'))[0].last_crawler_time
        // console.log(query)
        let n = 0
        if (last_tail_code) {
            let sqlforn = `SELECT r.n from(SELECT ROW_NUMBER() over(ORDER BY art_code desc) AS n,art_code FROM f5s7 WHERE 1=1 `
            if (searchStr) {
                let value = searchStr
                sqlforn += `AND (title LIKE '%${value}%' OR short_name LIKE '%${value}%' OR stock_code LIKE '%${value}%') `
            }
            sqlforn += stockcodeQuery + platenameQuery
            sqlforn += `) r WHERE r.art_code = "${last_tail_code}";`
            let result = await this.service.sqlService.query(sqlforn)
            n = result[0].n || 0 //拿到上次列表的最后一个的排序后行号
        }
        let sqlString = `SELECT Id AS id,art_code,title,short_name,notice_date,crawl_time,stock_code from
        (SELECT ROW_NUMBER() over(ORDER BY art_code desc) as n,art_code as ac from f5s7 WHERE 1=1 `
        if (searchStr) {
            let value = searchStr
            sqlString += `AND (title LIKE '%${value}%' OR short_name LIKE '%${value}%' OR stock_code LIKE '%${value}%') `
        }
        sqlString += stockcodeQuery + platenameQuery
        sqlString += `)AS r,f5s7
        WHERE r.n>${n} AND r.ac = art_code LIMIT 0,${pagesize}  `
        let res = await this.service.sqlService.query(sqlString, )
        if (res.length > 0) {
            let len = res.length
            let tail_code = res[len - 1].art_code
            this.ctx.body = {
                success: 1,
                listdata: {
                    last_tail_code: query.last_tail_code,
                    list: res,
                    tail_code,
                    time: new Date().valueOf(),
                    query,
                    last_crawler_time,
                }
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没有查询到新的数据',
                listdata: {
                    last_tail_code: query.last_tail_code,
                    list: [],
                    tail_code: query.last_tail_code,
                    time: new Date().valueOf(),
                    query,
                    last_crawler_time,
                }
            }
        }
    }
    async usergetcomponybystock() {
        let query = this.ctx.query
        const {
            stock_code
        } = query
        let sqlString = `SELECT * FROM listed WHERE code = "${stock_code}" ; `
        let res = await this.ctx.service.sqlService.query(sqlString)

        let sqlString2 = `SELECT plates.*,plate_info.Id AS p_id FROM plates JOIN plate_info WHERE stock_code = "${stock_code}" AND plate_valid = 1 AND stock_plate = plate_name`
        let res2 = await this.ctx.service.sqlService.query(sqlString2)
        if (res.length == 1) {
            const plates = res2.map(item => ({
                Id: item.Id,
                stock_plate: item.stock_plate,
                p_id: item.p_id
            }))
            const listed = {
                ...res[0],
                plates
            }
            this.ctx.body = {
                success: 1,
                listed,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没查到'
            }
        }
    }
    async usergetarticlebystock() {
        let query = this.ctx.query
        const {
            art_code
        } = query
        let sqlString = `SELECT * FROM f5s7 WHERE art_code = "${art_code}" ; `
        let res = await this.ctx.service.sqlService.query(sqlString)
        let view = await this.service.sqlService.viewsumbyartcode(art_code)
        // console.log(view)
        if (res.length == 1) {
            this.ctx.body = {
                success: 1,
                art: res[0],
                view
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没查到'
            }
        }
    }
    async usergetcompanylist() {
        //获取公司列表
        const that = this
        let body = this.ctx.request.body;
        let {
            option
        } = body
        let page = body.page
        let pagesize = body.pagesize
        let sqlString = `SELECT Id,code,company,type From listed WHERE 1 = 1 `
        let wheres = ' '
        if (option['searchValue']) {
            //模糊匹配
            const value = option['searchValue']
            wheres += `AND (code LIKE '%${value}%' OR company LIKE '%${value}%' OR type LIKE '%${value}%' OR code IN (SELECT stock_code FROM plates WHERE stock_plate LIKE '%${value}%' ) ) `
        }
        if (option['plateId']) {
            wheres += `AND code IN (SELECT stock_code FROM (SELECT plate_name FROM plate_info WHERE Id = '${option['plateId']}' )a ,plates WHERE a.plate_name = plates.stock_plate) `
        }
        sqlString += wheres + ` LIMIT ${page*pagesize},${pagesize};`
        let total = await this.ctx.service.sqlService.listedtotal(wheres)
        let pageMax = Math.ceil((total) / pagesize)
        let res = await this.ctx.service.sqlService.query(sqlString)
        let tmp = []
        let findplates_promise = (res) => {
            return res.map(item => {
                return new Promise(async (resolve) => {
                    let plates = await this.ctx.service.sqlService.listedbycode(item.code)
                    resolve({
                        ...item,
                        plates,
                    })
                })
            })
        }
        tmp = await Promise.all(findplates_promise(res))
        // console.log({
        //     tmp,
        //     body,
        //     option,
        //     wheres
        // })
        this.ctx.body = {
            success: 1,
            arr: tmp,
            page,
            pagesize,
            pageMax,
            total,
            md5: that.app.md5
        }
    }
    async usergetPlateList() {
        const that = this
        async function getTmp(res) {
            let tmp = []
            let findlisted_promise = (res) => {
                return res.map(item => {
                    return new Promise(async (resolve) => {
                        let listed = await that.ctx.service.sqlService.listedbyplate(item.plate_name)
                        let attentions = await that.ctx.service.sqlService.usersbyplate(item.plate_name)
                        resolve({
                            ...item,
                            listed,
                            attentions_count: attentions.length || 0,
                        })
                    })
                })
            }
            tmp = await Promise.all(findlisted_promise(res))
            return tmp
        }
        const pagesize = 12
        let query = this.ctx.query
        const {
            last_tail_id,
            searchStr,
            user_id,
            orderValue,
        } = query
        // console.log(query)
        let orderBY = `ORDER BY ${orderValue} desc`
        let n = 0
        let userQuery = ""
        if (user_id) {
            userQuery = `AND Id IN (SELECT plate_info_id from user_attention_plate_info WHERE user_id = "${user_id}") `
        }
        if (last_tail_id) {
            let sqlforn = `SELECT r.n from(SELECT ROW_NUMBER() over(${orderBY}) AS n,Id FROM plate_info WHERE 1=1 `
            if (searchStr) {
                let value = searchStr
                sqlforn += `AND (plate_name LIKE '%${value}%' ) `
            }
            sqlforn += `) r WHERE r.Id = "${last_tail_id}";`
            let result = await this.service.sqlService.query(sqlforn)
            n = result[0].n || 0 //拿到上次列表的最后一个的排序后行号
        }
        let sqlString = `SELECT Id AS id,plate_name FROM 
        (SELECT ROW_NUMBER() over(${orderBY}) as n,Id as _id FROM plate_info WHERE 1=1 `
        if (searchStr) {
            let value = searchStr
            sqlString += `AND (plate_name LIKE '%${value}%' ) `
        }
        sqlString += userQuery
        sqlString += `)AS r,plate_info `
        let total = ((await this.service.sqlService.query(sqlString + 'WHERE r._id = id', )).length)
        sqlString += `WHERE r.n>${n} AND r._id = id LIMIT 0,${pagesize}  `
        let res = await this.service.sqlService.query(sqlString, )
        if (res.length > 0) {
            let len = res.length
            let tail_id = res[len - 1].id
            let tmp = await getTmp(res)
            this.ctx.body = {
                success: 1,
                listdata: {
                    last_tail_code: last_tail_id,
                    list: tmp,
                    tail_id,
                    query,

                },
                total,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '没有查询到新的数据',
                listdata: {
                    last_tail_id: last_tail_id,
                    list: [],
                    tail_id: null,
                    query,
                }
            }
        }
    }
    async usergetPlateInfo() {
        const that = this
        let query = this.ctx.query
        const {
            plate_Id
        } = query
        let sqlString = `SELECT * FROM plate_info WHERE Id = "${plate_Id}" ;`
        let res = (await this.service.sqlService.query(sqlString))[0]
        // console.log(query,res)
        if (res) {
            this.ctx.body = {
                success: 1,
                plateinfo: res,
            }
        } else {
            this.ctx.body = {
                success: 0,
            }
        }
    }
    async usergetattentionplatesbyid() {
        const that = this
        let query = this.ctx.query
        const {
            user_id
        } = query
        let attentions = await that.ctx.service.sqlService.platesbyuser(user_id)
        this.ctx.body = {
            success: 1,
            attentions
        }
    }
    async userattentionplate() {
        const that = this
        let query = this.ctx.query
        const {
            type, //0取关，1关注
            user_id,
            plate_info_id,
            attention_time,
        } = query
        let res
        if (type == 1) {
            await this.app.mysql.delete('user_attention_plate_info', {
                user_id,
                plate_info_id,
                // attention_time,
            })
            res = await this.app.mysql.insert('user_attention_plate_info', {
                user_id,
                plate_info_id,
                attention_time,
            })
        } else {
            res = await this.app.mysql.delete('user_attention_plate_info', {
                user_id,
                plate_info_id,
                // attention_time,
            })
        }
        if (res.affectedRows >= 1) {
            this.ctx.body = {
                success: 1,
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: res + '',
            }
        }
    }
    async usergetplateattention() {
        const that = this
        let query = this.ctx.query
        const {
            plate_name
        } = query
        let n = (await that.ctx.service.sqlService.usersbyplate(plate_name)).length || 0
        this.ctx.body = {
            success: 1,
            plate_name,
            n
        }
    }
    async userviewart() {
        const that = this
        let query = this.ctx.query
        const {
            user_id,
            art_code,
            view_time,
        } = query
        if (user_id && art_code) {
            let sql = `INSERT INTO user_view_art(user_id,art_code,view_count,last_view_time) 
            VALUES("${user_id}","${art_code}",1,"${view_time}") ON DUPLICATE KEY UPDATE 
            view_count=view_count+1,last_view_time="${view_time}"
            `
            let result = await this.service.sqlService.query(sql)
            //通过浏览取消pushed中的状态
            await this.service.sqlService.query(`DELETE FROM user_wait_push WHERE user_id = "${user_id}" AND art_code = "${art_code}" `)
            // console.log(result, sql)
            this.ctx.body = {
                success: 1,
                result
            }

        } else {
            this.ctx.body = {
                success: 0,
                msg: '参数不全'
            }
        }
    }
    async usergetviewarthistory() {
        const that = this
        let query = this.ctx.query
        const {
            user_id,
        } = query
        let sql = `SELECT a.art_code,a.last_view_time,f5s7.title,f5s7.short_name,f5s7.stock_code,f5s7.notice_date,f5s7.url 
        FROM (SELECT art_code,user_id,max(last_view_time) as last_view_time  FROM user_view_art GROUP BY art_code,user_id )a
		  join f5s7 ON a.art_code = f5s7.art_code 
		  WHERE user_id = "${user_id}" 
        ORDER BY  last_view_time desc
        `
        let res = await this.service.sqlService.query(sql)
        this.ctx.body = {
            success: 1,
            views: res,
        }
        // console.log(res)
    }
    async usergetartcommentbycode() {
        const that = this
        let query = this.ctx.query
        const {
            art_code,
        } = query
        if (art_code) {
            let sqlStr = `SELECT a.*,b.user_nickname as to_name FROM 
            (SELECT u_c.*,u.user_nickname,u.user_avator FROM user_comments u_c JOIN users u ON  u_c.user_id=u.Id)a 
            LEFT JOIN users b ON a.to_id=b.Id WHERE art_code = ?  `
            let res = await that.service.sqlService.query(sqlStr, [art_code])
            this.ctx.body = {
                success: 1,
                comments: await that.service.common.commentfilter(res),
            }
        } else {
            this.ctx.body = {
                success: 0,
                msg: '无传参'
            }
        }
    }
    async usermakecomment() {
        const that = this
        let body = this.ctx.request.body
        let result = await that.app.mysql.insert("user_comments", body)
        if (result.affectedRows == 1) {
            let sqlStr = `SELECT a.*,b.user_nickname as to_name FROM 
            (SELECT u_c.*,u.user_nickname,u.user_avator FROM user_comments u_c JOIN users u ON  u_c.user_id=u.Id)a 
            LEFT JOIN users b ON a.to_id=b.Id WHERE art_code = ?  `
            let res = await that.service.sqlService.query(sqlStr, [body.art_code])
            this.ctx.body = {
                success: 1,
                comments: await that.service.common.commentfilter(res),
            }
            // console.log(result, res)
        } else {
            this.ctx.body = {
                success: 0,
            }
        }

    }
    async userdeletecomment() {
        const that = this
        let query = this.ctx.query
        const {
            comment_id,
            user_id,
            comment_u_id,
            art_code
        } = query
        if (user_id !== comment_u_id) {
            this.ctx.body = {
                success: 0,
                msg: '不是你的不能删除哦~'
            }
        } else {
            let result = await that.app.mysql.update("user_comments", {
                id: comment_id,
                disabled: 1,
            })
            if (result.affectedRows == 1) {
                let sqlStr = `SELECT a.*,b.user_nickname as to_name FROM 
                (SELECT u_c.*,u.user_nickname,u.user_avator FROM user_comments u_c JOIN users u ON  u_c.user_id=u.Id)a 
                LEFT JOIN users b ON a.to_id=b.Id WHERE art_code = ?  `
                let res = await that.service.sqlService.query(sqlStr, [art_code])
                this.ctx.body = {
                    success: 1,
                    comments: await that.service.common.commentfilter(res),
                }
            } else {
                this.ctx.body = {
                    success: 0,
                    msg: '删除失败'
                }
            }
        }
    }
    async usersetpushlistviewed() {
        const that = this
        let body = this.ctx.request.body
        const {
            push_id_list,
            user_id
        } = body
        let tmp = push_id_list.map(async (id) => {
            return await this.app.mysql.delete("user_wait_push", {
                id,
            })
        })
        let res = await Promise.all(tmp)
        let sql = `SELECT user_wait_push.*,f5s7.short_name,f5s7.stock_code,f5s7.notice_date,f5s7.title,f5s7.crawl_time 
        FROM user_wait_push JOIN f5s7 
        ON user_wait_push.art_code = f5s7.art_code 
        WHERE f5s7.valid = 1 
        AND user_wait_push.user_id = "${user_id}" ;`
        let arr = (await that.app.mysql.query(sql))
        this.ctx.body = {
            success: 1,
            res,
            pushlist: arr,
        }
    }
    async getrecommendartbyuserid() {
        //用户主动获取推荐
        const that = this
        let query = this.ctx.query
        const {
            user_id,
            start_time,
        } = query
        //1.获取用户所有关注板块
        /**
         * [RowDataPacket {
                Id: 24,
                user_id: 1,
                plate_info_id: 41,
                attention_time: '1619004561769',
                plate_name: '国企改革',
                weighted_art:[]
            },...]
         */
        let plates = await this.service.sqlService.platesbyuser(user_id)
        let user_p = plates.map(item => item.plate_name) //用户关注版块
        let arts = await this.service.sqlService.artbyplates(user_p)
        let tmp = arts.slice(0, 10)
        let len = tmp.length
        let radom = Math.floor(Math.random() * len)
        if (arts.length) {
            this.ctx.body = {
                success: 1,
                art: tmp[radom],
                start_time,
            }
        }else{
            if(user_p.length){
                this.ctx.body = {
                    success: 0,
                    art: tmp[radom],
                    start_time,
                    msg:'错误x'
                }
            }
            else{
                this.ctx.body = {
                    success: 0,
                    art: tmp[radom],
                    start_time,
                    msg:'再去关注点板块吧'
                }
            }

        }

    }
}
module.exports = VisitorController;