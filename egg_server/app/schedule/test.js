//https://eggjs.org/zh-cn/basics/schedule.html
module.exports = {
  schedule: {
    interval: 1000, //每小时的定时任务
    type: 'all', // 指定所有的 worker 都需要执行
    disable: true,//废除这个定时，因为想操控interval
  },
  async task(ctx) { //测试定时任务
    let fn = require('../utils/getList') //引用utils
    const that = ctx
    console.log('schedule')
    //await fn.call(that)
  },
};