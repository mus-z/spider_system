const request = require("superagent")
require('superagent-proxy')(request)
//require('superagent-charset')(request)
const FUA = require('fake-useragent')
const cheerio = require('cheerio') //html操作会用到，或者写正则代替
//初始化一个默认cookie，但是程序运行还是会重新获取的，东方财富对cookie的限制不是很严重
let cookie = 'intellpositionL=1215.35px; intellpositionT=455px; dRecords=%u516C%u544A%u5927%u5168-%u5C71%u4E1C%u9EC4%u91D1%7Chttp%3A//data.eastmoney.com/notices/stock/600547/html%2C*%u6CAA%u6DF1A%u80A1%u516C%u544A%7Chttp%3A//data.eastmoney.com/notices/hsa/5.html; cowCookie=true; qgqp_b_id=0f18b42a64b6c4bd82f2faad9f01a459; st_si=17209978998651; st_pvi=51205202926938; st_sp=2021-03-02%2010%3A46%3A14; st_inirUrl=; st_sn=1; st_psi=20210308162602719-113300301011-4200709975; st_asi=delete'
const valueOfTime = (time, d = 1) => parseInt((((new Date(time)).valueOf()) / d))
const plates_JOIN = ';'
// 代理服务器
const proxyHost = "http-dyn.abuyun.com";
const proxyPort = 9020;
// 代理隧道验证信息
const proxyUser = "HP94O133O20DO2LD";
const proxyPass = "30B5EA4AB22D0F36";
const proxyUrl = "http://" + proxyUser + ":" + proxyPass + "@" + proxyHost + ":" + proxyPort;
const PROXY_TYPE = 0 //1就用代理，0就用本机，用代理防反扒
const plimit = require('p-limit')

function getProxy(PROXY_TYPE) { //获取随机ip
    if (PROXY_TYPE) return proxyUrl
    else {
        return
    }
}

function getListByPage(i) { //传递了每页的i过来
    return new Promise(async (resolve, reject) => {
        request.get(`http://np-anotice-stock.eastmoney.com/api/security/ann?page_size=100&page_index=${i}&ann_type=A&client_source=web&f_node=5&s_node=7`)
            //.charset('gbk')
            .set({
                'User-Agent': FUA(), //获取一个随机的User-Agent
                'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
                'Cookie': cookie
            })
            .proxy(getProxy(PROXY_TYPE))
            .then(async (res) => {
                //console.log(res.header)
                //console.log(JSON.parse(res.text.trim()))
                const obj = JSON.parse(res.text.trim())
                if (!obj.error) {
                    resolve(obj.data.list)
                } else {
                    resolve(await getListByPage(i))
                }
            }, async (err) => {
                console.trace(err)
                resolve(await getListByPage(i))
            })
    })

}
async function initCookie() { //通过无头浏览器初始化获取cookie
    const url = 'http://data.eastmoney.com/notices/hsa/5.html' //列表页
    async function init() {
        const puppeteer = require('puppeteer-core');
        const findChrome = require('./find_chrome');
        let findChromePath = await findChrome({});
        let executablePath = findChromePath.executablePath;
        const options = {
            executablePath,
            headless: false,
            defaultViewport: {
                width: 1400,
                height: 800
            },
            ignoreHTTPSErrors: true,
            headless: true,
            //args: ["--proxy-server=" + proxyServer],
        }
        let browser = await puppeteer.launch(options) //开启浏览器实例，传入参数
        let [page] = await browser.pages();
        //await page.authenticate({ username : proxyUser, password : proxyPass});
        await page.goto(url)
        const context = await page.mainFrame().executionContext();
        const aHandle = await context.evaluateHandle(() => document.cookie);
        //console.log(aHandle._remoteObject.value)
        const cookie = await aHandle._remoteObject.value
        //console.log(cookie)
        page.close()
        browser.close()
        return cookie
    }
    try{
        cookie = await init()
    }catch(e){
        console.trace(e)
    }
    return cookie
}
async function getListAfterTime(latest_display_time) { //第一步拿到所有列表
    const that = this
    let i = 1
    let list = []
    try {
        await initCookie() //通过无头浏览器获得Cookie
    } catch (e) {
        console.trace(e)
    }
    while (1) {
        /*
        page_size=100 表示页中有100条，已达到服务器支持的最大值，减小了请求次数
        page_index=${i} 表示第i页 这里的操作是当爬取返回的 res['data']['list]为空时，把已经缓存的数据保存起来结束循环
        */
        let res = await getListByPage(i++)
        const len = res.length
        if (!res.length) break //没有了就结束循环
        const first = res[0]
        const last = res[len - 1]
        if (valueOfTime(first.display_time, 1000) <= valueOfTime(latest_display_time, 1000)) break;
        console.log(i - 1, valueOfTime(first.display_time, 1000), valueOfTime(first.display_time, 1000) >= valueOfTime(latest_display_time, 1000), valueOfTime(latest_display_time, 1000), valueOfTime(last.display_time, 1000) <= valueOfTime(latest_display_time, 1000), valueOfTime(last.display_time, 1000))
        if (latest_display_time &&
            valueOfTime(first.display_time, 1000) >= valueOfTime(latest_display_time, 1000) &&
            valueOfTime(last.display_time, 1000) <= valueOfTime(latest_display_time, 1000)) {
            let tmp = []
            for (let i = 0; i < len; i++) {
                let cur = res[i]
                if (valueOfTime(cur.display_time, 1000) >= valueOfTime(latest_display_time, 1000)) tmp.push(cur)
                else break;
            }
            list = [...list, ...tmp]
            break;
        } //爬到上次缓存的那一页就结束循环

        list = [...list, ...res]
    }

    // let arr =[]
    // for (let i of list){
    //     let res = await that.app.mysql.query('SELECT * FROM f5s7 WHERE art_code = ?', [i.art_code])
    //     console.log(res.length)
    //     (res.length == 0)?arr.push(i):null
    // }
    // console.log({arr})
    return list
}

function getTextByIndex(art_code, i) { //对于某些页数大于1的文章做特殊处理
    //`http://np-anotice-stock.eastmoney.com/api/security/ann?cb=jQuery1123005232201986625973_1615178110381&sr=-1&page_size=50&page_index=1&ann_type=A&client_source=web&f_node=5&s_node=7`
    const url = `http://np-cnotice-stock.eastmoney.com/api/content/ann?art_code=${art_code}&client_source=web&page_index=${i}` //文章内页接口
    return new Promise(async (resolve, reject) => {
        request.get(url)
            //.charset('gbk')
            .set({
                'User-Agent': FUA(), //获取一个随机的User-Agent
                'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
                'Cookie': cookie
            })
            .proxy(getProxy(PROXY_TYPE))
            .then(async (res) => {
                const obj = JSON.parse(res.text.trim())

                if (obj.success) {
                    resolve(obj['data']['notice_content'])
                } else {
                    resolve(await getTextByIndex(art_code, i))
                }
            }, async (err) => {
                console.trace(err)
                resolve(await getTextByIndex(art_code, i))
            })
    })
}

function getText(art_code) { //根据代码拿到text
    const url = `http://np-cnotice-stock.eastmoney.com/api/content/ann?art_code=${art_code}&client_source=web&page_index=${1}` //文章内页接口
    return new Promise(async (resolve, reject) => {
        let texttmp = ''
        request.get(url)
            //.charset('gbk')
            .set({
                'User-Agent': FUA(), //获取一个随机的User-Agent
                'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
                'Cookie': cookie
            })
            .proxy(getProxy(PROXY_TYPE))
            .then(async (res) => {
                const obj = JSON.parse(res.text.trim())
                if (obj.success) {
                    const n = obj['data']['page_size']
                    let m = n
                    for (let i = 1; i <= m; i++) {
                        if (n == 1) resolve(obj['data']['notice_content'])
                        if (n >= 2) {
                            m = 2
                            texttmp += await getTextByIndex(art_code, i)
                            if (i == 2 && n > 2) texttmp += '...更多请回原文：' + art_code
                        } else {
                            texttmp += await getTextByIndex(art_code, i)
                        }

                        //console.log(art_code,n,texttmp)
                    }
                    resolve(texttmp)
                } else {
                    resolve(await getText(art_code))
                }
            }, async (err) => {
                console.trace(err)
                resolve(await getText(art_code))
            })
    })
}

function getListItem(item) { //爬取列表中每一项
    return new Promise(async (resolve, reject) => {
        try {
            let text = await getText(item.art_code)
            let stocks = item['codes']
            let len = stocks.length
            let n = -1
            for (let i = 0; i < len; i++) {
                if (stocks[i].ann_type.indexOf('Bond') == -1 && stocks[i].short_name.indexOf('转债') == -1) //排除债券code
                {
                    n = i;
                    break;
                }
            }
            if (n == -1) {
                n = 0
                resolve({
                    art_code: item.art_code,
                    ann_type: item['codes'][n].ann_type,
                    short_name: item['codes'][n].short_name,
                    stock_code: item['codes'][n].stock_code,
                    display_time: new Date(item.display_time).valueOf(),
                    notice_date: new Date(item.notice_date).valueOf(),
                    title: item.title,
                    url: `http://data.eastmoney.com/notices/detail/${item['codes'][n].stock_code}/${item.art_code}.html`,
                    text: text,
                    crawl_time: new Date().valueOf()
                })
            }

            let newItem = {
                art_code: item.art_code,
                ann_type: item['codes'][n].ann_type,
                short_name: item['codes'][n].short_name,
                stock_code: item['codes'][n].stock_code,
                display_time: new Date(item.display_time).valueOf(),
                notice_date: new Date(item.notice_date).valueOf(),
                title: item.title,
                url: `http://data.eastmoney.com/notices/detail/${item['codes'][n].stock_code}/${item.art_code}.html`,
                text: text,
                crawl_time: new Date().valueOf()
            }
            resolve(newItem)
        } catch (e) {
            console.trace(e)
            resolve(await getListItem(item))
        }

    })
}

// function getListedType(page_url) { //拿到公司上市的type类型
//     let p = page_url
//     return new Promise(async (resolve, reject) => {
//         request.get(page_url)
//             //.charset('gbk')
//             .set({
//                 'User-Agent': FUA(), //获取一个随机的User-Agent
//                 'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
//                 'Cookie': cookie
//             })
//             .proxy(getProxy(PROXY_TYPE))
//             .then(async (res) => {
//                 const html = (res.text.trim()) //拿到html文本格式
//                 let $ = cheerio.load(html)
//                 let target = $('script')[2].children[0]
//                 let text = target.data
//                 //用正则拿到 CodeWithMarket
//                 let reg = /var CodeWithMarket = "\d{6}\.(.*)";/g
//                 let test = (reg.exec(text))
//                 if (!test) resolve('NULL')
//                 let type = (reg.exec(text))[1].toUpperCase()
//                 resolve(type)
//             }, async (err) => {
//                 console.trace(err)
//                 resolve(await getListedType(page_url))
//             })
//     })
// }

function getListedPlates(plates_api) { //拿到公司上市的plates板块
    return new Promise(async (resolve, reject) => {
        request.get(plates_api)
            //.charset('gbk')
            .set({
                'User-Agent': FUA(), //获取一个随机的User-Agent
                'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
                'Cookie': cookie
            })
            .proxy(getProxy(PROXY_TYPE))
            .then((res) => {
                const obj = JSON.parse(res.text.trim())
                if (obj['data']) {
                    let tmp = obj.data.diff
                    let list = []
                    for (let k in tmp) {
                        list.push(tmp[k]['f14'])
                    }
                    resolve(list)
                }
                resolve([])
            }, async (err) => {
                console.trace(err)
                resolve(await (getListedPlates(plates_api)))
            })
    })
}

function getListScope(f10api) { //拿到公司上市的scope经营范围
    return new Promise(async (resolve, reject) => {
        request.get(f10api)
            //.charset('gbk')
            .set({
                'User-Agent': FUA(), //获取一个随机的User-Agent
                'Referer': 'http://data.eastmoney.com/notices/hsa/5.html',
                'Cookie': cookie
            })
            .proxy(getProxy(PROXY_TYPE))
            .then((res) => {
                const obj = JSON.parse(res.text.trim())
                if (obj['data']) {
                    let len = obj['data'].length
                    let scope = []
                    for (let i = 1; i < len; i++) {
                        let cur = obj['data'][i]
                        scope.push({
                            key: cur['ClassificationName'],
                            value: cur['MainPointCon']
                        })
                    }
                    resolve(scope)
                }
                resolve([])
            }, async (err) => {
                console.trace(err)
                resolve(await (getListScope(f10api)))
            })
    })
}

function getListedCompany(code, company, plates, key) { //拿到公司表数据
    const that = this
    return new Promise(async (resolve, reject) => {
        try {
            const page_url = `http://guba.eastmoney.com/list,${code}.html` //公司列表主页，通过cheerio爬取上市所代码
            let scope = []
            const types = ['SZ', 'SH']
            let type = types[key] //拿到上市类型
            if (type == 'NULL') {
                for (let i = 0; i < types.length; i++) {
                    let cur = types[i]
                    let res = await getListScope(`http://data.eastmoney.com/dataapi/stockdata/f10api?type=F10%2FRptF10Content&postData=%7B%22SecurityCode%22%3A+%22${code}.${cur}%22%7D`)
                    if (res.length) {
                        scope = res;
                        break
                    }
                }
            } else {
                //http://data.eastmoney.com/dataapi/stockdata/f10api?type=F10%2FRptF10Content&postData=%7B%22SecurityCode%22%3A+%300571.SZ%22%7D
                const f10api = `http://data.eastmoney.com/dataapi/stockdata/f10api?type=F10%2FRptF10Content&postData=%7B%22SecurityCode%22%3A+%22${code}.${type}%22%7D`

                scope = await getListScope(f10api) //拿到公司的经营范围
            }

            let stock = {
                type,
                plates: plates.join(plates_JOIN),
                scope: JSON.stringify(scope)
            }
            let listed_nsert_sql = `INSERT INTO listed(code,company,scope,type,plates) 
            VALUES('${code}','${company}','${stock.scope}','${type}','${plates.join(plates_JOIN)}') ON DUPLICATE KEY UPDATE 
            code="${code}"
            `
            await that.app.mysql.query(listed_nsert_sql)
            //await that.app.mysql.insert('listed', {
            //    ...stock,
            //    code,
            //    company,
            //})
            let stock_code = code
            //先检索plate_info 
            let limit = plimit(1) //改成n个async任务并行去跑而不是纯串行单任务
            let p = plates.map((item, i) => {
                return limit((async () => {
                    let res = await that.app.mysql.get('plate_info', {
                        plate_name: item
                    })
                    if (!res) {
                        try {
                            res = await that.app.mysql.insert('plate_info', {
                                plate_name: item
                            })
                        } catch (e) {
                            console.log(e)
                        }
                    }
                    return item
                }))
            })
            const pr = await (await Promise.all(p))
            let arr = plates.map(a => ({
                stock_code,
                stock_plate: a
            }))
            try {
                await that.app.mysql.insert('plates', arr)
            } catch (e) {
                console.trace(e)
            }
            resolve(stock)
        } catch (e) {
            console.trace(e)
            resolve(await getListedCompany.call(that, code, company, plates, key))
        }

    })

}
async function insertf5s7Item(f5s7) { //设置插入数据库的事务
    const that = this
    //插入f5s7以及crawl_log日志
    //console.log(f5s7)
    //经过call改绑this，可以使用this.app.mysql
    //插入内容包括f5s7的实体，还有crawler_log的操作日志，以及listed的增量更新
    const new_log = {
        art_code: f5s7.art_code,
        title: f5s7.title,
        crawl_time: f5s7.crawl_time,
        crawler: 'admin',
        type: 0
    }
    let issaved = 1
    const conn = await this.app.mysql.beginTransaction(); // 初始化事务
    try {
        const f5s7_res = await conn.query('SELECT * FROM f5s7 WHERE art_code = ?', [f5s7.art_code])
        let key = (+f5s7.stock_code[0]) > 5 ? 1 : 0 //api的secid，自己猜的规律
        const plates_api = `http://push2.eastmoney.com/api/qt/slist/get?pn=1&pz=100&spt=3&fields=f14&secid=${key}.${f5s7.stock_code}&fltt=2` //拿到100以内的所属板块，一般不会超过20个
        //拿到page_url通过正则匹配script标签中的var CodeWithMarket = "601369.sh";类似的字段拿到601369.SH
        let plates = await getListedPlates(plates_api) //拿到公司板块数组
        if (f5s7_res.length == 0) {
            const listed_res = await conn.query('SELECT * FROM listed WHERE code = ?', [f5s7.stock_code])
            if (listed_res.length == 0) {
                //需要爬一下公司,存入到listed库
                let code = f5s7.stock_code
                let company = f5s7.short_name
                const listed = await getListedCompany.call(that, code, company, plates, key)
            }

            await conn.insert('f5s7', {
                ...f5s7,
                valid: 1
            })
            await conn.insert('crawl_log', new_log)
            await conn.commit(); //提交事务
            console.log(new_log)
        } else {
            issaved = 0
            await conn.commit(); //提交事务
            console.log('saved', new_log)
        }

    } catch (e) {
        console.trace(e)
        await conn.rollback();
        return await insertf5s7Item.call(that, f5s7)
    }
    if (issaved) return f5s7.art_code
}
let sleep = (time) => {
    return new Promise((resolve) => {
        setTimeout((() => {
            console.log(time, "sleeped")
            resolve(time)
        }), time * 1000)
    })
}
async function run(code) {
    console.log('-------------------------spider start--------------------------------------')
    console.time('spider')
    const that = this
    let latest_display_time = +((await this.app.mysql.get('base', {
        admin: 'admin'
    }))['latest_display_time']) //缓存时间，初始为null,但是会每次爬取完去更新一个display_time，作为增量依据
    if (code) latest_display_time = 0
    console.log(latest_display_time)
    let list = await getListAfterTime.call(this, latest_display_time)
    if (!list.length) {
        console.log('无更新') //这个list过滤还未实现
        return
    }
    console.log(list.length, '准备更新(有误差操作)')
    const cur_latest_display_time = list[0]['display_time'] //用于缓存更新位置，发现display_time有顺序，而art_code只能保证唯一性
    console.log(cur_latest_display_time)
    //下面实现基于promise的串行爬取到数据库，确保错误处理明确，以及避免服务请求太多炸掉
    let d = 0 //断点继续爬，做调试的拼接，默认给0即可
    list = list.slice(d)
    let newarr = []
    let limit = plimit(3) //改成n个async任务并行去跑而不是纯串行单任务
    let l = list.map((item, i) => {
        return limit((async () => {
            let cur = item
            let f5s7 = await getListItem.call(that, cur)
            //数据库插入
            let tmp = await insertf5s7Item.call(that, f5s7) //f5s7平铺和数据库表格式相同
            if (tmp) newarr.push(tmp)
            console.log(d + i)
            // await sleep(1)
            return tmp
        }))
    })
    const lr = await (await Promise.all(l)).filter(item => !!item)
    console.log('limit', lr)
    // await list.reduce(async (pre, cur, i) => {//单任务串行
    //     try {
    //         await pre
    //     } catch (e) {
    //         console.trace(e)
    //     }
    //     return new Promise(async (resolve, reject) => {

    //         let f5s7 = await getListItem.call(that, cur)
    //         //数据库插入
    //         let tmp = await insertf5s7Item.call(that, f5s7) //f5s7平铺和数据库表格式相同
    //         if (tmp) newarr.push(tmp)
    //         console.log(d + i)
    //         resolve()
    //     })
    // }, Promise.resolve())
    //跑完爬取之后缓存时间上次时间
    try {

        let result = await this.app.mysql.query(`
        UPDATE base SET 
        latest_display_time = '${valueOfTime(cur_latest_display_time)-1000}', 
        last_crawler_time = '${valueOfTime(new Date())}' 
        WHERE id = 1 ;
        `)
        //因为东方财富的display_time的毫秒级别总是有误差，直接减1s
        if (
            result.affectedRows == 0 &&
            result.message.indexOf("matched: 0") > 0
        ) {
            //id无效的时候直接新插入
            try {
                result = await this.app.mysql.insert("base", {
                    "Id": 1,
                    "admin": "admin",
                    "password": "admin",
                    "ps": "管理员",
                    "latest_display_time": valueOfTime(cur_latest_display_time),
                    "last_crawler_time": valueOfTime(new Date())
                });
            } catch (e) {
                console.log(e)
            }

        }
        console.log('时间已缓存', result)
        console.log('-------------------------spider end --------------------------------------')
        console.timeEnd('spider')

    } catch (e) {
        console.trace('更新base时间失败', e)
        console.log('-------------------------spider end --------------------------------------')
        console.timeEnd('spider')
    }
    if (newarr.length > 0) {
        let md5 = require('./md5')
        that.app.md5 = md5(new Date())
        const set_new_art = require('../socket_utils/new_art').set_new_art
        set_new_art.call(that, {
            newarr: lr,
        })
        this.app.events.emit('new_art')
        // that.app.io.of('/admin').emit('new_art', {
        //     newarr: lr,
        //     md5:that.app.md5
        // })
    }
    return newarr
    //console.log(list[0])
    /*  item项的格式
        {
        art_code: 'AN202103091469943055',
        codes: [
            {
            ann_type: 'A,SHA',
            market_code: '1',
            short_name: '浦东建设',
            stock_code: '600284'
            }
        ],
        columns: [ { column_code: '001002006004', column_name: '重大合同' } ],
        display_time: '2021-03-09 15:39:52:034',
        notice_date: '2021-03-10 00:00:00',
        title: '600284:浦东建设重大工程项目中标公告'
        }
    */


}
//run()
module.exports = run