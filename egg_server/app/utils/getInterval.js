async function getInterval() {//获得爬虫时间间隔
    const that = this
    let n = +((await that.app.mysql.query('select update_interval from base'))[0]['update_interval'])
    if (n <= 0) {
        await this.app.mysql.query(`
            UPDATE base SET 
            update_interval = '${1}' 
            WHERE id = 1 ;
            `)
        n = 1
    }
    return n
}
module.exports = getInterval