let fn = require('./getList')
async function setTimer(hours, state) { //设置爬虫定时器
    const that = this
    try {
        clearInterval(that.app.timer)
        that.app.timer = null
        if (state == null) state = +((await that.app.mysql.query('select timer_state from base'))[0]['timer_state'])
        if (hours == null) {
            hours = await require('./getInterval').call(that)
        }
        console.log("爬虫" + (state ? '开启' : '关闭'), '间隔' + hours + 'h')
        await this.app.mysql.query(`
            UPDATE base SET 
            timer_state = '${state}' ,update_interval = '${hours}' 
            WHERE id = 1 ;
            `)
        if (state == 0) {
            
        } else {
            await fn.call(that)
            this.app.timer = setInterval(async () => {
                await fn.call(that)
            }, hours * 1000 * 60 * 60)
        }
        
    } catch (e) {
        console.trace(e)
        return e
    }

}
module.exports = setTimer