const plimit = require('p-limit')
async function  run() {
    const that = this
    let platesobjs = await that.app.mysql.query('SELECT * FROM plates')
    let plates = platesobjs.map(item=>{
        return item.stock_plate
    })
    let limit = plimit(1) //改成n个async任务并行去跑而不是纯串行单任务
    let p = plates.map((item, i) => {
        return limit((async () => {
            let res = await that.app.mysql.get('plate_info',{
                plate_name:item
            })
            if(!res){
                res = await that.app.mysql.insert('plate_info',{
                    plate_name:item
                })
            }
            return item 
        }))
    })
    const pr = await (await Promise.all(p))
}
module.exports= run