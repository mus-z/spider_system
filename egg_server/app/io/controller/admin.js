const Controller = require('egg').Controller;
const {
  getchatusers
} = require('../../socket_utils/getchatusers')
class DefaultController extends Controller {
  async server() {
    const {
      ctx,
      app
    } = this;
    const that = this
    const message = ctx.args;
    const admin_get_new_art = require('../../socket_utils/new_art').admin_get_new_art
    admin_get_new_art.call(that)
    const id = message[0]
    // console.log(id)
    const admin_nsp = this.app.io.of('/admin')
    // console.log(admin_nsp.sockets[message[0]].id)
  }
  async getchat() {
    const {
      ctx,
      app
    } = this;
    app.chat = {
      ...(app.chat || {})
    }
    const admin_nsp = this.app.io.of('/admin')
    admin_nsp.emit('chat', await getchatusers(app.chat, app))
  }
  async chat() {
    const {
      ctx,
      app
    } = this;
    const nsp = app.io.of('/visitor');
    const message = ctx.args[0];
    const {
      user_id,
      text,
      msg_time,
    } = message
    const obj = {
      ...message,
      isAdmin: 1
    }
    app.chat = {
      ...(app.chat || {})
    }
    app.chat[user_id] = [...(app.chat[user_id] || []), obj]
    nsp.emit(user_id+'msgList', {
      msgList: app.chat[user_id] || []
    })
    const admin_nsp = this.app.io.of('/admin')
    admin_nsp.emit('chat', await getchatusers(app.chat, app))
  }

}

module.exports = DefaultController;