const Controller = require('egg').Controller;
const {
    getchatusers
} = require('../../socket_utils/getchatusers')

function adminOnlie(app) {
    return app.rooms['admin'] ?.length > 0
}

class DefaultController extends Controller {
    async server() {
        const {
            ctx,
            app
        } = this;
        const socket = ctx.socket;
        const that = this
        const nsp = app.io.of('/visitor');
        const query = socket.handshake.query
        const message = ctx.args[0];
        const {
            user_id
        } = message
        // console.log(user_id)
        const id = socket.id;

        const client = socket.id;
        // console.log(app.user_ids)
        const visitor_get_new_art = require('../../socket_utils/new_art').visitor_get_new_art
        visitor_get_new_art.call(that, user_id)
        app.chat = {
            ...(app.chat || {})
        }
        app.chat[user_id] = [...(app.chat[user_id] || [])]
        nsp.emit(user_id, {
            msg: 'login',
            user_ids: app.user_ids,
            admin_online: that.app.rooms['admin'] ?.length > 0,
        })
        nsp.emit(user_id+'msgList',{
            msgList: app.chat[user_id] || []
        })
        // console.log(client, message, query)
        // console.log(admin_nsp.sockets[message[0]].id)
    }
    async getchat() {
        const {
            ctx,
            app
        } = this;
        const message = ctx.args[0];
        const {
            user_id,
        } = message
        const nsp = app.io.of('/visitor');
        app.chat = {
            ...(app.chat || {})
        }
        app.chat[user_id] = [...(app.chat[user_id] || [])]
        nsp.emit(user_id+'msgList', {
            msgList: app.chat[user_id] || [],
            append: '你已经可以开始和管理员交谈了'
        })
    }
    async chat() {
        const {
            ctx,
            app
        } = this;
        const socket = ctx.socket;
        const that = this
        const nsp = app.io.of('/visitor');
        const message = ctx.args[0];
        const {
            user_id,
            text,
            msg_time,
        } = message

        if (adminOnlie(app)) {
            const obj = {
                ...message,
                isAdmin: 0
            }
            app.chat = {
                ...(app.chat || {})
            }
            app.chat[user_id] = [...(app.chat[user_id] || []), obj]
            nsp.emit(user_id+'msgList', {
                msgList: app.chat[user_id] || []
            })
            const admin_nsp = this.app.io.of('/admin')
            admin_nsp.emit('chat', await getchatusers(app.chat, app))
        } else {

        }
    }
}

module.exports = DefaultController;