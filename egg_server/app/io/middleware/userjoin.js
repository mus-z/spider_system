module.exports = app => {
    // 这个中间件的作用是提示用户连接与断开的，连接成功的消息发送到客户端，断开连接的消息在服务端打印
    return async function (ctx, next) {
        const {
            app,
            socket
        } = ctx;
        const that = this
        const query = socket.handshake.query
        const {
            user_id
        } = query
        const id = socket.id;
        //靠全局唯一的app绑定用户在线用户id的数组
        app.user_ids = {...app.user_ids}
        app.user_ids[user_id]=[...(app.user_ids[user_id]||[]),id]
        // console.log(user_id, 'userjoin')
        await next();
        // console.log(user_id, 'disconnection!');
        //断开的连接及时清出去
        app.user_ids[user_id] = app.user_ids[user_id].filter(item => item !== id)
    };
};