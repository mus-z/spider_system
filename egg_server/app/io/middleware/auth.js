function AdminOnlineStatePush(app){
    const nsp = app.io.of('/visitor')
    app.user_ids = {...(app.user_ids||{})}
    Object.keys(app.user_ids).map(user_id=>{//提醒用户们管理员上线
        nsp.emit(user_id, {
            admin_online: app.rooms['admin'] ?.length > 0
        })
    })
}
module.exports = app => {
    // 这个中间件的作用是提示用户连接与断开的，连接成功的消息发送到客户端，断开连接的消息在服务端打印
    return async function (ctx, next) {
        const {
            app,
            socket
        } = ctx;
        const that = this
        const id = socket.id;
        let admin_nsp = app.io.of('/admin')
        // console.log(admin_nsp)
        app.rooms['admin'] = app.rooms['admin'] || []
        if (app.rooms['admin'].length <= 0) {
            app.rooms['admin'] = [id]
        } else {
            app.rooms['admin'].push(id)
        }
        AdminOnlineStatePush(app)
        // console.log('auth')
        await next();
        // console.log('disconnection!');
        // console.log(app.rooms['admin'], id)
        //断开的连接及时清出去
        app.rooms['admin'] = app.rooms['admin'].filter(item => item !== id)
        AdminOnlineStatePush(app)
        // console.log(app.rooms['admin'], id)
    };
};