const plimit = require('p-limit')
async function getpushart(list) {
    const that = this
    let tmp = await list.map(async (item) => {
        const {
            user_id,
            art_code
        } = item
        let sql = `SELECT user_wait_push.*,f5s7.short_name,f5s7.stock_code,f5s7.notice_date,f5s7.title,f5s7.crawl_time 
        FROM user_wait_push JOIN f5s7 
        ON user_wait_push.art_code = f5s7.art_code 
        WHERE f5s7.valid = 1 
        AND user_wait_push.art_code = "${art_code}" AND user_wait_push.user_id = "${user_id}" ;`
        let res = await (await that.app.mysql.query(sql))[0]
        return res
    })
    let arr = (await Promise.all(tmp)).filter(item => !!item)
    return arr
}
module.exports = {
    set_new_art: async function (obj) {
        const {
            newarr
        } = obj
        const that = this
        const {
            app
        } = that
        /**
         * admin 离线保存在app.tmps['admin']中，只保存了art_code
         */
        that.app.rooms['admin'] = that.app.rooms['admin'] || []
        if (that.app.rooms['admin'] && that.app.rooms['admin'].length > 0) {
            that.app.io.of('/admin').emit('new_art', {
                newarr,
                md5: that.app.md5
            })
        } else {
            that.app.tmps['admin'] = that.app.tmps['admin'] || []
            that.app.tmps['admin'] = [...that.app.tmps['admin'], ...newarr] || [...newarr] //缓存离线数据
            // console.log(that.app.tmps['admin'])
        }
        /* 
         * newarr Array:[art_code]
         * app.user_ids[user_id]:[socket_id]
         * visitor 保存到mysql user_wait_push 中
            	`Id` INT(11) NOT NULL AUTO_INCREMENT,
                `user_id` INT(11) NOT NULL,
                `art_code` VARCHAR(20) NOT NULL DEFAULT '' COLLATE 'utf8mb4_0900_ai_ci',
                `pushed` TINYINT(4) NOT NULL DEFAULT '0' COMMENT '是否已经推送，默认0未推送',
        */
        app.user_ids = {
            ...app.user_ids
        }
        let ids = Object.keys(app.user_ids).filter(key => app.user_ids[key].length > 0) // 在线的所有用户id
        let limit = plimit(5)
        let pre_users = newarr.map(art_code => {
            return limit(async () => {
                let sql = `SELECT c.*,user_attention_plate_info.user_id FROM
                (SELECT b.*,plate_info.Id AS plate_info_id FROM 
                    (SELECT a.*,plates.stock_plate FROM 
                        (SELECT f5s7.art_code,f5s7.stock_code FROM f5s7 WHERE f5s7.art_code = "${art_code}")a JOIN plates 
                        ON a.stock_code = plates.stock_code WHERE plate_valid = 1 )b 
                JOIN plate_info ON b.stock_plate = plate_info.plate_name)c 
            JOIN user_attention_plate_info ON c.plate_info_id = user_attention_plate_info.plate_info_id`
                return await that.app.mysql.query(sql)
            })
        })
        /**
         * pr这次更新的文章对应的板块和关注用户
         * 
            {
            "art_code": "AN202104191486453515",
            "stock_code": "300291",
            "stock_plate": "创业板综",
            "plate_info_id": 28,
            "user_id": 1
            },
         */
        let tmp_pr = await Promise.all(pre_users)
        console.log(tmp_pr)
        Array.prototype.flat = function (count) {
            let c = count || 1;
            let len = this.length;
            let exe = [];
            if (this.length == 0) return this;
            while (c--) {
                let _arr = [];
                let flag = false;
                if (exe.length == 0) {
                    flag = true;
                    for (let i = 0; i < len; i++) {
                        if (this[i] instanceof Array) {
                            exe.push(...this[i]);
                        } else {
                            exe.push(this[i]);
                        }
                    }
                } else {
                    for (let i = 0; i < exe.length; i++) {
                        if (exe[i] instanceof Array) {
                            flag = true;
                            _arr.push(...exe[i]);
                        } else {
                            _arr.push(exe[i]);
                        }
                    }
                    exe = _arr;
                }
                if (!flag && c == Infinity) {
                    break;
                }
            }
            return exe;
        }
        const pr = (tmp_pr || []).flat(1)
        //转化一下格式,tmp的key为user_id,value为待推送文章的数组
        /**
         * 
            "1": [
            "AN202104191486453515",
            "AN202104191486453515"
            ],
            "2": [
            "AN202104191486452799"
            ],
            "10": [
            "AN202104191486454037"
            ]
         */
        let arttmp = {}
        if (true) {
            for (let item of pr) {
                const {
                    user_id,
                    art_code,
                    stock_plate,
                } = item
                if (!arttmp[user_id]) {
                    arttmp[user_id] = [{
                        art_code,
                        plates: [stock_plate]
                    }]
                } else {
                    function findItem(arr, art_code) {
                        let t = -1
                        for (let i = 0; i < arr.length; i++) {
                            let item = arr[i]
                            if (item.art_code == art_code) {
                                t = i;
                                break;
                            }
                        }
                        return t
                    }
                    let cur = findItem(arttmp[user_id], art_code)
                    if (cur < 0)
                        arttmp[user_id].push({
                            art_code,
                            plates: [stock_plate]
                        })
                    else {
                        // console.log(cur,arttmp,art_code)
                        arttmp[user_id][cur].plates.push(stock_plate)
                    }
                }
            }
        }
        //过滤在线的和不在线的
        let pushlist = (await Object.keys(arttmp).map((user_id) => {
            let t = [...arttmp[user_id]]
            t = t.map(item => {
                return {
                    user_id,
                    art_code: item.art_code,
                    plates: item.plates.join(';')
                }
            })
            return t
        })).flat(1)
        // console.log(list)
        try {
            //把推送都加入到数据库中
            pushlist.map(async (item) => {
                await that.app.mysql.insert('user_wait_push', item)
            })

        } catch (e) {

        }

        //如果在线就马上推送
        await ids.map(async (user_id) => {
            let t = [...(arttmp[user_id]||[])]
            t = t.map(item => ({
                user_id,
                art_code: item.art_code,
            }))
            arttmp[user_id] = []
            if (t.length) {
                let pushlist = await getpushart.call(that, t)
                await app.io.of('/visitor').emit(user_id, {
                    pushlist,
                })
            }
            return user_id
        })
        return arttmp
    },
    admin_get_new_art: async function () {
        //admin用的上线后推送
        const that = this
        const {
            app
        } = that
        that.app.tmps['admin'] = that.app.tmps['admin'] || []
        if (app.tmps['admin'] && app.tmps['admin'].length > 0) {
            const tmp = app.tmps['admin']
            app.io.of('/admin').emit('new_art', {
                newarr: tmp,
                md5: app.md5
            })
            app.tmps['admin'] = []
        }
        return app.tmps['admin']
    },
    visitor_get_new_art: async function (user_id) {
        //visitor用的上线后推送
        const that = this
        const {
            app
        } = that

        let list = await that.app.mysql.query(`SELECT * FROM user_wait_push WHERE user_id = "${user_id}" `)

        let pushlist = await getpushart.call(that, list)
        // console.log({
        //     user_id,
        //     pushlist
        // })
        await app.io.of('/visitor').emit(user_id, {
            pushlist
        })
    }
}