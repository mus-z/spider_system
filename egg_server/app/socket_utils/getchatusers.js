async function getchatusers(chat={},app){
    let tmp = []
    let ll=Object.keys(chat).map(async(key)=>{
        const t = {}
        const item = chat[key]
        if(!item.length)return null
        let sql = `SELECT Id,user_nickname FROM users WHERE Id = ${key} `
        let user = (await app.mysql.query(sql))[0]
        user = {
            ...user,msgs:item
        }
        // console.log(user)
        return user
    })
    let res = (await Promise.all(ll)).filter(item=>!!item)
    // console.log(res)
    return res
}
module.exports={
    getchatusers
}