module.exports = app => {
    const {
        router,
        controller,
        io
    } = app
    let adminauth = app.middleware.adminauth()
    let adminhome = controller.admin.home
    router.get('/admin/index', adminhome.index)
    router.get('/admin/changetimer/:hours', adminhome.changeTimer)
    router.get('/admin/switchtimer/:state', adminhome.switchTimer)
    router.get('/admin/spideronce', adminhome.spiderOnce)
    router.post('/admin/adminlogin', adminhome.adminLogin)
    router.get('/admin/adminlogout', adminauth, adminhome.adminLogout)
    router.post('/admin/adminlogcheck', adminauth, adminhome.adminLogCheck)
    router.get('/admin/adminquery', adminauth, adminhome.adminQuery)
    router.post('/admin/adminupdate', adminauth, adminhome.adminUpdate)
    router.post('/admin/admingetlist', adminauth, adminhome.adminGetList)
    router.post('/admin/admingetcompanylist', adminauth, adminhome.adminGetCompanyList)
    router.get('/admin/admincheckmd5', adminauth, adminhome.adminCheckmd5)
    router.get('/admin/admingetcomponybystock/:stock', adminauth, adminhome.admingetcomponybystock)
    router.get('/admin/admingetplatesbystock/:stock', adminauth, adminhome.admingetplatesbystock)
    router.post('/admin/adminaddoneplate', adminauth, adminhome.adminaddoneplate)
    router.post('/admin/admindeloneplate', adminauth, adminhome.admindeloneplate)
    router.get('/admin/admingetarticlebycode/:artcode', adminauth, adminhome.admingetarticlebycode)
    router.get('/admin/admingetallplates', adminauth, adminhome.admingetallplates)
    router.get('/admin/admingetallusers', adminauth, adminhome.admingetallusers)
    router.get('/admin/admingetuserbyid', adminauth, adminhome.admingetuserbyid)
    router.get('/admin/usergetviewarthistory', adminauth, adminhome.usergetviewarthistory)
    router.get('/admin/admingetartcommentbycode', adminauth, adminhome.admingetartcommentbycode)
    router.get('/admin/admindeletecomment', adminauth, adminhome.admindeletecomment)
    router.get('/admin/admingetdata', adminauth, adminhome.admingetdata)
    
    /** */
    io.of('/admin').route('server',io.controller.admin.server);
    io.of('/admin').route('chat',io.controller.admin.chat);
    io.of('/admin').route('getchat',io.controller.admin.getchat);

}