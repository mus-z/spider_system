module.exports = app => {
    const {
        router,
        controller,
        io
    } = app
    const visitor = controller.visitor.home
    router.get('/visitor/index',visitor.index)
    router.post('/visitor/userregister',visitor.userRegister)
    router.post('/visitor/userlogin',visitor. userLogin)
    router.get('/visitor/usercklogin',visitor. userCkLogin)
    router.get('/visitor/userlogout',visitor. userLogout)
    router.post('/visitor/userupdate',visitor.userUpdate)
    router.get('/visitor/usergetartlist',visitor.usergetArtList)
    router.get('/visitor/usergetcomponybystock',visitor.usergetcomponybystock)
    router.get('/visitor/usergetarticlebystock',visitor.usergetarticlebystock)
    router.post('/visitor/usergetcompanylist',visitor.usergetcompanylist)
    router.get('/visitor/usergetplatelist',visitor.usergetPlateList)
    router.get('/visitor/usergetplateinfo',visitor.usergetPlateInfo)
    router.get('/visitor/usergetattentionplatesbyid',visitor.usergetattentionplatesbyid)
    router.get('/visitor/userattentionplate',visitor.userattentionplate)
    router.get('/visitor/usergetplateattention',visitor.usergetplateattention)
    router.get('/visitor/userviewart',visitor.userviewart)
    router.get('/visitor/usergetviewarthistory',visitor.usergetviewarthistory)
    router.get('/visitor/usergetartcommentbycode',visitor.usergetartcommentbycode)
    router.post('/visitor/usermakecomment',visitor.usermakecomment)
    router.get('/visitor/userdeletecomment',visitor.userdeletecomment)
    router.post('/visitor/usersetpushlistviewed',visitor.usersetpushlistviewed)
    router.get('/visitor/getrecommendartbyuserid',visitor.getrecommendartbyuserid)
    
    /** */
    io.of('/visitor').route('server',io.controller.visitor.server);
    io.of('/visitor').route('chat',io.controller.visitor.chat);
    io.of('/visitor').route('getchat',io.controller.visitor.getchat);

}