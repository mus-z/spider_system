/* eslint valid-jsdoc: "off" */

'use strict';

/**
 * @param {Egg.EggAppInfo} appInfo app info
 */
module.exports = appInfo => {
  /**
   * built-in config
   * @type {Egg.EggAppConfig}
   **/
  const config = exports = {};

  // use for cookie sign key, should change to your own and keep security
  config.keys = appInfo.name + '_1615115609733_2337';

  // add your middleware config here
  config.middleware = [];

  // add your user config here
  const userConfig = {
    // myAppName: 'egg',
  };
  config.mysql = {
    // database configuration
    client: {
      // host
      host: "localhost",
      // port
      port: "3306",
      // username
      user: "root",
      // password
      password: "mysql80",
      // database
      database: "spider_system",
    },
    // load into app, default is open
    app: true,
    // load into agent, default is close
    agent: false,
  };
  config.security = { //安全机制关闭
    csrf: {
      enable: false,
    },
    domainWhiteList: ['http://localhost:9000', 'http://localhost:9001', 'http://localhost:3000'],
  };

  config.cors = {
    //origin:"http://localhost:3000", //只允许这个域进行访问接口
    //隐藏origin会覆盖domainWhiteList白名单
    credentials: true, // 开启认证
    allowMethods: 'GET,HEAD,PUT,POST,DELETE,PATCH,OPTIONS'
  };
  config.io = {
    init: {
      wsEngine: 'ws',
    }, // passed to engine.io
    namespace: {
      '/admin': {
        connectionMiddleware: ['auth'],
        packetMiddleware: ['filter'],
      },
      '/visitor':{
        connectionMiddleware: ['userjoin'],
      }
    },
    // redis: {
    //   host: '127.0.0.1',
    //   port: 6379,
    // },
  };
  // config.redis = {
  //   client: {
  //     port: 6379,
  //     host: '127.0.0.1',
  //     password: '',
  //     db: 0,
  //   },
  // }
  return {
    ...config,
    ...userConfig,
  };
};