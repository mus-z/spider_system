import { useState } from 'react';
import axios from 'axios'
import dayjs from 'dayjs'
import { Form, Input, Button, Radio, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { useRouter } from 'next/router'
let server = require('@/String').server
const LoginForm = (props) => {
  const [user, setUser] = useState({})
  const router = useRouter()
  const onRegister = (values) => {
    //console.log('register: ', values);
    axios({
      url: server + 'userregister',
      method: 'post',
      withCredentials: true,
      data: { ...values, user_creattime: dayjs().valueOf() }
    }).then(res => {
      console.log(res)
      const { data } = res
      if (data?.success) {
        message.success('注册成功')
      } else {
        message.warn(data.msg + '')
      }
    }, e => {
      message.warn(e + '')
    })
  };
  const onLogin = (values) => {
    axios({
      url: server + 'userlogin',
      method: 'post',
      withCredentials: true,
      data: { ...values }
    }).then(res => {
      console.log(res)
      const { data } = res
      if (data?.success) {
        const user = data.data
        message.success('登录成功')
        setUser(user)
        router.push({
          pathname: "/",
          state: user,
          query: {
            user_name: user.user_name
          },
        }, '/')
      } else {
        message.warn(data.msg + '')
      }
    }, e => {
      message.warn(e + '')
    })
  };
  const [isregister, setisregister] = useState(0)
  const options = [
    { label: 'login', value: 0 },
    { label: 'register', value: 1 },
  ];
  const layout = {
    labelCol: {
      span: 6,
    },
    wrapperCol: {
      span: 18,
    },
    width: '100%'
  };
  const tailLayout = {
    wrapperCol: {
      span: 24,
    },
  };
  return (
    <>
      <Radio.Group
        options={options}
        onChange={(e) => {
          setisregister(e.target.value)
        }}
        value={isregister}
        optionType="button"
        buttonStyle="solid"
        style={{
          paddingBottom: '2rem'
        }}
      />
      {isregister ?
        <Form
          {...layout}
          name="normal_rigister"
          className="rigister-form"
          onFinish={onRegister}
        >
          <div>

          </div>
          <Form.Item
            name="user_nickname"
            rules={[
              {
                required: true,
                message: 'Nickname, required',
              },
            ]}
            label="昵称"
          >
            <Input placeholder="Nickname" />
          </Form.Item>
          <Form.Item
            name="user_name"
            rules={[
              {
                pattern: /^[a-z\d\.\_]+$/,
                required: true,
                message: 'Username, in (a-z|0-9|.|_)',
              },
            ]}
            label="账号"
          >
            <Input placeholder="Username" />
          </Form.Item>
          <Form.Item
            name="user_password"
            rules={[
              {
                pattern: /^[a-z\d\.\_]+$/,
                required: true,
                message: 'Password, in (a-z|0-9|.|_)',
              },
            ]}
            label="密码"
          >
            <Input
              type="password"
              placeholder="Password"
            />
          </Form.Item>
          <Form.Item
            name="user_email"
            rules={[
              {
                required: true,
                type: 'email',
                message: 'Please input your legal email',
              },
            ]}
            label="邮箱"
          >
            <Input placeholder="e-mail" />
          </Form.Item>
          <Form.Item {...tailLayout}>

            <Button type="primary" htmlType="submit" className="login-form-button" block>
              register now
      </Button>
          </Form.Item>
        </Form>
        :
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{
            remember: true,
          }}
          onFinish={onLogin}
        >
          <Form.Item
            name="user_name"
            rules={[
              {
                required: true,
                message: 'Please input your Username/e-mail!!',
              },
            ]}
          >
            <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username/e-mail" />
          </Form.Item>
          <Form.Item
            name="user_password"
            rules={[
              {
                required: true,
                message: 'Please input your Password!',
              },
            ]}
          >
            <Input
              prefix={<LockOutlined className="site-form-item-icon" />}
              type="password"
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button" block>
              Log in
        </Button>
          </Form.Item>
        </Form>
      }
      <style jsx global>
        {`
        
        .visitor-login-main .login-form-forgot {
          float: right;
        }
        
        .visitor-login-main .ant-col-rtl .login-form-forgot {
          float: left;
        }
        `}
      </style>
    </>
  );
};
export default LoginForm