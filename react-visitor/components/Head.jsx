import Head from 'next/head'
function MHead(props) {
    return (
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <title>{props.title}</title>
            <link rel="icon" href="/favicon.ico" />
        </Head>)
}
export default MHead
