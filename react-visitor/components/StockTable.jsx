import React, { useState, useEffect, useMemo } from 'react';
import { Button, Table, Tooltip, Input, Row, Col, Tag } from 'antd';
import axios from 'axios'
import {
    ContactsFilled,
    SearchOutlined,
} from "@ant-design/icons";
function StockTable(props) {
    const [data, setData] = useState([])
    const [total, setTotal] = useState(0)
    const { breakpoint } = props
    const [isLoading, setIsLoading] = useState(false)
    const [page, setPage] = useState(0)
    const [pagesize, setPagesize] = useState(10)

    const [searchValue, setSearchValue] = useState('')
    const { plateId, platename, } = props
    let option = useMemo(() => {
        return {
            searchValue, plateId
        }
    }, [searchValue, plateId])
    useEffect(() => {
        pageInit(1, 10)
        // const sv = props.history?.location?.state?.searchValue
        // if (sv) {
        //     setSearchValue(sv)
        // }
    }, [])
    const pageInit = (page, pagesize) => {
        setPage(page - 1)
        setPagesize(pagesize)
        sessionStorage.setItem(JSON.stringify(option) + 'page', page - 1)
        sessionStorage.setItem(JSON.stringify(option) + 'pagesize', pagesize)
    }
    const clickChangeValue = (value) => { setSearchValue(value) }
    // code,company,type,plates
    const columnsx = [
        {
            title: '股票代码',
            dataIndex: 'code',
            render: (value, obj) => {
                return (
                    <div
                        key={value}
                    >
                        {value}
                    </div>
                )
            }
        },
        {
            title: '公司',
            dataIndex: 'company',
            render: (value, obj) => {
                return (
                    <a key={value}
                        style={{
                            display: 'block'
                        }}
                        href={`/company/${obj.code}`}
                    >
                        {value}
                    </a>
                )
            }
        },
        {
            title: '上市类型',
            dataIndex: 'type',
            render: (value) => {
                return (<Tooltip
                    placement="right"
                    key={value}
                    title={() => {
                        switch (value) {
                            case "SH": {
                                return '沪市'
                            }
                            case 'SZ': {
                                return '深市'
                            }
                        }
                    }} ><Tag
                        icon={<ContactsFilled />}
                        color={"#cd201f"}
                    ><span
                        style={{
                            cursor: 'alias'
                        }}
                        onClick={() => {
                            // console.log(value)
                            clickChangeValue(value)
                        }}>{value}
                        </span></Tag></Tooltip>)
            },
        },
        {
            title: '所属板块',
            dataIndex: 'stock_plates',
            width: '60%',
            render: (_, obj) => {
                const plates = obj['plates']
                return (
                    <div key={obj.code}>
                        {
                            plates.map(plate => {
                                return (<Tag
                                    color={(() => {
                                        if (plate == searchValue) return 'green'
                                        return (platename == plate ? "red" : 'gold')
                                    })()}
                                    key={_ + plate}
                                ><span
                                    style={{
                                        cursor: 'alias'
                                    }}
                                    onClick={() => {
                                        clickChangeValue(plate)
                                    }}
                                >{plate}</span></Tag>)
                            })
                        }
                    </div>
                )
            },
        },
    ]
    const tableSearch = async (page, pagesize, option) => {
        setIsLoading(true)
        let url = require('@/String.js').server + 'usergetcompanylist'
        async function getlist() {
            try {
                let res = await axios.post(url, { page, pagesize, option }, { withCredentials: true, })
                const { data } = res
                // console.log(data)
                setData(data.arr)
                setTotal(data.total)
                // console.log(data)
            } catch (e) {
                console.trace({ e })
            }
        }
        setData([])
        setTotal(0)
        //更新了
        await getlist()
        setIsLoading(false)
    }
    useEffect(async () => {
        await tableSearch(page, pagesize, option)
    }, [props.admin, page, pagesize])

    const onSearch = async () => {
        pageInit(1, 10)
        setData([])
        await tableSearch(page, pagesize, option)
    }

    return (
        <div style={{ width: '100%' }} className="admin-tablelist-div" >
            <Row style={{ marginLeft: breakpoint ? 0 : '5%' }} >
                <Col sm={8} xs={20}>
                    <Tooltip placement="topRight" title={'search by stock/compony/plates'}>
                        <Input
                            value={searchValue}
                            onChange={(e) => { setSearchValue(e.target.value) }}
                            onPressEnter={async () => {
                                await onSearch()
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col sm={8} xs={4}>
                    <Button icon={<SearchOutlined />}
                        onClick={async () => {
                            await onSearch()
                        }}
                        style={{
                            height: '100%'
                        }}
                    />
                </Col>
            </Row>
            <Row>
                <Table
                    columns={columnsx}
                    dataSource={data}
                    style={{}}
                    bordered
                    className="admin-tablelist-table"
                    loading={isLoading}
                    scroll
                    size="small"
                    style={{
                        marginTop: '1rem',
                        maxWidth: 1000,
                        borderTop: '2px double #ac8901',
                        marginLeft: breakpoint ? 0 : '5%',
                    }}
                    title={() => <div style={{ color: '#aaa' }}>公司列表</div>}
                    footer={() => <div style={{ color: '#aaa' }}>共{total}条</div>}
                    pagination={{
                        total: total || 0,
                        simple: true,
                        onChange: async (page, pagesize) => {
                            pageInit(page, pagesize)
                        },
                        showQuickJumper: true,
                        defaultCurrent: +page + 1
                    }}
                    rowKey={(row) => { return row.code }}
                >
                </Table>
            </Row>
        </div>)
}
export default StockTable