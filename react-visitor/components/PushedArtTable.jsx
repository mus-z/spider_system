import { Table, Button, Tooltip, Typography, message } from 'antd';
import { connect } from 'react-redux'
import { useState, useMemo, useEffect } from 'react';
import axios from 'axios';
import dayjs from 'dayjs';
import { server } from '@/String';
import { useRouter } from 'next/router'
function PushedArtTable(props) {
    const { article_pushed, setArticlePushed,  user } = props
    const router = useRouter();
    const onSelectChange = selectedRowKeys => {
        //selectedRowKeys :Id[]
        // console.log('selectedRowKeys changed: ', selectedRowKeys);
        setselectedRowKeys(selectedRowKeys)
    };
    const [selectedRowKeys, setselectedRowKeys] = useState([])
    const [loading, setloading] = useState(false)
    const [data, setdata] = useState([])
    useEffect(() => {
        setdata(article_pushed)
    }, [article_pushed.length])
    let start = () => {
        setloading(true)
        axios({
            method: 'post',
            withCredentials: true,
            data: {
                user_id: user.Id,
                push_id_list: selectedRowKeys,
            },
            url: server + 'usersetpushlistviewed'
        }).then(res => {
            const { data } = res
            if (data.success) {
                setdata(data.pushlist)
                setArticlePushed(data.pushlist)
                message.success('清除成功')
            }
            // console.log(data)
            setloading(false)
        })
        setselectedRowKeys([])
        // ajax request after empty completing



    };
    const rowSelection = {
        selectedRowKeys,
        onChange: onSelectChange,
    };
    let hasSelected = useMemo(() => {
        return selectedRowKeys.length > 0
    }, [selectedRowKeys.length])
    const columns = [
        {
            title: '公司',
            dataIndex: 'short_name',
            render: (v, o) => {
                return (
                    <Tooltip title={o.stock_code}>
                        <Button
                            type="link"
                            onClick={() => {
                                router.push(`/company/${o.stock_code}`)
                            }}
                        >{v}
                        </Button>
                    </Tooltip>)
            }
        },
        {
            title: '公告',
            dataIndex: 'x',
            render: (v, o) => {
                return (
                    <div style={{
                        whiteSpace: 'break-spaces',
                        maxWidth: "30rem"
                    }}>
                        <Tooltip title={o.title}>
                            <Typography.Text ellipsis={true} style={{ width: '90%' }}> {o.title}</Typography.Text>
                        </Tooltip>
                    </div>
                )
            },
        },
        {
            title: '公告日期',
            dataIndex: 'notice_date',
            render: (v) => {
                return <span style={{ whiteSpace: 'nowrap' }}>{dayjs(+v).format('YYYY-MM-DD')}</span>
            },
            sorter: (a, b) => a.notice_date - b.notice_date
        },
        {
            title: 'Action',
            render: (v, o) => {
                return (
                    <Button
                        type="link"
                        onClick={() => {
                            router.push(`/article/${o.art_code}`)
                        }}
                    >查看
                    </Button>
                )
            }
        }
    ];

    return (<>
        <div>
            <div>
                <div style={{ marginBottom: 16 }}>
                    <Button type="primary" onClick={start} disabled={!hasSelected} loading={loading}>
                        已读所选
                        </Button>
                    {/* <span style={{ marginLeft: 8 }}>
                        {hasSelected ? `${selectedRowKeys.length} ` : ''}
                    </span> */}
                </div>
                <Table
                    rowSelection={rowSelection}
                    columns={columns}
                    dataSource={data}
                    rowKey={(item) => { return item.Id }}
                    pagination={false}
                />
            </div>
        </div>
    </>)
}
const mapState = state => ({
    user: state.user,
    article_pushed: state.article_pushed,
})

const mapDispatch = ({
    article_pushed: { setArticlePushed }
}) => ({
    setArticlePushed,
})
export default connect(mapState, mapDispatch)(PushedArtTable)