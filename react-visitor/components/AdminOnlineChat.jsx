import { useEffect, useState, useRef, } from 'react'
import { UserOutlined } from '@ant-design/icons'
import { Row, Col, Input, Avatar, Button, Tooltip } from 'antd'
import dayjs from 'dayjs'
import { connect } from 'react-redux'
import ChatList from '@/components/ChatList'
import { Picker } from 'emoji-mart'
import 'emoji-mart/css/emoji-mart.css'
import { SmileOutlined } from "@ant-design/icons"

function AdminOnlineChat(props) {
    const { msgList, setmsgList, user } = props
    const [text, settext] = useState("")
    const [showEmojiModal, setshowEmojiModal] = useState(false)
    const user_id = user.Id
    let searchEmoji = (emoji, e) => {
        // console.log("emoji, event=====", emoji, e)
        const native = emoji.native
        settext(text + native)
    }
    useEffect(() => {
        new Promise((res, rej) => {
            while (1) {
                if (window.socket) { res(); break; }
            }
        }).then(() => {
            window.socket.emit('getchat', {
                user_id,
            })
        })
    }, [])
    const handleMag = () => {
        window?.socket?.emit('chat', {
            user_id,
            text,
            msg_time: +dayjs()
        })
        settext('')
    }
    const ref = useRef(null)
    useEffect(() => {
        // console.log(ref)
        ref.current?.scrollIntoView()
    }, [msgList])
    return (
        <div className="user_admin_chat"
            style={{
                display: 'flex',
                alignItems: 'flex-start',
                justifyContent: "start",
                height: '40rem',
                minWidth: '50rem'
            }}
        >
            <aside style={{
                height: "100%",
                backgroundColor: '#001529',
                color: '#fff',
                minWidth: '10rem',
                padding: '2rem 2rem',
                cursor: 'pointer',
            }}>
                <Row>
                    <Col span={24} style={{
                        textAlign: 'center'
                    }}>
                        <Avatar size="large" ><UserOutlined /></Avatar>
                    </Col>
                    <Col span={24} style={{
                        textAlign: 'center'
                    }}>
                        <p>Admin</p>
                    </Col>
                </Row>
            </aside>
            <main style={{
                flex: 1,
                height: "100%",
                border: "1px solid #eaeaea",
                backgroundColor: '#eae3d399',
                boxShadow: 'rgb(0 0 0 / 8%) 3px 2px 9px 4px',
                display: 'flex',
                flexDirection: 'column'
            }}>
                <header
                    style={{
                        backgroundColor: "#f5f5f5",
                        padding: "0.9rem 2rem",
                        fontSize: "1.2rem",
                        fontWeight: 'bold',
                        color: '#888',
                        borderLeft: '1.5rem solid #495866',
                        borderBottom: '1px solid #eaeaea',
                        cursor: 'pointer'
                    }}
                >
                    与Admin的对话
                </header>
                <main
                    style={{
                        height: '25rem',
                        border: '2px solid #eaeaea',
                        overflow: 'auto',
                        backgroundColor: '#a0b1c0'
                    }}
                >
                    <Row>
                        <Col style={{ width: '100%', padding: '.5rem .5rem .2rem .5rem' }}>
                            <ChatList msgList={msgList} user_id={user.Id} user_nickname={user.user_nickname} />
                            <div id="msg_end" style={{ height: '0px', overflow: 'hidden' }} ref={ref}></div>
                        </Col>
                    </Row>

                </main>
                <div className="emoji_container" style={{ position: 'absolute' }}>
                    {showEmojiModal && <Picker
                        // set='apple'
                        emoji=''
                        showPreview={false}
                        onClick={(emoji, e) => searchEmoji(emoji, e)} />}
                </div>
                {/* emoji=''设置preview默认状态下不显示图片 */}
                <div style={{ padding: '.5rem', cursor: 'pointer', backgroundColor: '#ddd' }}
                    onClick={() => setshowEmojiModal(!showEmojiModal)}>
                    <Tooltip title="表情">
                        <SmileOutlined style={{ fontSize: 20, color: '#787878' }} />
                    </Tooltip>
                </div>
                <div
                    style={{
                        flex: 1,
                        backgroundColor: "#eee",
                        display: 'flex'
                    }}
                ><Input.TextArea style={{ height: "100%" }}
                    bordered
                    value={text}
                    onChange={(e) => { settext(e.target.value) }}
                    />
                    <Button style={{ height: "100%" }} type="primary"
                        onClick={() => {
                            handleMag()
                        }}
                    >发送</Button>
                </div>
            </main>
        </div>
    )
}
const mapState = state => ({
    user: state.user,
    msgList: state.msgList,
})

const mapDispatch = ({
    msgList: { setmsgList },
}) => ({
    setmsgList,
})
export default connect(mapState, mapDispatch)((AdminOnlineChat))
