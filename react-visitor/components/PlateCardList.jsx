import { useCallback, useEffect, useState } from 'react'
import { Input, Button,  message, List, Radio, Statistic, Tooltip, Divider, Row, Col, Card, Badge } from 'antd';
import axios from 'axios'
import { server } from '@/String';
import { withRouter, useRouter } from 'next/router'
import { connect } from 'react-redux';
import dayjs from 'dayjs';
function PlateCardList(props) {
    const { user, user_attention_plates, setAttentionPlates, user_id, article_pushed } = props
    const router = useRouter()
    const [loading, setloading] = useState(false)
    const [list, setlist] = useState([])
    const [last_tail_id, setLTI] = useState(null)
    const [searchStr, setsearchStr] = useState('')
    const [orderValue, setorderValue] = useState('company_count')
    const [total, settotal] = useState(0)
    const getData = async (prelist, last_tail_id) => {
        await axios({
            method: 'get',
            url: server + 'usergetplatelist',
            params: {
                last_tail_id,
                searchStr,
                user_id,
                orderValue,
            }
        }).then(res => {
            const { data } = res
            // console.log(data)
            if (data.success) {
                setlist([...prelist, ...data.listdata.list])
                setLTI(data.listdata.tail_id)
                if (data.total) settotal(data.total)
                // console.log(data.listdata)
            } else {
                message.error(data.msg)
            }
        }, e => {
            console.trace(e)
        })
    }
    useEffect(() => {
        getData([], null)
    }, [orderValue])
    const onLoadMore = async () => {
        setloading(true)
        await getData(list, last_tail_id)
        setloading(false)
    }
    const checkAttention = useCallback((plate_info_id) => {
        for (let item of user_attention_plates) {
            if (item.plate_info_id == plate_info_id) return true
        }
        return false
    }, [user_attention_plates, user.Id])
    const renewAttentionPlates = () => {
        axios({
            method: 'get',
            withCredentials: true,
            url: server + 'usergetattentionplatesbyid',
            params: {
                user_id: user.Id
            }
        }).then(res => {
            const {
                data
            } = res
            if (data.success) {
                // console.log(data)
                setAttentionPlates(data.attentions)
            }
        })
    }
    const pushedcount = (plate_name) => {
        let count = 0
        for (let item of article_pushed) {
            if (item.plates.indexOf(plate_name) > -1) count++
        }
        return count
    }
    const handleOnAttention = (type, plate_info_id) => {
        axios({
            method: 'get',
            withCredentials: true,
            url: server + 'userattentionplate',
            params: {
                type,
                user_id: user.Id,
                plate_info_id,
                attention_time: +dayjs(),
            }
        }).then(async (res) => {
            const { data } = res
            if (data.success) {
                message.success('成功' + (type ? '关注' : '取关'))
                //更新用户关注数据
                renewAttentionPlates()
                //更新某板块数据，其他的可以刷新更新
                let tmp = await Promise.all(list.map(async (item) => {
                    if (item.id == plate_info_id) {
                        const { plate_name } = item
                        let res = await axios({
                            withCredentials: true,
                            url: server + 'usergetplateattention',
                            params: {
                                plate_name,
                            },
                            method: 'get',
                        })
                        const { data } = res
                        if (data.success) {
                            let t = { ...item }
                            t.attentions_count = data.n
                            return t
                        } else {
                            return item
                        }
                    }
                    else {
                        return item
                    }
                }))
                setlist(tmp)
            }
            if (user_id) router.reload()
        })
    }
    const loadMore =
        !loading ? (
            <div
                style={{
                    textAlign: 'center',
                    marginTop: 6,
                    height: 32,
                    lineHeight: '32px',
                }}
            >
                <Button onClick={onLoadMore} block type="link" >︾</Button>
            </div>
        ) : null;
    return (
        <div >
            <Row gutter={20}>
                <Col sm={12} xs={12}>
                    <Tooltip title="search by plate">
                        <span>
                            <Input.Search style={{ float: 'right', width: '100%' }} placeholder="search by plate"
                                loading={loading}
                                value={searchStr}
                                onChange={e => { setsearchStr(e.target.value) }}
                                onSearch={() => {
                                    getData([], null)
                                    setlist([])
                                }}
                            />
                        </span>
                    </Tooltip>
                </Col>
                <Col sm={8} xs={6}>
                    <Radio.Group
                        optionType="button"
                        options={[
                            { label: '公司量', value: 'company_count' },
                            { label: '关注量', value: 'user_count' },
                        ]}
                        onChange={(e) => {
                            setorderValue(e.target.value)
                        }} value={orderValue}>
                    </Radio.Group>
                </Col>
            </Row>
            <Divider ><span style={{ color: "#aaa" }}>共{total}个</span></Divider>
            <Row sapn={24}>
                <List
                    loading={loading}
                    className="platecardlist-main"
                    grid={{
                        gutter: 8,
                        xs: 1,
                        sm: 1,
                        md: 2,
                        lg: 3,
                        xl: 3,
                        xxl: 4,
                    }}
                    style={{
                        width: '100%',
                        backgroundColor: '#ededed',
                        padding: '.5rem'
                    }}

                    loadMore={loadMore}
                    dataSource={list}
                    itemLayout="vertical"
                    renderItem={item => (
                        <div key={item.id}>
                            <List.Item
                            >
                                <Card

                                    title={<>
                                        <Badge count={pushedcount(item.plate_name)} size="small">
                                            <span
                                                style={{
                                                    cursor: 'pointer',
                                                    fontSize: '1.2rem',
                                                    color: '#d0a00e'
                                                }}
                                                onClick={() => {
                                                    router.push(`/plates/${item.id}`)
                                                }}>{item.plate_name}</span>
                                        </Badge>
                                    </>}
                                    hoverable
                                    bordered
                                    extra={<>
                                        {
                                            checkAttention(item.id) ?
                                                <Tooltip title="点击取关">
                                                    <Button
                                                        onClick={() => {
                                                            handleOnAttention(0, item.id)
                                                        }}
                                                        type="text">取关</Button> </Tooltip> :
                                                <Button
                                                    onClick={() => {
                                                        handleOnAttention(1, item.id)
                                                    }}
                                                    type="link">关注</Button>
                                        }

                                    </>}
                                    style={{ cursor: 'auto' }}
                                >
                                    <Row style={{ width: '100%' }}>
                                        <Col span={12}>
                                            <Statistic
                                                title="公司量"
                                                value={item.listed?.length || 0}
                                            />
                                        </Col>
                                        <Col span={12}>
                                            <Statistic
                                                title="关注量"
                                                value={item.attentions_count || 0}
                                            />
                                        </Col>
                                    </Row>
                                </Card>
                            </List.Item>
                        </div>
                    )}
                />
            </Row>
        </div>
    )
}
const mapState = state => ({
    user: state.user,
    user_attention_plates: state.user_attention_plates,
    article_pushed: state.article_pushed,
})

const mapDispatch = ({
    user_attention_plates: { setAttentionPlates },
}) => ({
    setAttentionPlates,
})

export default connect(mapState, mapDispatch)(withRouter(PlateCardList))