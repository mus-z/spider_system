import { Comment, Avatar, Popconfirm } from 'antd';
import dayjs from 'dayjs';
import { useMemo } from 'react';
export default function MyComment(props) {
    const { item, minutes, text, setansTo, floor, user_id, handledelete, getcomment } = props
    const t = useMemo(() => {
        let m = dayjs().diff(dayjs(+item.comment_time), 'minute')
        // console.log(m,dayjs().format(),dayjs(+item.comment_time).format())
        if (m < 1) {
            return "1分钟内"
        }
        if (m < 60) {
            return m + '分钟前'
        }
        if (m < 60 * 24) {
            return ((m / 60) | 0) + '小时前'
        }
        if (m < 60 * 24 * 30) {
            return dayjs(+item.comment_time).format('YYYY-MM-DD')
        }
        if (m < 60 * 24 * 30 * 12) {
            return dayjs(+item.comment_time).format('YYYY-MM-DD')
        }
        return dayjs(+item.comment_time).format('YYYY-MM-DD')

    }, [minutes])
    const handleReply = (item) => {
        setansTo(item)
        text.current.focus()
        // console.log(item)
    }
    return (
        <Comment
            style={{
                padding: '0 .2rem',
                borderTop: '1px solid #d9d9d9'
            }}
            author={
                <span
                    style={{
                        cursor: 'pointer',
                        fontSize: "1rem",
                        color: '#444',
                        fontWeight: "bolder",
                    }}
                    onClick={() => {
                        handleReply(item)
                    }}
                >
                    {floor && <span style={{ color: '#999', fontWeight: "normal", fontSize: ".8rem" }}>{floor}楼 </span>}
                    {item.user_nickname}
                    {item.to_id &&
                        <span style={{ color: '#999', fontWeight: "normal", }}>(to:{item.to_name})</span>
                    }
                </span>
            }
            avatar={<Avatar
                onClick={() => {

                }}
                style={{ backgroundColor: '#f56a00', verticalAlign: 'middle', border: '1px solid #faad14' }}
                src={item.user_avator}
                alt={item.user_nickname}
            >{item.user_nickname}</Avatar>}
            content={
                <div
                    style={{
                        whiteSpace: 'pre-warp',
                        color: '#666'
                    }}>
                    <span
                    style={{
                        whiteSpace:'break-spaces'
                    }}
                    >{getcomment(item)}</span>
                </div>}
            datetime={t}
            actions={!item.disabled&&
                [<>
                    {<span key="replyto"
                        onClick={() => {
                            handleReply(item)
                        }}
                    >回复</span>}
                </>,
                <>
                    {user_id == item.user_id && !item.disabled&&
                        <Popconfirm
                        title="确定删除评论？"
                        onConfirm={()=>{
                            handledelete(item, user_id)
                        }}
                        >
                            <span key="delete"
                            >删除</span>
                        </Popconfirm>}
                </>,
                ]}
        >
            {props.children}
        </Comment>

    )
}