import {
    VerticalAlignTopOutlined
} from '@ant-design/icons';
import { Button } from 'antd';

function BackTop(props) {
    //backtop平滑滚动到顶部
    let { level } = props
    if (!level) level = 1
    if (level > 10) level = 10
    const scrollTop = () => {
        let fn = function () {
            // 页面滚动的距离
            let scrollTop = window.document.body.scrollTop;
            // 控制滚动速率
            let speed = Math.floor(-scrollTop / 10) * (level || 1);
            window.document.body.scrollTop = scrollTop + speed;
            if (scrollTop > 1) {
                timer = window.requestAnimationFrame(fn)
            }
        }
        let timer = window.requestAnimationFrame(fn)
    }
    return (<div className="backtop">
        <Button
            size="large"
            onClick={scrollTop}
            type="ghost" shape="circle" icon={<VerticalAlignTopOutlined />} />
        <style>{`
        .backtop{
            position:fixed;
            bottom:1rem;
            left:1rem;
        }
        `}</style>
    </div>)
}
export default BackTop