import styles from '@/styles/components/Header.module.css'
import { connect } from 'react-redux'
import { useEffect, useMemo, useRef, useState } from 'react'
import { Avatar, Button, message, Badge, Tooltip, notification } from 'antd';
import axios from 'axios'
import { withRouter, useRouter } from 'next/router'
import { Menu, Dropdown } from 'antd';
import { socket_io, initSocket } from '@/socketIO'
import JaccardButton from '@/components/JaccardButton'
import {
    MessageTwoTone
} from '@ant-design/icons';
import {server, host} from '@/String'
function Header(props) {
    const { user, setUser, needlogin,
        setAttentionPlates, setArticlePushed, article_pushed, setAdminOnlieState, admin_online, setmsgList
    } = props
    const user_id = user.Id
    const location = useMemo(() => {
        const route = props.router.route
        // console.log(route.split('/')[1])
        // const l = route.split('/')[1] ? route.split('/')[1] : '/'
        return '/' + route.split('/')[1]
    }, [props.router.route])
    const [menuCurrent, setmenuCurrent] = useState(location)
    const router = useRouter()
    const handleClick = e => {
        const { key } = e
        // console.log('click ', e);
        // console.log(router)
        router.push(key)
        // setmenuCurrent(key)
    };
    useEffect(() => {
        //  console.log(props)
        axios({
            url: server + 'usercklogin',
            method: 'get',
            withCredentials: true,
        }).then(res => {

            // console.log(res)
            const { data } = res
            if (data?.success) {
                const user_id = data.data.Id
                setUser(data.data)
                axios({
                    method: 'get',
                    withCredentials: true,
                    url: server + 'usergetattentionplatesbyid',
                    params: {
                        user_id
                    }
                }).then(res => {
                    const { data } = res
                    if (data.success) {
                        // console.log(data)
                        setAttentionPlates(data.attentions)
                    }
                })

                /*********************************************************/
                //在线状态加socket
                // socket.current = socket_io('127.0.0.1:7001/visitor', {
                //     query: {
                //         room: 'visitor',
                //         user_id
                //     },
                //     // autoConnect: false,//默认不开启
                //     transports: ['websocket'],
                // })
                window.socket = socket_io(host+':7001/visitor', {
                    query: {
                        room: 'visitor',
                        user_id
                    },
                    autoConnect: false,//默认不开启
                    transports: ['websocket'],
                })
                let socket = window.socket
                // console.log(window.socket)
                initSocket(socket, user_id, setArticlePushed, setAdminOnlieState, setmsgList)
                // socket.removeAllListeners(user_id)
                // socket.on('connect', () => {
                //     // console.log(socket)
                //     socket.emit('server', {
                //         user_id
                //     })
                //     socket.on(user_id, (data) => {
                //         console.log(data)
                //         if (data["pushlist"]) {//推送的未读
                //             const { pushlist } = data
                //             if (pushlist.length > 0) {
                //                 //表示都是未读的
                //                 setArticlePushed(pushlist)
                //             }
                //         }
                //         if (data['admin_online'] == true || data['admin_online'] == false) {//管理员在线状态
                //             setAdminOnlieState(data['admin_online'])
                //         }
                //     })
                // })
                // socket.on('disconnect', (s) => {
                //     socket.removeAllListeners(user_id)
                //     console.log('disconnect', s)
                // })
                socket.connect()

                /*********************************************************/
            } else {
                // console.log({ needlogin })
                if (needlogin) { router.replace('/login'); message.warning('请先登录，正在返回登录页面') }
            }
        }, e => {
            message.warn(e + '')
        })
        return () => {
            if (window?.socket?.removeAllListeners) window?.socket.removeAllListeners(user_id)
            window?.socket?.disconnect()
        }
    }, [])

    useEffect(() => {
        setmenuCurrent(location)
    }, [location])
    const onLogout = () => {
        axios({
            url: server + 'userlogout',
            method: 'get',
            withCredentials: true,
        }).then(res => {
            console.log(res)
            const { data } = res
            if (data?.success) {
                window?.socket?.disconnect()
                message.success('退出成功')
                setUser({})
                if (needlogin) { router.replace('/login') }
            }
        }, e => {
            message.warn(e + '')
        })
    }
    const menu = (
        <Menu theme="light"
            forceSubMenuRender
            onClick={(item) => {
                if (item.key == 'LOGOUT') {
                    onLogout()
                } else {
                    router.push(item.key)
                }

            }}
        >
            <Menu.Item key={'/usercenter/plates'} className="user-menu-item">
                <span >
                    我的关注
                </span>
            </Menu.Item>
            <Menu.Item key={'/usercenter/viewhistory'} className="user-menu-item" >
                <span>浏览记录</span>
            </Menu.Item>
            <Menu.Item key={'/usercenter/pushed'} className="user-menu-item" >
                <Badge count={article_pushed.length} overflowCount={99} size="small"
                    offset={[10, 0]}
                >
                    <span>订阅公告 </span>
                </Badge>
            </Menu.Item>
            <Menu.Item key={'/usercenter/calladmin'} className="user-menu-item" >
                <Badge
                    dot
                    color={admin_online ? "green" : "gray"}
                ><span>联系管理员</span></Badge>
            </Menu.Item>
            <Menu.Item key={"LOGOUT"} className="user-menu-item" >
                <span>登出</span>
            </Menu.Item>

            <style>{`
                .user-menu-item:hover{
                    background-color:#c7a519;
                    color:#fff;
                }
                ul.ant-dropdown-menu{
                    min-width:10rem;
                }
            `}
            </style>
        </Menu>
    );
    return (
        <header className={styles.header} style={{ ...props.style }}>
            <aside style={{ ...props.asideStyle }} >
                <span
                    style={{
                        cursor: 'pointer',
                        fontSize: '1.5rem'
                    }}
                    onClick={() => {
                        router.push('/')
                    }}
                >
                    east money
                </span>
            </aside>
            {props.children}
            {user?.user_name ?
                <Menu
                    onClick={handleClick}
                    selectedKeys={[menuCurrent]}
                    mode="horizontal"
                    style={{
                        backgroundColor: 'inherit',
                        color: '#eee',
                        flex: '0 1 50%',
                        textAlign: 'center'
                    }}
                >
                    <Menu.Item key="/" >
                        近期公告
                    </Menu.Item>
                    <Menu.Item key="/company" >
                        上市公司
                    </Menu.Item>
                    <Menu.Item key="/plates" >
                        板块信息
                    </Menu.Item>
                    <Menu.Item key="/usercenter" >
                        用户中心
                    </Menu.Item>
                </Menu> : null}
            <div>
                {user?.user_name && (
                    <Tooltip title={admin_online ? "管理员在线" : "管理员离线"}>
                        <Button
                            onClick={() => {
                                admin_online ? router.push("/usercenter/calladmin") :
                                    message.info('管理员不在线呦~请等工作时间再联系叭！')
                            }}
                            type="link" style={{ color: '#fff' }}
                        ><MessageTwoTone
                                twoToneColor={admin_online ? "#caac2e" : "gray"}
                            />
                        </Button>
                    </Tooltip>
                )
                }
                {user?.user_name &&
                    //用户主动去拿推荐文章，然后返回并跳转到页面
                    <JaccardButton router={router} user_id={user_id} />
                }
            </div>
            {user?.user_name ?
                <Dropdown overlay={menu} placement="bottomRight" className="avator-dropdown">
                    <div>
                        <span
                            style={{
                                cursor: 'pointer',
                                justifySelf: 'flex-end'
                            }}
                        >
                            <Badge count={article_pushed.length} overflowCount={99} size="small"
                                offset={[0, "90%"]}
                            >
                                <Avatar
                                    size={{ xs: 40, sm: 40, md: 40, lg: 50, xl: 50, xl: 50, xxl: 50 }}
                                    src={user?.user_avator}
                                    style={{ backgroundColor: '#f56a00', verticalAlign: 'middle', border: '2px solid #fff' }}
                                >{user?.user_nickname || 'U'}
                                </Avatar>
                            </Badge>
                        </span>
                        <style global="true">{`
                        .ant-dropdown-menu{
                            padding:0
                        }
                        .ant-dropdown-menu-item{
                            border-top: 1px solid #d9d9d9;
                        }
                        `}</style>
                    </div>

                </Dropdown>
                :
                <div>
                    <Button
                        size="large"
                        style={{
                            color: '#eee',
                        }}
                        type="link"
                        onClick={() => {
                            router.push('/login')
                        }}>登录
                        </Button>
                </div>
            }
        </header>
    )
}
const mapState = state => ({
    user: state.user,
    user_attention_plates: state.user_attention_plates,
    article_pushed: state.article_pushed,
    admin_online: state.admin_online,
    msgList: state.msgList,
})

const mapDispatch = ({
    user: { setUser, changeProps },
    user_attention_plates: { setAttentionPlates, pushAttentionPlates },
    article_pushed: { setArticlePushed },
    admin_online: { setAdminOnlieState },
    msgList: { setmsgList },
}) => ({
    setUser: (obj) => setUser(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setAttentionPlates,
    pushAttentionPlates,
    setArticlePushed,
    setAdminOnlieState,
    setmsgList,
})
export default connect(mapState, mapDispatch)(withRouter(Header))