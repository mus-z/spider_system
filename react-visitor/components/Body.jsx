import Header from '@/components/Header'
import Footer from '@/components/Footer'
import styles from '@/styles/Home.module.css'
import { connect } from 'react-redux'
import { Empty } from 'antd'
import BackTop from '@/components/BackTop'
function Body(props) {
  const { needlogin, user } = props
  return (<>
    <BackTop level={1}/>
    <div className={styles.container}>
      <Header needlogin={needlogin} ></Header>
      <main className={styles.main}>
        <main className={styles.main2}>
          {
            (!needlogin || user.Id) ?
              <>{props.children}</>
              : <Empty></Empty>
          }
        </main>
      </main>
      {/* <footer className={styles.footer}>
        <Footer />
      </footer> */}
    </div>
  </>)
}
const mapState = state => ({
  user: state.user,
})

const mapDispatch = ({ user: { setUser, changeProps } }) => ({
  setUser: (obj) => setUser(obj),
  changeProps: (key, value) => changeProps({ key, value }),
})
export default connect(mapState, mapDispatch)(Body)