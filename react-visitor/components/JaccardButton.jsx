import { useRef, useState } from 'react'
import { Button, Tooltip, notification, message } from 'antd';
import {
    SyncOutlined
} from '@ant-design/icons';
import dayjs from 'dayjs'
import axios from 'axios'
const server = require('@/String').server
function JaccardButton(props) {
    const { router, user_id } = props
    const countdown = useRef(0)
    const [iconSpin, seticonSpin] = useState(false)

    const countdownNorify = (data, route, m, s, ms, t) => {
        notification.open({
            key: 'countdown',
            message: (
                <Tooltip
                    title="点击直接跳转"
                >
                    <a
                        onClick={() => { clearInterval(t) }}
                        href={route}
                    >{data.art.title}</a>
                </Tooltip>),
            description: (
                <div>
                    <p>{countdown.current}秒后跳转到文章，如不想跳转请关闭此tip</p>
                    <p>不好意思让您等了,
                        {m ? (<span style={{ color: '#888' }}>{m}分 </span>) : null}
                        {s ? (<span style={{ color: '#888' }}>{s - m * 60}秒 </span>) : null}
                        {ms && (<span style={{ color: '#888' }}>{ms - s * 1000}毫秒 </span>)}
                    </p>
                </div>),
            onClose: () => {
                clearInterval(t)
            },
            duration: 3.5
        })
    }
    return (
        <Tooltip title="点击推荐">
            <Button type="link" style={{ color: '#fff' }}
                disabled={iconSpin}
                onClick={async () => {
                    seticonSpin(true)
                    try {
                        let res = await axios({
                            method: 'get',
                            withCredentials: true,
                            params: {
                                user_id,
                                start_time: +dayjs()
                            },
                            url: server + "getrecommendartbyuserid"
                        })
                        const { data } = res
                        if (data.success) {
                            const route = `/article/${data.art.art_code}`
                            console.log(data)
                            countdown.current = 3
                            const start = dayjs(+data.start_time)
                            const m = dayjs().diff(start, 'm')
                            const s = dayjs().diff(start, 's')
                            const ms = dayjs().diff(start, 'ms')
                            let t = setInterval(() => {
                                let cur = countdown.current - 1
                                countdown.current = cur
                                countdownNorify(data, route, m, s, ms, t)
                                if (cur <= 0) {
                                    console.log(t)
                                    router.push(route)
                                    clearInterval(t)
                                }

                            }, 1000);
                            countdownNorify(data, route, m, s, ms, t)
                        } else {
                            message.warn(data.msg || 'ERROR')
                        }
                    } catch (e) {
                        console.trace(e)
                    }
                    seticonSpin(false)
                }}
            >
                <SyncOutlined spin={iconSpin} />
            </Button>
        </Tooltip>
    )
}
export default JaccardButton