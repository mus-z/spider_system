import { useEffect, useState } from 'react'
import { Input, Button,  message, List, Skeleton, Typography, Tooltip, Divider, Row, Col, Badge } from 'antd';
import axios from 'axios'
import { server } from '@/String';
import { withRouter, useRouter } from 'next/router'
import { connect } from 'react-redux';
import dayjs from 'dayjs';
function LoadMoreList(props) {
    const { user, stockcode, platename, article_pushed } = props
    const router = useRouter()
    const [loading, setloading] = useState(false)
    const [list, setlist] = useState([])
    const [last_tail_code, setLTC] = useState(null)
    const [searchStr, setsearchStr] = useState('')
    const [last_crawler_time, setLCT] = useState(0)

    let ismain = (!stockcode) && (!platename)
    const getData = async (prelist, last_tail_code) => {
        setloading(true)
        await axios({
            method: 'get',
            url: server + 'usergetartlist',
            params: {
                last_tail_code,
                searchStr,
                stockcode,
                platename
            }
        }).then(res => {
            const { data } = res
            // console.log(data)
            if (data.success) {
                setlist([...prelist, ...data.listdata.list])
                setLTC(data.listdata.tail_code)
                if (data.listdata.last_crawler_time) setLCT(+data.listdata.last_crawler_time)
                // console.log(data.listdata)
            } else {
                message.error(data.msg)
            }
            setloading(false)
        }, e => {
            console.trace(e)
            setloading(false)
        })
    }
    useEffect(() => {
        getData(list, null)
    }, [])
    const onLoadMore = async () => {
        await getData(list, last_tail_code)
        window.dispatchEvent(new Event('resize'))
    }
    const ispushed = (art_code) => {
        let t = 0
        article_pushed.forEach(item => {
            if (item.art_code == art_code) t++
        })
        return t
    }
    const r_list = () => {
        return list.sort((a, b) => {
            if(ispushed(b.art_code) != ispushed(a.art_code))return ispushed(b.art_code) - ispushed(a.art_code)
            else return b.notice_date - a.notice_date
        })
    }
    const loadMore =
        !loading ? (
            <div
                style={{
                    textAlign: 'center',
                    marginTop: 12,
                    height: 32,
                    lineHeight: '32px',
                }}
            >
                <Button onClick={onLoadMore} block type="link" >︾</Button>
            </div>
        ) : null;
    return (
        <div >
            {ismain ? <Row>
                <Col sm={12} xs={18}>
                    <p style={{ color: '#aaa' }}>上次修改时间：{dayjs(+last_crawler_time).format('YYYY/MM/DD HH:mm:ss')}</p>
                </Col>
                <Col sm={12} xs={18}>
                    <Tooltip title="search by title stock company">
                        <span>
                            <Input.Search style={{ float: 'right', width: '100%' }} placeholder="search by title/stock/company"
                                loading={loading}
                                value={searchStr}
                                onChange={e => { setsearchStr(e.target.value) }}
                                onSearch={() => {
                                    getData([], null)
                                    setlist([])
                                }}
                            />
                        </span>
                    </Tooltip>
                </Col>
            </Row> : null}
            <List
                itemLayout="horizontal"
                loadMore={loadMore}
                loading={loading}
                dataSource={r_list()}
                style={{
                    backgroundColor: '#fff'
                }}
                header={<div style={{ color: '#aaa' }}>公告列表</div>}
                renderItem={item => (
                    <div key={item.id} >
                        <Divider></Divider>

                        <List.Item
                            actions={[<Badge count={ispushed(item.art_code)} dot><a
                                onClick={() => {
                                    if (!user?.Id) {
                                        router.push(`/login`)
                                        message.info('请登录')
                                        return;
                                    }
                                    router.push(`/article/${item.art_code}`)
                                }}
                            >查看</a></Badge>]}
                            style={{
                                backgroundColor: ispushed(item.art_code) ? '#d9363e26' : '#fff',
                                padding:'1rem',
                            }}
                        >
                            <Skeleton loading={loading} active>
                                <List.Item.Meta

                                    title={<a
                                        href={`/company/${item.stock_code}`}
                                        onClick={(e) => {
                                            e.preventDefault()
                                            if (!user.Id) {
                                                router.push(`/login`)
                                                message.info('请登录')
                                                return;
                                            }
                                            router.push(`/company/${item.stock_code}`)
                                        }} >{item.short_name}</a>}
                                    description={<>{item.stock_code}</>}
                                />
                                <Tooltip title={item.title} placement="topLeft" >
                                    <div style={{ width: '70%', display: 'flex', alignItems: 'flex-start', flexDirection: 'column' }}>
                                        <Typography.Text ellipsis={true} style={{ width: '90%' }}> {item.title}</Typography.Text>
                                        <Typography.Text ellipsis={true} > 公告日期：{dayjs(+item.notice_date).format('YYYY-MM-DD')}</Typography.Text>
                                    </div>
                                </Tooltip>
                            </Skeleton>

                        </List.Item>
                    </div>
                )}
            />
            {loading ? <List
                dataSource={(() => {
                    let n = 5
                    let tmp = []
                    for (let i = 0; i < n; i++) {
                        tmp.push({
                            Id: 'vitrulSkeleton' + i
                        })
                    }
                    return tmp
                })()}
                renderItem={item => (
                    <div key={item.Id}>
                        <List.Item
                        >
                            <Skeleton loading={loading} active>
                            </Skeleton>

                        </List.Item>
                    </div>
                )}
            /> : null}
        </div>
    )
}
const mapState = state => ({
    user: state.user,
    article_pushed: state.article_pushed,
})

const mapDispatch = ({ user: { setUser, changeProps } }) => ({
    setUser: (obj) => setUser(obj),
    changeProps: (key, value) => changeProps({ key, value }),
})

export default connect(mapState, mapDispatch)(withRouter(LoadMoreList))