import axios from "axios"
const server = require('@/String').server
export const user = {
  state: {}, // 默认变量
  reducers: {
    // 修改state的纯函数
    setUser(state, newState) {
      //console.log({state,newState})
      return newState
    }
  },
  effects: (dispatch) => ({
    // handle state changes with impure functions.
    // 处理异步action
    async testAsync(payload, rootState, k) {
      console.log({
        payload,
        rootState,
        k
      }) //只能传两个参数过来
    }
  })
}
export const user_attention_plates = {
  //用户关注板块
  state: [],
  reducers: {
    setAttentionPlates(state, newState) {
      return newState
    },
  },
}
export const article_pushed = {
  //用户被推送文章
  state: [],
  reducers: {
    setArticlePushed(state, newState) {
      return newState
    },
  },
}
export const admin_online = {
  //管理员admin是否在线
  state: false,
  reducers:{
    setAdminOnlieState(state,newstate){
      return newstate
    },
  }
}
export const msgList = {
  state:[],
  reducers:{
    setmsgList(state,newstate){
      return newstate
    },
  }
}