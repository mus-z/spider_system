import io from 'socket.io-client'
export const socket_io = io
import {
    notification
} from 'antd';
import {server, host} from '@/String'
export let socket = io(host+':7001/visitor', {
    // query: {
    //     room: 'demo',
    //     userId: `client_${Math.random()}`,
    // },
    autoConnect: false, //默认不开启
    transports: ['websocket'],
})
export let initSocket = (socket, user_id, setArticlePushed, setAdminOnlieState, setmsgList) => {
    socket.removeAllListeners(user_id)
    socket.removeAllListeners(user_id + 'msgList')
    socket.on('connect', () => {
        console.log(socket)
        socket.emit('server', {
            user_id
        })
        socket.on(user_id, (data) => {
            // console.log(data)
            if (data["pushlist"]) { //推送的未读
                const {
                    pushlist
                } = data
                if (pushlist.length > 0) {
                    //表示都是未读的
                    setArticlePushed(pushlist)
                }
            }
            if (data['admin_online'] == true || data['admin_online'] == false) { //管理员在线状态
                setAdminOnlieState(data['admin_online'])
            }
        })
        socket.on(user_id + 'msgList', function (data) {
            // console.log(data)
            if (data['msgList']) {
                setmsgList(data['msgList'])
                if (data.append) {
                    notification.success({
                        message: data.append,
                        key: data.append,
                        style: {
                            opacity: '0.8'
                        }
                    })
                }
            }
        })
    })
    socket.on('disconnect', (s) => {
        socket.removeAllListeners(user_id)
        console.log('disconnect', s)
    })
}
export default socket