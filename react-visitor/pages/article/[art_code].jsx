import Head from '@/components/Head'
import { useEffect, useState } from 'react'
import { message, Tooltip, Divider, Row, Col, Typography, Statistic} from 'antd';
import axios from 'axios'
import { withRouter, useRouter } from 'next/router'
import { connect } from 'react-redux'
import { server } from '@/String';
import Body from '@/components/Body'
import dayjs from 'dayjs'
import { EyeFilled } from '@ant-design/icons';
import CommentBoard from "@/components/CommentBoard"
const { Title, Text } = Typography;

function ArticlePage(props) {
    const router = useRouter()
    const { art, user, view, article_pushed, setArticlePushed } = props
    let artObj = art
    useEffect(() => {
        if (!art.Id) {
            //未查到公司
            message.error('无此公告')
            router.back()
        } else {
            // console.log(art, user)
            if (user.Id) axios({
                method: 'get',
                url: server + 'userviewart',
                withCredentials: true,
                params: {
                    user_id: user.Id,
                    art_code: art.art_code,
                    view_time: +dayjs(),
                }
            }).then(res => {
                const { data } = res
                // console.log(data)
                if (!data.success) {
                    message.error('插入记录失败')
                } else {
                    let tmp = []
                    for (let item of article_pushed) {
                        if (item.art_code != art.art_code) {
                            tmp.push(item)
                        }
                    }
                    setArticlePushed(tmp)
                }

            })
        }
    }, [user.Id, art.Id])
    return (<>
        <Head title={`vital_article-${art.short_name}-公告`} ></Head>
        <Body needlogin>
            <div className="user-art-main">
                <Row gutter={2}>
                    <Col span={24} >
                        <Typography>
                            <Row gutter={10}>
                                <Col span={22}>
                                    <Tooltip
                                        placement="topLeft"
                                        title={() => {
                                            return (
                                                <a href={artObj.url} target="_blank" >点击回原文</a>
                                            )
                                        }}
                                    ><Title level={2}>{artObj.title}</Title></Tooltip>
                                </Col>
                                <Col span={4}>
                                    <Statistic title="浏览量" value={view} valueStyle={{ color: '#c7a519' }} prefix={<EyeFilled />} />
                                </Col>
                            </Row>

                            <Divider orientation="left"><a href={`/company/${artObj.stock_code}`} >{artObj.short_name}</a>--{dayjs(+artObj.notice_date).format('YYYY-MM-DD')}</Divider>
                            <div style={{ whiteSpace: 'pre-line', padding: '1rem', paddingTop: 0 }}>
                                <Text style={{}} >{artObj.text}</Text>
                            </div>
                        </Typography>
                    </Col>
                </Row>
            </div>
            <CommentBoard user_id={user.Id} art_code={art.art_code} />


        </Body>
    </>)
}
export async function getServerSideProps(context) {
    const { params } = context
    const { art_code } = params
    const res = await axios({
        url: server + 'usergetarticlebystock',
        method: 'get',
        withCredentials: true,
        params: {
            art_code
        }
    })
    // console.log(params, res.data)
    if (res.data.success) {
        return {
            props: {
                art: res.data.art,
                view: res.data.view,
            }
        }
    } else {
        return {
            redirect: {
                permanent: false,
                destination: "/",
            },
            props: {
                art: null
            }
        }
    }
}
const mapState = state => ({
    user: state.user,
    article_pushed: state.article_pushed,
})

const mapDispatch = ({ user: { setUser, changeProps }, article_pushed: { setArticlePushed } }) => ({
    setUser: (obj) => setUser(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setArticlePushed,
})
export default connect(mapState, mapDispatch)(withRouter(ArticlePage))