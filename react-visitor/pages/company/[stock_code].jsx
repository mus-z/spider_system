import Head from '@/components/Head'
import Body from '@/components/Body'
import { useEffect, useState } from 'react'
import { message, Tooltip, Divider, Tag, Collapse } from 'antd';
import axios from 'axios'
import { withRouter, useRouter } from 'next/router'
import { connect } from 'react-redux'
import { server } from '@/String';
import LoadMoreList from '@/components/LoadMoreList'
const { Panel } = Collapse;
function CompanyPage(props) {
    const { listed } = props
    const { code: stockcode, plates } = listed
    const [scope, setscope] = useState([])
    const router = useRouter()
    useEffect(() => {
        if (!listed) {
            //未查到公司
            message.error('无此公司')
            router.back()
        } else {
            const tmp = JSON.parse(listed.scope)
            const scope = {}
            for (let i = 0; i < tmp.length; i++) {
                const cur = tmp[i]
                let { key, value } = cur
                if (scope[key]) scope[key].push(value)
                else scope[key] = [value,]
            }
            setscope(scope)
        }
        // console.log(props.listed)
    }, [])
    return (<>
        <Head title={`vital_article-公司-${listed.company}`} ></Head>
        <Body needlogin>
            <div className="user-stock-main">
                <h1><Tooltip
                    title={() => (<a href={`http://data.eastmoney.com/stockdata/${stockcode}.html`} target="_blank" >东方财富</a>)}
                >({stockcode}){listed.company}
                </Tooltip>
                                --
                                <Tooltip title={() => {
                        switch (listed.type) {
                            case "SH": {
                                return '沪市'
                            }
                            case 'SZ': {
                                return '深市'
                            }
                        }
                    }} >
                        <a onClick={() => {
                            console.log(listed.type)
                            // props.history.push({
                            //     pathname: '/admin/stocklist',
                            //     state: {
                            //         searchValue: listed.type
                            //     }
                            // })
                        }}>
                            {listed.type}
                        </a></Tooltip></h1>
                <Divider></Divider>
                <div className="user-stock-plates">
                    {
                        plates && plates.map((item, index) => {
                            return (
                                <Tag key={index}
                                    color="volcano"
                                    style={{
                                        marginBottom: '4px'
                                    }}
                                >
                                    <span
                                        style={{
                                            cursor: 'alias'
                                        }}
                                        onClick={() => {
                                            router.push(`/plates/${item.p_id}`)
                                            console.log(item.stock_plate)
                                        }}>{item.stock_plate}</span>
                                </Tag>
                            )
                        })

                    }
                </div>
                <Divider></Divider>
                <Collapse bordered={false}
                    defaultActiveKey={['loadmorelist']}
                >
                    {
                        listed && scope && Object.keys(scope).map((key, index) => {
                            const value = scope[key]
                            return (
                                <Panel header={key} key={"" + key + index}>
                                    {
                                        value.map((item, i) => {
                                            return <p style={{ textIndent: '2em' }} key={"" + key + index + ',' + i} >{item}</p>
                                        })
                                    }
                                </Panel>
                            )
                        })
                    }
                    <Panel header={'公司公告'} key="loadmorelist"
                    >
                        <LoadMoreList stockcode={stockcode} />
                    </Panel>
                </Collapse>
            </div>
        </Body>
    </>)
}
export async function getServerSideProps(context) {
    const { params } = context
    const { stock_code } = params
    const res = await axios({
        url: server + 'usergetcomponybystock',
        method: 'get',
        withCredentials: true,
        params: {
            stock_code
        }
    })
    // console.log(params, res.data)
    if (res.data.success) {
        return {
            props: {
                listed: res.data.listed,
            }
        }
    } else {
        return {
            redirect: {
                permanent: false,
                destination: "/company",
            },
            props: {
                listed: null
            }
        }
    }

}
const mapState = state => ({
    user: state.user,
})

const mapDispatch = ({ user: { setUser} }) => ({
    setUser: (obj) => setUser(obj),
})
export default connect(mapState, mapDispatch)(withRouter(CompanyPage))