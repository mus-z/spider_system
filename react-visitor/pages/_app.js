import '../styles/globals.css'
import 'antd/dist/antd.min.css'
import { Provider } from 'react-redux'
import store from '../redux/store'
import React from 'react'
import { PageTransition } from 'next-page-transitions'
import { throttle } from 'throttle-debounce';
import { useEffect, useRef, useState } from 'react'
function MyApp({  Component, pageProps, router }) {
  const scrollTop = useRef(0)
  useEffect(() => {
    scrollTop.current=(window.document.body.scrollTop)
    let scrollfn = throttle(300, true, function (e) {
      scrollTop.current= Math.round(window.document.body.scrollTop)
      //console.log(scrollTop)
    })
    window.addEventListener('scroll', scrollfn, true)
    return () => {
      window.removeEventListener('scroll', scrollfn, true)
    }
  }, [])
  return (
    <Provider store={store}>
      <PageTransition timeout={300} classNames="page-transition">
        <Component {...pageProps} key={router.route} scrollTop={scrollTop}/>
      </PageTransition>
        <style jsx global>{`
          .page-transition-enter {
            opacity: .8;
          }
          .page-transition-enter-active {
            opacity: 1;
            transform: translateX(0);
            transition: all 500ms;
          }
          .page-transition-exit {
            opacity: .6;
          }
          .page-transition-exit-active {
            opacity: 0;
            transition: opacity 500ms;
            transform: translateX(0);
          }
        `}</style>
    </Provider>
  )
}
export default (MyApp)
