import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import PlateCardList from '@/components/PlateCardList'
import Body from '@/components/Body'
function PlateList(props) {
  return (<>
    <Head title={'vital_article-板块信息'} ></Head>
    <div className={styles.container}>
      <Body needlogin >
            <PlateCardList />
      </Body>
    </div>
  </>)
}
export default (PlateList)