import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import { useEffect, useState } from 'react'
import { Input, Button,  message, Avatar, Divider, Row, Col, Typography, Upload, } from 'antd';
import axios from 'axios'
import Body from '@/components/Body'
import { connect } from 'react-redux'
import dayjs from 'dayjs'
import ImgCrop from "antd-img-crop";
import {
  LoadingOutlined,
  PlusOutlined
} from "@ant-design/icons";
const server = require('@/String').server
function Usercenter(props) {
  const { user, setUser } = props
  const [loading, setLoading] = useState(true);
  const [nickName, setnickName] = useState(user?.user_nickname)
  const [password, setpassword] = useState(user?.user_password)
  const [eMail, seteMail] = useState(user?.user_email)
  const [introduce, setintroduce] = useState(user?.user_introduce)
  const [imageUrl, setImageUrl] = useState(user?.user_avator);
  const obj = {
    id: user?.id || user?.Id,
    Id: user?.id || user?.Id,
    user_name: user?.user_name,
    user_password: password,
    user_email: eMail,
    user_avator: imageUrl,
    user_introduce: introduce,
    user_nickname: nickName,
    user_creattime: user?.user_creattime
  }
  useEffect(() => {
    initvalue(user)
    // console.log(user, obj)
  }, [user.Id])
  function initvalue(user = user) {
    setnickName(user?.user_nickname)
    setpassword(user?.user_password)
    seteMail(user?.user_email)
    setintroduce(user?.user_introduce)
    setImageUrl(user?.user_avator)
  }

  const flexCentor = {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }


  const onFinish = () => {
    // console.log(obj)
    axios({
      method: 'post',
      url: server + 'userupdate',
      withCredentials: true,
      data: obj
    }).then(res => {
      const { data } = res
      console.log(data)
      if (data.success) {
        message.success('更新成功')
        setUser(data.data)
        initvalue(data.data)
      } else {
        console.trace(data)
      }
    }, e => {
      console.trace(e)
    })
  };

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className='ant-upload-text'>Upload</div>
    </div>
  );
  function beforeUpload(file) {
    setLoading(true);
    //console.log(file);
    const isLt1M = file.size / 1024 / 1024 < 1;
    const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
      setLoading(false);
      return false;
    } else if (!isLt1M) {
      message.error("Image must smaller than 1MB!");
      setLoading(false);
      return false;
    } else if (window.FileReader) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = function () {
        setImageUrl(reader.result);
        message.success("getImg");
        setLoading(false);
      };
      return false;
    } else {
      message.error("error");
      return false;
    }
  }
  // console.log(props)
  return (
    <>
      <Head title={'vital_article-用户中心'} ></Head>
      <div className={styles.container}>
        <Body needlogin >
          <Row>
            <Col span={8} style={{
              padding: '1rem 0',
              ...flexCentor
            }}>
              <Avatar
                size={{ xs: 60, sm: 80, md: 80, lg: 80, xl: 80, xl: 100, xxl: 100 }}
                shape="square"
                src={user.user_avator}
                style={{ backgroundColor: '#f56a00', verticalAlign: 'middle' }}
              >{user.user_nickname || 'U'}</Avatar>
            </Col>
            <Col span={16} style={{
              padding: '1rem',
            }}>
              <Row span={24} style={{}}>
                <Typography.Title level={4} >昵称：{user.user_name}</Typography.Title>
              </Row>
              <Row span={24} style={{}}>
                <Typography.Title level={4} >注册日期：{dayjs(+user.user_creattime).format('YYYY-MM-DD')}</Typography.Title>
              </Row>
              <Row span={24} style={{}}>
                <Typography.Title level={4} >邮箱：{user.user_email}</Typography.Title>
              </Row>
            </Col>
          </Row>
          <Divider />
          <Row style={{ ...flexCentor }}>
            <div style={{ width: '80%' }}>
              <Row align='middle' gutter={20}>
                <Col sm={8} xs={10} >
                  <Typography.Title level={4}>头像</Typography.Title>
                </Col >
                <Col span={16}>
                  <ImgCrop rotate>
                    <Upload
                      name='avatar'
                      listType='picture-card'
                      className='avatar-uploader'
                      showUploadList={false}
                      beforeUpload={beforeUpload}
                    >
                      {imageUrl ? (
                        <img src={imageUrl} alt='avatar' style={{ width: "100%" }} />
                      ) : (
                        uploadButton
                      )}
                    </Upload>
                  </ImgCrop>
                </Col>
              </Row>
              <Row align='middle' gutter={20} style={{ padding: '1rem 0' }}>
                <Col sm={8} xs={10} >
                  <Typography.Title level={4}>昵称</Typography.Title>
                </Col >
                <Col span={16}>
                  <Input value={nickName} onChange={(e) => { setnickName(e.target.value) }} />
                </Col>
              </Row>
              <Row align='middle' gutter={20} style={{ padding: '1rem 0' }}>
                <Col sm={8} xs={10} >
                  <Typography.Title level={4}>密码</Typography.Title>
                </Col >
                <Col span={16}>
                  <Input.Password value={password} onChange={(e) => { setpassword(e.target.value) }} />
                </Col>
              </Row>
              {/* <Row align='middle' gutter={20} style={{ padding: '1rem 0' }}>
                  <Col sm={8} xs={10} >
                    <Typography.Title level={4}>邮箱</Typography.Title>
                  </Col >
                  <Col span={16}>
                    <Input type="email" value={eMail} onChange={(e) => { seteMail(e.target.value) }} />
                  </Col>
                </Row> */}
              <Row align='middle' gutter={20} style={{ padding: '1rem 0' }}>
                <Col sm={8} xs={10} >
                  <Typography.Title level={4}>简介</Typography.Title>
                </Col >
                <Col span={16}>
                  <Input.TextArea autoSize value={introduce} onChange={(e) => { setintroduce(e.target.value) }} />
                </Col>
              </Row>
              <Row>
                <Col sm={8} xs={10} >
                </Col >
                <Col span={16}>
                  <Button onClick={onFinish} block type="primary" size="large" >
                    保存
                    </Button>
                </Col>
              </Row>
            </div>
          </Row>
        </Body>
      </div>
    </>
  )
}

const mapState = state => ({
  user: state.user,
})

const mapDispatch = ({ user: { setUser, changeProps } }) => ({
  setUser: (obj) => setUser(obj),
  changeProps: (key, value) => changeProps({ key, value }),
})

export default connect(mapState, mapDispatch)((Usercenter))