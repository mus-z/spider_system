import React from 'react'
import { Empty } from 'antd'
import { withRouter, useRouter } from 'next/router'
class Error extends React.Component {
  static getInitialProps({ res, err }) {
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode }
  }
  componentDidMount(){
    console.log(this.props)
    setTimeout(()=>{
        this.props.router.replace('/')
    },3000)
  }
  render() {
    return (<div style={{width:'100vw',height:'100vh',display:'flex',justifyContent:'center',alignItems:'center'}}>
      <Empty>
        {this.props.statusCode
          ? `An error ${this.props.statusCode} occurred on server`
          : 'An error occurred on client'}
          {`, 3 seconds later back to home`}
      </Empty>
      </div>
    )
  }
}
export default withRouter(Error)
