import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import { useEffect, useRef, useState } from 'react'
import { Empty } from 'antd';
import { connect } from 'react-redux'
import Body from '@/components/Body'
import AdminOnlineChat from '@/components/AdminOnlineChat'
function CallAdmin(props) {
    const {admin_online } = props
    const socket = useRef(null)
    // console.log(props)
    useEffect(() => {
        if (!socket.current) socket.current = window.socket
    })
    return (<>
        <Head title={`vital_article-联系管理员(${admin_online ? '在线' : '离线'})`} ></Head>
        <div className={styles.container}>
            <Body needlogin >
                {admin_online && window.socket ? <AdminOnlineChat /> :
                    <Empty description={
                        <span style={{ color: '#aaa' }}>
                            管理员不在线
                        </span>
                    }></Empty>}
            </Body>
        </div>
    </>)
}

const mapState = state => ({
    user: state.user,
    admin_online: state.admin_online
})

const mapDispatch = ({
    user: { setUser, changeProps },
    admin_online: { setAdminOnlieState }
}) => ({
    setUser: (obj) => setUser(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setAdminOnlieState,
})
export default connect(mapState, mapDispatch)(CallAdmin)