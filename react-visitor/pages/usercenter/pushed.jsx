import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import { connect } from 'react-redux'
import Body from '@/components/Body'
import PushedArtTable from '@/components/PushedArtTable'
function ViewHistory(props) {
    const { article_pushed} = props
    return (<>
        <Head title={`vital_article-${article_pushed.length ? ('(' + (article_pushed.length || 0) + '条未读)') : '推送'}`} ></Head>
        <div className={styles.container}>
            <Body needlogin >
                <PushedArtTable/>
            </Body>
        </div>
    </>)
}

const mapState = state => ({
    user: state.user,
    user_attention_plates: state.user_attention_plates,
    article_pushed: state.article_pushed,
})

const mapDispatch = ({
    user: { setUser, changeProps },
    user_attention_plates: { setAttentionPlates, pushAttentionPlates },
    article_pushed: { setArticlePushed }
}) => ({
    setUser: (obj) => setUser(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setAttentionPlates,
    pushAttentionPlates,
    setArticlePushed,
})
export default connect(mapState, mapDispatch)(ViewHistory)