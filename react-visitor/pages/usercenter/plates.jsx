import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import { connect } from 'react-redux'
import PlateCardList from '@/components/PlateCardList'
import Body from '@/components/Body'
function PlateList(props) {
  const { user } = props
  return (<>
    <Head title={'vital_article-我的关注-板块'} ></Head>
    <div className={styles.container}>
      <Body needlogin >
            <PlateCardList user_id={user.Id} />
      </Body>
    </div>
  </>)
}
const mapState = state => ({
  user: state.user,
})

const mapDispatch = ({ user: { setUser, changeProps } }) => ({
  setUser: (obj) => setUser(obj),
  changeProps: (key, value) => changeProps({ key, value }),
})
export default connect(mapState, mapDispatch)(PlateList)