import Head from '@/components/Head'
import styles from '@/styles/Home.module.css'
import { connect } from 'react-redux'
import Body from '@/components/Body'
import ArtHistoryTable from '@/components/ArtHistoryTable'
function ViewHistory(props) {
  const { user} = props
  return (<>
    <Head title={'vital_article-历史观看-公告'} ></Head>
    <div className={styles.container}>
      <Body needlogin >
            <ArtHistoryTable user_id={user.Id}/>
      </Body>
    </div>
  </>)
}

const mapState = state => ({
  user: state.user,
})

const mapDispatch = ({ user: { setUser, changeProps } }) => ({
  setUser: (obj) => setUser(obj),
  changeProps: (key, value) => changeProps({ key, value }),
})
export default connect(mapState, mapDispatch)(ViewHistory)