import Head from '@/components/Head'
import styles from '@/styles/Login.module.css'
import '@/styles/Login.module.css';
import LoginFrom from '@/components/LoginForm'
import Header from '@/components/Header'
import Footer from '@/components/Footer'
function Login(props) {
  return (<>
    <Head title="vital_article-登录"></Head>
    <div className={styles.main + ' ' + 'visitor-login-main'}>
      <header className={styles.header}>
        <Header />
        <section className={styles.section} >
          <div style={{
            //蒙版
            position: 'absolute',
            backgroundColor: '#000',
            width: '100%',
            height: '100%',
            opacity: 0.1,
          }}></div>
          <div style={{
            height: '100%',
            position: 'relative',
            display: 'flex',
            justifyContent: 'center',
            minWidth: '40%',
            alignItems: 'start',
            opacity: 0.9,
          }}>
            <div className={styles.login}>
              <LoginFrom {...props} />
            </div>
          </div>
        </section>
        <Footer />
      </header>
    </div>

  </>)
}

export default Login