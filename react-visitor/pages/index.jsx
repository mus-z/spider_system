import Head from '@/components/Head'
import LoadMoreList from '@/components/LoadMoreList'
import Body from '@/components/Body'
import { useEffect } from 'react'
function Home(props) {
  const {scrollTop}=props
  useEffect(()=>{
  },[])
  return (
    <>
      <Head title={'vital_article-近期列表'} ></Head>
      <Body >
        <LoadMoreList />
      </Body>
    </>
  )
}
export default (Home)