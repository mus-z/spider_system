import Body from '@/components/Body'
import Head from '@/components/Head'
import StockTable from '@/components/StockTable'
function CompanyList(props) {
  return (<>
    <Head title={'vital_article-公司列表'} ></Head>
    <Body needlogin >
      <StockTable />
    </Body>
  </>)
}
export default CompanyList