import { init } from '@rematch/core'
import * as models from './models'
import immerPlugin from '@rematch/immer';

const store = init({
  models,
  plugins:[immerPlugin()]
})

export default store