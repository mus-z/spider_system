import React from 'react'
import { Provider, connect } from 'react-redux'
import store from './store'
const dispatch = store.dispatch
const Count = props => (
  <div>
    The count is {props.count}
    <button onClick={props.increment}>increment</button>
    <button onClick={props.incrementAsync}>incrementAsync</button>
    <button onClick={() => {
      dispatch.count.decrement(1)
    }}>decrement</button>
    <button onClick={() => {
      dispatch({
        type: 'count/decrement',
        payload: 1
      })
    }}>decrement</button>
  </div>
)

const mapState = state => ({
  count: state.count
})

const mapDispatch = ({ count: { increment, incrementAsync, decrement } }) => ({
  increment: () => increment(1),
  incrementAsync: () => incrementAsync(1),
  decrement: () => decrement(1)
})

const CountContainer = connect(mapState, mapDispatch)(Count)
function Index() {
  return (
    <Provider store={store}>
      <CountContainer />
    </Provider>)
}
export default Index
