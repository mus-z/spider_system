export const count = {
    state: 0, // 默认变量
    reducers: {
      // 修改state的纯函数
      increment(state, payload) {
        return state + payload
      },
      decrement:(state, payload)=>state - payload
    },
    effects: {
      // handle state changes with impure functions.
      // 处理异步action
      async incrementAsync(payload, rootState) {
        await new Promise(resolve => setTimeout(resolve, 1000))
        this.increment(payload)
      }
    }
  }