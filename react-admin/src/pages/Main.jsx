import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import Login from '@/pages/Login'
import Home from '@/pages/Home'
import '@/css/Main.less'
import { ConfigProvider } from 'antd';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import 'dayjs/locale/zh-cn';


function Main(props) {
    return (
        <>
            <ConfigProvider locale={zh_CN}>
                <Router>
                    <Switch>
                        <Route path="/admin" component={Home} />
                        <Route path="/login" exact component={Login} />
                        <Redirect to="/admin" />
                        {/* <Link to="/login">tologin</Link> */}
                    </Switch>
                </Router>
            </ConfigProvider>
        </>
    )
}

export default (Main)