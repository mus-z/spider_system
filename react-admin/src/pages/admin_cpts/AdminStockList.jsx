import AdminStockTable from '@/components/AdminStockTable'

function AdminStockListWithoutConnect(props) {
    return (
        <div className="admin-stock-list">
            <AdminStockTable {...props} />
        </div>
    )
}

export const AdminStockList = AdminStockListWithoutConnect