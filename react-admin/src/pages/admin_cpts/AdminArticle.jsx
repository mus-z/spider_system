import axios from 'axios'
import { useEffect, useState } from 'react'
import { Typography, Divider, Tooltip, Timeline, Col, Row, Button, Statistic } from 'antd';
import { NavLink } from 'react-router-dom';
import { DayjsFormate } from '@/utils/TimeString'
import { EyeFilled } from '@ant-design/icons';
import CommentBoard from '@/components/CommentBoard'
const { Title, Text } = Typography;

function AdminArticleWithoutConnect(props) {
    const [artObj, setartObj] = useState({})
    const [view, setview] = useState(0)
    const art_code = props.match.params.code
    const artInit = () => {
        let url = require('@/String.js').server + 'admingetarticlebycode'
        axios({
            method: 'get',
            withCredentials: true,
            url: url + '/' + art_code,
        }).then(res => {
            const { data } = res
            if (data.success) {
                // console.log(data)
                let text = (data.art.text)
                text = text.replace(/(\n)+/g, '$1')//去除多余换行
                setartObj({ ...data.art, text } || {})
                setview(data.view)
            } else {
                console.trace(res)
            }
        }, e => {
            console.trace(e)
        })
    }
    useEffect(() => {
        artInit()
    }, [])
    const breakpoint = props.breakpoint
    return (
        <div className="admin-art-main">
            <Row gutter={2}>
                <Col sm={20} xs={24} >
                    <Typography>
                        <Tooltip
                            placement="topLeft"
                            title={() => {
                                return (
                                    <a href={artObj.url} target="_blank" >点击回原文</a>
                                )
                            }}
                        ><Title level={breakpoint ? 4 : 2}>{artObj.title}</Title></Tooltip>
                        <Col span={4}>
                            <Statistic title="浏览量" value={view} valueStyle={{ color: '#c7a519' }} prefix={<EyeFilled />} />
                        </Col>
                        <Divider orientation="left"><NavLink to={{
                            pathname: '/admin/' + artObj.stock_code
                        }} >{artObj.short_name}</NavLink></Divider>
                        <div style={{ whiteSpace: 'pre-line', maxHeight: "50vh", overflow: "auto", backgroundColor: '#ccc', padding: '1rem', paddingTop: 0 }}>
                            <Text style={{}} >{artObj.text}</Text>
                        </div>
                    </Typography>

                </Col>
                <Col sm={4} xs={24}>
                    <Timeline style={{ paddingTop: '2rem' }}>
                        <Timeline.Item>
                            <Tooltip title="公告时间" trigger="focus">
                                <Button type="text" style={{ background: 'inherit' }} >{DayjsFormate(artObj.notice_date, 'YYYY-MM-DD')}</Button>
                            </Tooltip>
                        </Timeline.Item>
                    </Timeline>
                </Col>
            </Row>
            <Row>
                <Col span={20}>
                    <CommentBoard {...props} user_id={"admin"} art_code={art_code} />
                </Col>
            </Row>
        </div>
    )
}

export const AdminArticle = AdminArticleWithoutConnect