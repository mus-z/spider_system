import { connect } from 'react-redux'
import React, { useState, useEffect } from "react";
import { Button, message, Row, Col, Descriptions, Input, Modal, Switch, InputNumber, Space } from "antd";
import {
    EyeOutlined,
    EyeInvisibleOutlined
} from "@ant-design/icons";
import { TimeString} from '@/utils/TimeString'
import axios from 'axios';
const { confirm } = Modal;
const PasswordLabel = ({ canSee, setCanSee }) => {
    return (
        canSee ?
            <>
                密码
                <Button type="link" onClick={() => {
                    setCanSee(0)
                }}>
                    <EyeOutlined />
                </Button>
            </> :
            <>
                密码
                <Button type="link" onClick={() => {
                    setCanSee(1)
                }}>
                    <EyeInvisibleOutlined />
                </Button>
            </>
    )
}

function AdminControlWithoutConnect(props) {
    const [isEdit, setIsEdit] = useState(0)
    const [canSee, setCanSee] = useState(0)
    let obj = props.admin
    const [password, setPassword] = useState(props.admin.password)
    const [ps, setPs] = useState(props.admin.ps)
    const [interval, setInterval] = useState(props.admin.update_interval)
    const [toggle, setToggle] = useState(props.admin.timer_state)
    function init() {
        setPassword(props.admin.password)
        setPs(props.admin.ps)
        setInterval(props.admin.update_interval)
        setToggle(props.admin.timer_state)
    }
    useEffect(async () => {
        let url = require('@/String.js').server + 'adminquery'
        await axios.get(url, { withCredentials: true, }).then(res => {
            props.setAdmin(res.data)
        }, e => {
            console.trace(e)
        })
        init()
    }, [isEdit])

    return (
        <>
            <Col className="admin-control">
                <Descriptions
                    title={`爬虫控制台, ${props.admin.admin}~${obj['openId']}`}
                    column={1}
                    extra={isEdit ?
                        <>
                            <Button className="toggle_label" size='large' type="link" onClick={() => {
                                confirm({
                                    title: `确定保存？`,
                                    content: "ok--保存",
                                    onOk() {
                                        axios({
                                            method: 'post',
                                            withCredentials: true,
                                            data: {
                                                options: {
                                                    id: obj['Id'],
                                                    password: password,
                                                    update_interval: interval,
                                                    timer_state: toggle,
                                                    ps: ps,
                                                },
                                                openId: obj['openId']
                                            },
                                            url: require('@/String.js').server + 'adminupdate'
                                        }).then(res => {
                                            let { data } = res
                                            console.log(data)
                                            if (data.success) {
                                                props.setAdmin(data.admin)
                                                message.success('修改成功')
                                            } else {
                                                message.warn('修改失败')
                                            }
                                            setIsEdit(0)
                                        }, (e) => {
                                            message.error(e + '')
                                            console.error(e)
                                            setIsEdit(0)
                                        })
                                    },
                                    onCancel() {
                                        setIsEdit(0)
                                    },
                                });
                            }}>
                                Save
                        </Button>
                        </> :
                        <>

                            <Button className="toggle_label" size='large' type="link" onClick={() => {
                                setIsEdit(1)
                            }}>
                                Edit
                        </Button>
                        </>}
                    bordered
                    labelStyle={{ backgroundColor: '#ddd' }}
                >
                    {isEdit ?
                        // 通过isEdit button切换模式并表单保存
                        (
                            <Space direction="vertical">
                                <Row>
                                    <Col sm={6} xs={12}><PasswordLabel canSee={canSee} setCanSee={setCanSee} /> </Col>
                                    <Col span={13}>
                                        <Input type={canSee ? "text" : "password"} value={password} onChange={(e) => {
                                            let value = e.target.value
                                            setPassword(value)
                                        }}>
                                        </Input>
                                    </Col>
                                </Row>

                                <Row>
                                    <Col sm={6} xs={12}>定时爬虫间隔</Col>
                                    <Col span={13}>
                                        <InputNumber min={0.2} max={48.0}
                                            defaultValue={interval}
                                            onChange={(value) => {
                                                setInterval(value)
                                            }}
                                        >
                                        </InputNumber> h
                                        {/* {interval} */}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6} xs={12}>定时爬虫状态</Col>
                                    <Col span={13}>
                                        <Switch checked={toggle} onChange={(v) => {
                                            setToggle(v + 0)
                                        }}
                                            checkedChildren="开启" unCheckedChildren="关闭"
                                        />
                                        {/* {toggle} */}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col sm={6} xs={12}>备注</Col>
                                    <Col span={13}>
                                        <Input.TextArea autoSize value={ps} onChange={(e) => {
                                            let value = e.target.value
                                            setPs(value)
                                        }}>
                                        </Input.TextArea>
                                    </Col>
                                </Row>
                            </Space>
                        ) :
                        (<>
                            <Descriptions.Item label={'管理员账号'}>{obj['admin']}</Descriptions.Item>
                            <Descriptions.Item label={<PasswordLabel canSee={canSee} setCanSee={setCanSee}></PasswordLabel>}>{
                                !canSee ?
                                    obj['password'] && obj['password'].replace(/./g, '*') : obj['password']
                            }</Descriptions.Item>
                            <Descriptions.Item label={'最近公告时间'}>{TimeString(obj['latest_display_time'])}</Descriptions.Item>
                            <Descriptions.Item label={'上次爬虫时间'}>{TimeString(obj['last_crawler_time'])}</Descriptions.Item>
                            <Descriptions.Item label={'定时爬虫状态'}>{obj['timer_state'] ? '开' : '关'}</Descriptions.Item>
                            <Descriptions.Item label={'定时爬虫间隔（小时）'}>{obj['update_interval']}</Descriptions.Item>
                            <Descriptions.Item label={'备注'} ><pre>{obj['ps']}</pre></Descriptions.Item>
                        </>)
                    }
                </Descriptions>
            </Col>
        </>
    )
}
const mapState = state => ({
    admin: state.admin
})
const mapDispatch = ({ admin: { setAdmin, changeProps } }) => ({
    setAdmin: (obj) => setAdmin(obj),
    changeProps: (key, value) => changeProps({ key, value }),
})
export const AdminControl = connect(mapState, mapDispatch)(AdminControlWithoutConnect)