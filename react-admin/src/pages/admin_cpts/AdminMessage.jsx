import { Menu, } from 'antd'
import { useEffect, useState } from 'react'
import socket from '@/socketIO'
import ChatMain from "@/components/ChatMain"

export const AdminMessage = (props) => {

    const [headUserId, setheadUserId] = useState("")
    const [chatList, setchatList] = useState([])

    const finduser = (Id) => {
        for (let item of chatList) {
            if (item.Id == Id) return item
        }
        return { user_nickname: "", msgs: [] }
    }
    useEffect(() => {
        console.log(socket)
        // socket.removeAllListeners('chat')
        socket.on('chat', function (data) {
            // console.log(data)
            setchatList(data)
        })
        socket.emit('getchat')
        return()=>{
            socket?.removeAllListeners('chat')
        }
    }, [])
    return (
        <>
            <div className="user_admin_chat"
                style={{
                    display: 'flex',
                    alignItems: 'flex-start',
                    justifyContent: "start",
                    height: '40rem',
                    minWidth: '50rem'
                }}
            >
                <aside style={{
                    height: "100%",
                    backgroundColor: '#001529',
                    color: '#fff',
                    minWidth: '10rem',
                    padding: '2rem 2rem',
                    cursor: 'pointer',
                }}>
                    <Menu theme="dark"
                        style={{
                            overflow: 'auto'
                        }}
                        onClick={({ key }) => {
                            // console.log(key)
                            setheadUserId(key)
                        }}
                        selectedKeys={[headUserId]}
                    >
                        {chatList.map(item => {
                            return (
                                <Menu.Item key={item.Id}
                                    style={{
                                        borderLeft: (headUserId != item.Id) ? '.5rem solid #eee' : '',
                                        transition: "all 0.3s ease 0s"
                                    }}
                                >{item.user_nickname}</Menu.Item>
                            )
                        })}

                    </Menu>
                </aside>
                {socket.id && <ChatMain user_id={headUserId} item={finduser(headUserId)} socket={socket} />}
            </div>
        </>
    )
}
export default AdminMessage