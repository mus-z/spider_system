import AdminArtTable from '@/components/AdminArtTable'

function AdminArtListWithoutConnect(props) {
    return (
        <div className="admin-art-list">
            <AdminArtTable {...props} />
        </div>
    )
}
export const AdminArtList = AdminArtListWithoutConnect