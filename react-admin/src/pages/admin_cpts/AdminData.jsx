import { useEffect, useState } from "react"
import dayjs from 'dayjs';
import axios from 'axios'
import { Statistic, Card, Row, Col } from 'antd';
import ReactEChartsCore from 'echarts-for-react/lib/core';
import * as echarts from 'echarts/core';
import { ArrowUpOutlined } from '@ant-design/icons';
import {
    LineChart,
    BarChart,
    // PieChart,
    // ScatterChart,
    // RadarChart,
    // MapChart,
    // TreeChart,
    TreemapChart,
    // GraphChart,
    // GaugeChart,
    // FunnelChart,
    // ParallelChart,
    // SankeyChart,
    // BoxplotChart,
    // CandlestickChart,
    // EffectScatterChart,
    // LinesChart,
    // HeatmapChart,
    // PictorialBarChart,
    // ThemeRiverChart,
    // SunburstChart,
    // CustomChart,
} from 'echarts/charts';
import {
    // GridSimpleComponent,
    GridComponent,
    // PolarComponent,
    // RadarComponent,
    // GeoComponent,
    // SingleAxisComponent,
    // ParallelComponent,
    // CalendarComponent,
    // GraphicComponent,
    ToolboxComponent,
    TooltipComponent,
    // AxisPointerComponent,
    // BrushComponent,
    TitleComponent,
    // TimelineComponent,
    // MarkPointComponent,
    // MarkLineComponent,
    // MarkAreaComponent,
    // LegendComponent,
    // LegendScrollComponent,
    // LegendPlainComponent,
    // DataZoomComponent,
    // DataZoomInsideComponent,
    // DataZoomSliderComponent,
    // VisualMapComponent,
    // VisualMapContinuousComponent,
    // VisualMapPiecewiseComponent,
    // AriaComponent,
    // TransformComponent,
    // DatasetComponent,
} from 'echarts/components';
import {
    CanvasRenderer,
    // SVGRenderer,
} from 'echarts/renderers';
import { server } from "@@/src/String";
echarts.use(
    [TitleComponent, TooltipComponent, GridComponent, BarChart, CanvasRenderer, LineChart, ToolboxComponent, TreemapChart]
);
export function AdminData(props) {
    const [art_time_date_data, setart_time_date_data] = useState([])
    const [user_time_date_data, setuser_time_date_data] = useState([])
    const [user_plates_data, setuser_plates_data] = useState([])
    const [company_plates_data, setcompany_plates_data] = useState([])
    const [plate_count, setplate_count] = useState(0)
    const [stock_count, setstock_count] = useState(0)
    const [view_count,setview_count] =useState(0)
    const [comment_count,setcomment_count]=useState(0)
    const [user_activity_data,setuser_activity_data]=useState([])
    useEffect(() => {
        axios({
            method: 'get',
            withCredentials: true,
            url: server + 'admingetdata'
        }).then(res => {
            const { data } = res
            if (data.success) {
                console.log(data)
                setart_time_date_data(data.art_time_date_data)
                setuser_time_date_data(data.user_time_date_data)
                setplate_count(data.plate_count)
                setuser_plates_data(data.user_plates_data)
                setstock_count(data.stock_count)
                setcompany_plates_data(data.company_plates_data)
                setview_count(data.view_count)
                setcomment_count(data.comment_count)
                setuser_activity_data(data.user_activity_data)
            }
        })
    }, [])
    const getart_time_dateOption = () => {
        return {
            title: {
                text: '近期公告统计',
                left: 'center',
                align: 'right'
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: art_time_date_data.map(item => item.crawl_date)
            },
            yAxis: [{
                name: '日增量',
                type: 'value',
                min: 'dataMin',
                max: '100',
                scale: true
            },
            {
                name: '累计总量',
                type: 'value',
                // min: 'dataMin',

                scale: true
            }
            ],
            series: [{
                data: art_time_date_data.map(item => item.c),
                yAxisIndex: 1,
                type: 'line',
                name: '累计',
                areaStyle: {}
            },
            {
                data: art_time_date_data.map(item => item.diff),
                type: 'line',
                name: '新增',
                areaStyle: {}
            }],
            // toolbox: {
            //     feature: {
            //         dataZoom: {
            //             yAxisIndex: 'none'
            //         },
            //         restore: {},
            //         saveAsImage: {}
            //     }
            // },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
        };
    }
    const getuser_time_dateOption = () => {
        return {
            title: {
                text: '新增用户统计',
                left: 'center',
                align: 'right'
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: user_time_date_data.map(item => item.creat_date)
            },
            yAxis: [{
                name: '日增量',
                type: 'value',
                min: 0,
                scale: true
            },
            {
                name: '累计总量',
                type: 'value',
                // min: 'dataMin',
                scale: true
            }
            ],
            series: [{
                data: user_time_date_data.map(item => item.c),
                yAxisIndex: 1,
                type: 'line',
                name: '累计',
                areaStyle: {}
            },
            {
                data: user_time_date_data.map(item => item.diff),
                type: 'line',
                name: '新增',
            }],
            // toolbox: {
            //     feature: {
            //         dataZoom: {
            //             yAxisIndex: 'none'
            //         },
            //         restore: {},
            //         saveAsImage: {}
            //     }
            // },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
        };
    }
    const getuser_platesOption = () => {
        return {
            title: {
                text: '板块热度（关注量）',
                left: 'center',
                align: 'right'
            },
            tooltip: {
                show: true
            },
            series: [{
                type: 'treemap',
                data: user_plates_data,
                breadcrumb: { show: false },
                nodeClick: false,
                roam: false
            },]
        };
    }
    const getcompany_platesOption = () => {
        return {
            title: {
                text: '板块规模（公司数）',
                left: 'center',
                align: 'right'
            },
            tooltip: {
                show: true
            },
            series: [{
                type: 'treemap',
                data: company_plates_data,
                breadcrumb: { show: false },
                nodeClick: false,
                roam: false
            },]
        };
    }
    const getuser_activityOption = () => {
        return {
            title: {
                text: '近期日活统计',
                left: 'center',
                align: 'right'
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: user_activity_data.map(item => item.view_date)
            },
            yAxis: [{
                name: '日活(人次)',
                type: 'value',
                scale: true
            },],
            series: [{
                data: user_activity_data.map(item => item.view_c),
                type: 'line',
                name: '浏览',
                areaStyle: {}
            },
            {
                data: user_activity_data.map(item => item.comment_c),
                type: 'line',
                name: '评论',
                areaStyle: {}
            }],
            // toolbox: {
            //     feature: {
            //         dataZoom: {
            //             yAxisIndex: 'none'
            //         },
            //         restore: {},
            //         saveAsImage: {}
            //     }
            // },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'cross',
                    label: {
                        backgroundColor: '#6a7985'
                    }
                }
            },
        };
    }
    const gettodaydiff = (data) => {
        let today = dayjs().format('YYYY-MM-DD')
        let c = 0
        for (let item of data) {
            if (item.creat_date == today || item.crawl_date == today) { c = item.diff; break }
        }
        return c
    }
    return (
        <>
            <Row justify="start" style={{backgroundColor:'#fff'}}>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="公告总量"
                            value={art_time_date_data[art_time_date_data.length - 1]?.c}
                            valueStyle={{ color: '#cf1322' }}
                            suffix={<span>(<ArrowUpOutlined />{gettodaydiff(art_time_date_data)}) </span>}
                        />
                    </Card>
                </Col>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="注册人数"
                            value={user_time_date_data[user_time_date_data.length - 1]?.c}
                            valueStyle={{ color: '#cf1322' }}
                            suffix={<span>(<ArrowUpOutlined />{gettodaydiff(user_time_date_data)}) </span>}
                        />
                    </Card>
                </Col>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="公司总数"
                            value={stock_count}
                            valueStyle={{ color: '#cf1322' }}
                        />
                    </Card>
                </Col>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="板块总数"
                            value={plate_count}
                            valueStyle={{ color: '#cf1322' }}
                        />
                    </Card>
                </Col>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="浏览人次"
                            value={view_count}
                            valueStyle={{ color: '#cf1322' }}
                        />
                    </Card>
                </Col>
                <Col>
                    <Card bordered>
                        <Statistic
                            title="评论总数"
                            value={comment_count}
                            valueStyle={{ color: '#cf1322' }}
                        />
                    </Card>
                </Col>
            </Row>
            <Row gutter={40} style={{
                padding:'2rem 0'
            }}>
                <Col xs={24} sm={24} md={12}>
                    <ReactEChartsCore
                        echarts={echarts}
                        option={getart_time_dateOption()}
                        notMerge={true}
                        lazyUpdate={true}
                        theme={"theme_name"}
                        className="art_time_date"
                        style={{
                            height: '30rem'
                        }}
                    />
                </Col>
                <Col xs={24} sm={24} md={12}>
                    <ReactEChartsCore
                        echarts={echarts}
                        option={getuser_time_dateOption()}
                        notMerge={true}
                        lazyUpdate={true}
                        theme={"theme_name"}
                        className="user_time_date"
                        style={{
                            height: '30rem'
                        }}
                    />
                </Col>
                <Col xs={24} sm={24} md={12}>
                    <ReactEChartsCore
                        echarts={echarts}
                        option={getuser_platesOption()}
                        notMerge={true}
                        lazyUpdate={true}
                        theme={"theme_name"}
                        className="user_plates"
                        style={{
                            height: '30rem'
                        }}
                    />
                </Col>
                <Col xs={24} sm={24} md={12}>
                    <ReactEChartsCore
                        echarts={echarts}
                        option={getcompany_platesOption()}
                        notMerge={true}
                        lazyUpdate={true}
                        theme={"theme_name"}
                        className="company_plates"
                        style={{
                            height: '30rem'
                        }}
                    />
                </Col>
                <Col span={24}>
                    <ReactEChartsCore
                        echarts={echarts}
                        option={getuser_activityOption()}
                        notMerge={true}
                        lazyUpdate={true}
                        theme={"theme_name"}
                        className="user_activity"
                        style={{
                            height: '30rem'
                        }}
                    />
                </Col>
            </Row>
        </>
    )
}