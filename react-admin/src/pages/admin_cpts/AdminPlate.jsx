import AdminStockTable from '@/components/AdminStockTable'
import { useEffect, useState } from 'react'
import { Divider, Statistic, Row, Col, } from 'antd';
import { server } from "@/String";
import axios from "axios";
import { HeartFilled } from '@ant-design/icons';

export function AdminPlate(props) {
    const { plateId } = props.match.params
    const [plate, setPlate] = useState('')
    const [userCount, setuserCount] = useState(0)
    useEffect(() => {
        axios.get(server + 'admingetallplates', {
            withCredentials: true, params: {
                plateId
            }
        })
            .then(res => {
                const { data } = res
                if (data.success) {
                    setPlate(data.plates[0].plate_name)
                    setuserCount(data.plates[0].user_count)
                }
            }, e => {
                console.trace(e)
            })
    }, [])
    return (
        <>

            <div className="admin-plate-list">
                <Row gutter={20}>
                    <Col span={1}></Col>
                    <Col span={5}>
                        <Statistic title="板块" value={plate} valueStyle={{ color: '#c7a519' }} />
                    </Col>
                    <Col span={6}>
                        <Statistic title="关注人数" value={userCount} valueStyle={{ color: '#c7a519' }} prefix={<HeartFilled />} />
                    </Col>
                </Row>
                <Divider orientation="left"></Divider>
                <AdminStockTable {...props} plateId={plateId} platename={plate} />
            </div>
        </>
    )
}