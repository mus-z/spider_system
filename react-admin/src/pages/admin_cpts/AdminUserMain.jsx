import { server } from "@/String"
import { Row, Col, Avatar, Collapse, Input, Divider,  Descriptions } from "antd"
import axios from "axios"
import dayjs from "dayjs"
import { useEffect, useState } from "react"
import { AdminPlateList } from "@/pages/admin_cpts"
import ArtHistoryTable from "@/components/ArtHistoryTable"
const { Panel } = Collapse
function AdminUserMainWithoutConnect(props) {
    const userId = props.match.params.Id
    const [user, setuser] = useState({})
    useEffect(() => {
        axios({
            url: server + 'admingetuserbyid',
            withCredentials: true,
            params: {
                Id: userId
            }
        }).then(res => {
            const { data } = res
            if (data.success) {
                setuser(data.user)
            }
        })
        console.log(userId)
    }, [])
    const flexCentor = {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
    return (
        <>
            <div className="admin-user-main">
                <Row>
                    <Col span={24} style={{
                        padding: '1rem 0',
                        ...flexCentor
                    }}>
                        <Avatar
                            size={{ xs: 80, sm: 80, md: 100, lg: 100, xl: 120, xxl: 120 }}
                            shape="square"
                            src={user.user_avator}
                            style={{ backgroundColor: '#f56a00', verticalAlign: 'middle' }}
                        >{user.user_nickname || 'U'}</Avatar>
                    </Col>
                </Row>
                <Collapse defaultActiveKey={['intro', 'plates']}>
                    <Panel header={'用户信息'} key="intro">
                        <Row align="middle" justify="center" style={{
                            minWidth:"40rem"
                        }}> 
                            <Col span={24} style={{
                                padding: '1rem',
                            }}>
                                <Descriptions
                                    column={1}
                                    labelStyle={{
                                        textAlign: 'right',
                                        fontSize: '1rem'
                                    }}
                                    bordered
                                >
                                    <Descriptions.Item label="账号">{user.user_name}</Descriptions.Item>
                                    <Descriptions.Item label="昵称">{user.user_nickname}</Descriptions.Item>
                                    <Descriptions.Item label="注册日期">{dayjs(+user.user_creattime).format('YYYY-MM-DD')}</Descriptions.Item>
                                    <Descriptions.Item label="邮箱">{user.user_email}</Descriptions.Item>
                                    <Descriptions.Item label="简介">
                                        <Input.TextArea
                                            autoSize
                                            disabled value={user.user_introduce}
                                            style={{
                                                cursor: 'default'
                                            }}></Input.TextArea></Descriptions.Item>
                                </Descriptions>
                            </Col>
                        </Row>
                    </Panel>
                    <Panel header={'关注板块'} key="plates">
                        <AdminPlateList {...props} userId={userId} />
                    </Panel>
                    <Panel header={'浏览记录'} key="history">
                        <ArtHistoryTable {...props} user_id={userId} />
                    </Panel>
                </Collapse>

                <Divider />

            </div>
        </>
    )
}
export const AdminUserMain = AdminUserMainWithoutConnect