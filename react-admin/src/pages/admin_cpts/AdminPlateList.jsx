import { server } from "@@/src/String";
import axios from "axios";
import { useEffect, useState } from "react";
import { Tag ,Empty} from "antd";
export function AdminPlateList(props) {
    const [plates, setplates] = useState([])
    const { userId } = props
    useEffect(() => {
        axios.get(server + 'admingetallplates',
            {
                withCredentials: true,
                params: {
                    userId
                }
            })
            .then(res => {
                const { data } = res
                if (data.success) {
                    setplates(data.plates)
                }
            }, e => {
                console.trace(e)
            })
    }, [])
    return (
        <>
            <div>
                <p style={{color:'#aaa'}}>共{plates.length}个</p>
                {
                    plates.length>0?
                    plates && plates.map((item, index) => {
                        return (
                            <Tag key={index}
                                color="volcano"
                            >
                                <span
                                    style={{
                                        cursor: 'pointer'
                                    }}
                                    onClick={() => {
                                        props.history.push({
                                            pathname: '/admin/plates/' + item.Id,
                                        })
                                    }}>{item.plate_name}</span>
                            </Tag>
                        )
                    }):<Empty/>

                }
            </div>
        </>
    )
}