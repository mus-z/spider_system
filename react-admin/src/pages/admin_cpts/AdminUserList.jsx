import AdminUserTable from '@/components/AdminUserTable'
function AdminUserListWithoutConnect(props) {
    return (
        <>
            <div className="admin-userlist-main">
                <AdminUserTable />
            </div>
        </>
    )
}

export const AdminUserList = AdminUserListWithoutConnect