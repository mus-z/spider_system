import React, { useEffect, useRef, useState } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import {
    PlusOutlined,
} from "@ant-design/icons";
import { Modal, Input, Divider, Collapse, Tooltip, Tag } from "antd";
import AdminArtTable from '@/components/AdminArtTable'
const { Panel } = Collapse;
const { confirm } = Modal;
function AdminComponyWithoutConnect(props) {
    let { compony: stockcode } = props.match.params //公司页，拿到公司的stock_code
    //公司信息，公司文章列表，公司板块修改等
    let [listed, setListed] = useState({})
    let [plates, setPlates] = useState([])
    let [inputVisible, setinputVisible] = useState(false)
    let [inputValue, setinputValue] = useState('')
    let inputRef = useRef(null)
    const resetTag = async () => {
        let url = require('@/String.js').server + 'admingetplatesbystock'
        axios({
            method: 'get',
            withCredentials: true,
            url: url + '/' + stockcode,
        }).then(res => {
            if (res.data.success) {
                const { data } = res
                // console.log(data)
                setPlates(data.plates)
            } else {
                console.trace(res)
            }
        }, e => {
            console.trace(e)
        })
    }
    useEffect(() => {
        // console.log(props)
        let url = require('@/String.js').server + 'admingetcomponybystock'
        axios({
            method: 'get',
            withCredentials: true,
            url: url + '/' + stockcode,
        }).then(res => {
            if (res.data.success) {
                const { data } = res
                const tmp = JSON.parse(data.listed.scope)
                const scope = {}
                for (let i = 0; i < tmp.length; i++) {
                    const cur = tmp[i]
                    let { key, value } = cur
                    if (scope[key]) scope[key].push(value)
                    else scope[key] = [value,]
                }
                let listed = {
                    ...data.listed,
                    scope: scope
                }
                setListed(listed)
                // console.log(listed)
            } else {
                props.history.goBack()
                console.trace(res)
            }
        }, e => {
            props.history.goBack()
            console.trace(e)
        })
        resetTag()
    }, [])
    useEffect(() => {
        if (inputVisible) {
            inputRef.current.focus()
        }
    }, [inputVisible])
    const addTag = (value) => {
        let url = require('@/String.js').server + 'adminaddoneplate'
        axios({
            method: 'post',
            withCredentials: true,
            url: url,
            data: {
                value,
                stockcode,
            }
        }).then(res => {
            if (res.data.success) {
                const { data } = res
                // console.log(data)
                resetTag()
            } else {
                console.trace(res)
            }
        }, e => {
            console.trace(e)
        })
    }
    const delTag = (value, Id) => {
        // console.log(value, Id)
        let url = require('@/String.js').server + 'admindeloneplate'
        axios({
            method: 'post',
            withCredentials: true,
            url: url,
            data: {
                value,
                Id,
            }
        }).then(res => {
            if (res.data.success) {
                const { data } = res
                // console.log(data)
                resetTag()
            } else {
                console.trace(res)
            }
        }, e => {
            console.trace(e)
        })
    }
    // http://data.eastmoney.com/stockdata/002197.html
    return (
        <div className="admin-stock-main">
            <h1><Tooltip
                title={() => (<a href={`http://data.eastmoney.com/stockdata/${stockcode}.html`} target="_blank" >东方财富</a>)}
            >({stockcode}){listed.company}
            </Tooltip>
            --
            <Tooltip title={() => {
                    switch (listed.type) {
                        case "SH": {
                            return '沪市'
                        }
                        case 'SZ': {
                            return '深市'
                        }
                    }
                }} >
                    <a onClick={() => {
                        props.history.push({
                            pathname: '/admin/stocklist',
                            state: {
                                searchValue: listed.type
                            }
                        })
                    }}>
                        {listed.type}
                    </a></Tooltip></h1>
            <Divider></Divider>
            <div className="admin-stock-plates">
                {
                    plates && plates.map((item, index) => {
                        return (
                            <Tag key={index}
                                color="volcano"
                                closable={true}
                                onClose={(e) => {
                                    e.preventDefault();//阻止删除，等待请求后重新渲染
                                    // console.log(e);
                                    confirm({
                                        title: '删除Plate-' + item.stock_plate + '-(' + listed.company + ') ?',
                                        onOk() {
                                            delTag(item.stock_plate, item.Id)
                                        }
                                    })
                                }}
                            >
                                <span
                                    style={{
                                        cursor: 'alias'
                                    }}
                                    onClick={() => {
                                        // console.log(item.stock_plate)
                                        props.history.push({
                                            pathname: '/admin/stocklist',
                                            state: {
                                                searchValue: item.stock_plate
                                            }
                                        })
                                    }}>{item.stock_plate}</span>
                            </Tag>
                        )
                    })

                }
                {inputVisible && (
                    <Input
                        ref={inputRef}
                        type="text"
                        size="small"
                        className="tag-input"
                        value={inputValue}
                        onChange={(e) => {
                            setinputValue(e.target.value)
                        }}
                        onBlur={() => {
                            setinputVisible(false)
                            setinputValue('')
                            console.log('取消编辑')
                        }}
                        onPressEnter={() => {
                            setinputVisible(false)
                            setinputValue('')
                            addTag(inputValue)
                        }}
                    />
                )}
                {!inputVisible && (
                    <Tag className="site-tag-plus" onClick={() => {
                        setinputVisible(true)
                    }}>
                        <PlusOutlined /> New Plate
                    </Tag>
                )}
            </div>
            <Divider></Divider>
            <div className="admin-art-list" style={{ marginTop: '2rem' }}>
                <AdminArtTable {...props} stock={stockcode} />
            </div>
            <Collapse bordered={false}  >
                {
                    listed && listed.scope && Object.keys(listed.scope).map((key, index) => {
                        const value = listed.scope[key]
                        return (
                            <Panel header={key} key={"" + key + index}>
                                {
                                    value.map((item, i) => {
                                        return <p style={{ textIndent: '2em' }} key={"" + key + index + ',' + i} >{item}</p>
                                    })
                                }
                            </Panel>
                        )
                    })
                }

            </Collapse>

        </div>
    )
}
const mapState = state => ({
    admin: state.admin,
    breakpoint: state.breakpoint,
})

const mapDispatch = ({ admin: { setAdmin, changeProps }, breakpoint: { setBreakPoint } }) => ({
    setAdmin: (obj) => setAdmin(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setBreakPoint: (v) => setBreakPoint(v)
})
export const AdminCompony = connect(mapState, mapDispatch)(AdminComponyWithoutConnect)