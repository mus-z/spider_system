import { Form, Input, Button, message, Divider } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import '@/css/Login.less'
import axios from 'axios'
import React, {useEffect } from "react";
import LogCheck from '@/utils/LoginCheck'
function Login(props) {
    const { history } = props
    return (
        <>
            <div className='admin-login-main'>
                <NormalLoginForm history={history} />
            </div>
        </>
    )
}
export default Login

const NormalLoginForm = (props) => {
    useEffect(async () => {
        //console.log(props)
        let ck = await LogCheck(localStorage.getItem('openId'))
        console.log({ ck })
        if (ck) {
            props.history.replace(`/admin`)
        } else {
            localStorage.removeItem('openId')
        }
    }, [])
    const onFinish = (values) => {
        // console.log('Received values of form: ', values);
        //props.history.push('/')
        let url = require('@/String.js').server + 'adminlogin'
        axios.post(url, values, { withCredentials: true, }).then(res => {
            const { data } = res
            if (!data) {
                message.warn('登陆发生错误')
            } else {
                if (data.success) {
                    //登陆成功
                    message.success(data.msg)
                    localStorage.setItem("openId", data.openId);
                    props.history.push('/admin', { openId: data.openId })
                } else {
                    message.warn(data.msg)
                    localStorage.removeItem("openId");
                }
            }
        },(e)=>{
            console.log(e+'')
            message.warn(e+'')
        })
    };

    return (
        <Form
            name="normal_login"
            className="login-form"
            onFinish={onFinish}
        >
            <div className="login-form-header">
                <h1>Spider System Admin</h1>
            </div>
            <Divider />
            <Form.Item
                name="admin"
                initialValue="admin"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Username!',
                    },
                ]}
            >
                <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
            </Form.Item>
            <Form.Item
                name="password"
                rules={[
                    {
                        required: true,
                        message: 'Please input your Password!',
                    },
                ]}
            >
                <Input
                    prefix={<LockOutlined className="site-form-item-icon" />}
                    type="password"
                    placeholder="Password"
                />
            </Form.Item>

            <Form.Item>
                <Button type="dashed" htmlType="submit" className="login-form-button" block>
                    登入
                </Button>
            </Form.Item>
        </Form>
    );
};
