import React, { useState, useEffect, useMemo } from "react";
import { connect } from 'react-redux'
import { Layout, Menu, Button, Tooltip, message } from "antd";
import { notification } from "antd";
import {
    PieChartOutlined,
    FileOutlined,
    CommentOutlined,
    UserOutlined,
    CloseCircleOutlined,
    UnorderedListOutlined,
} from "@ant-design/icons";
import { Route, Redirect } from "react-router-dom";
import "@/css/Home.less";
import axios from 'axios'
import LogCheck from '@/utils/LoginCheck'
import { AdminArtList, AdminControl, AdminData, AdminMessage, AdminUserList, AdminArticle, AdminCompony, AdminStockList, AdminPlateList, AdminPlate, AdminUserMain } from './admin_cpts'
import AdminBreadcrub from '@/components/AdminBreadcrub'
import Spinner from 'react-spinkit'
import { useScroll } from 'ahooks';
import socket from '@/socketIO.js'
import AnimatedSwith from '@/components/AnimatedSwith'

const { Header, Content, Footer, Sider } = Layout;
const mapState = state => ({
    admin: state.admin,
    breakpoint: state.breakpoint,
})

const mapDispatch = ({ admin: { setAdmin, changeProps }, breakpoint: { setBreakPoint } }) => ({
    setAdmin: (obj) => setAdmin(obj),
    changeProps: (key, value) => changeProps({ key, value }),
    setBreakPoint: (v) => setBreakPoint(v)
})

function Home(props) {
    const scroll = useScroll(document);
    const siderTop = 100; //预留的sider顶端距离
    const [siderStyle, setSiderStyle] = useState({ paddingTop: siderTop + 'px' })
    const menuKey = useMemo(() => {
        let paths = props.location.pathname.split('/')
        return (paths[2])
    }, [props.location.pathname])//保存sider的默认值
    let [collapsed, setCollapsed] = useState(false);
    // let [breakpoint, setBreakpoint] = useState(false);
    const { breakpoint, setBreakPoint } = props
    let [ck, setck] = useState(true)
    let [checked,setchecked]=useState(false)
    const onCollapse = (collapsed) => {
        //console.log(collapsed);
        setCollapsed(collapsed);
    };
    const handleClick = function (e) {
        props.history.push(`/admin/${e.key}`)
    }
    const backToLogin = () => {
        socket.disconnect()
        localStorage.removeItem('openId')
        setck(false)
        props.history.replace(`/login`)
    }

    useEffect(async () => {
        ck = await LogCheck(localStorage.getItem('openId'))
        setck(ck)
        setchecked(true)
        console.log({ ck })
        if (!ck) {
            backToLogin()
        } else {
            let url = require('@/String.js').server + 'adminquery'
            axios.get(url, { withCredentials: true, }).then(res => {
                props.setAdmin(res.data)
            }, e => {
                console.trace(e)
            })
            //socket

            // socket.on('msg', (msg) => {
            //     console.log(msg)
            // })
        }
        return () => {
            socket.removeAllListeners('new_art') //get_new_art取消事件
        }
    }, [])
    useEffect(() => {
        if (checked&&ck) {
            if (!socket.id) {
                socket.connect()
            }
            let isCrawling = props.admin.timer_state
            const get_new_art = (data) => {
                if (data) {
                    console.log(data)
                    const { newarr, md5 } = data
                    if (newarr) {
                        notification.success({
                            message: `new Art(${newarr.length || 0}):`,
                            description: <div style={{ whiteSpace: "pre-line", maxHeight: '50vh', overflow: 'auto' }}>
                                {newarr.join(';\n')}
                            </div>
                            // duration: 3,
                        })
                    }
                    if (md5) {
                        sessionStorage.setItem('md5', md5);
                    }
                }
            }
            window.socket = socket
            socket.on('new_art', get_new_art)
        }
        return () => {
            socket.removeAllListeners('new_art') //get_new_art取消事件
        }
    }, [props.admin.timer_state, ck,checked])
    useEffect(() => {
        setSiderStyle({ paddingTop: siderTop + scroll.top + 'px' })
    }, [scroll.top])

    const testconsole = () => {
        //console.log(scroll)
    }
    const mainmargin = useMemo(() => {
        return breakpoint ? "0 1rem" : "0 3rem"
    }, [breakpoint])
    return (
        !(ck&&checked) ? <div style={{ height: '100vh', display: 'flex', justifyContent: 'center', alignItems: 'center' }}><Spinner name="ball-scale-ripple-multiple" color="coral"></Spinner></div> :
            <>
                {testconsole()}
                <Layout style={{ minHeight: "100vh", marginTop: '-3.5px' }}>
                    <Sider
                        style={siderStyle}
                        breakpoint="lg"
                        collapsedWidth="0"
                        //collapsible
                        collapsed={collapsed}
                        onCollapse={onCollapse}
                        onBreakpoint={(e) => {
                            setBreakPoint(e)
                            e ? setSiderStyle({ position: 'fixed', height: '100vh', opacity: 0.9, zIndex: 100, }) : setSiderStyle({ paddingTop: siderTop + scroll.top + 'px' })
                        }}
                    >
                        <Menu theme='dark' selectedKeys={[menuKey ? menuKey : 'control']} defaultSelectedKeys={[menuKey ? menuKey : 'control']} mode='inline' onClick={handleClick}>
                            <Menu.Item key='control' icon={<FileOutlined />} >爬虫控制</Menu.Item>
                            <Menu.Item key='artlist' icon={<UnorderedListOutlined />}>公告列表</Menu.Item>
                            <Menu.Item key='stocklist' icon={<UnorderedListOutlined />}>公司列表</Menu.Item>
                            <Menu.Item key='platelist' icon={<UnorderedListOutlined />}>板块列表</Menu.Item>
                            <Menu.Item key='userlist' icon={<UserOutlined />} >用户管理</Menu.Item>
                            <Menu.Item key='message' icon={<CommentOutlined />} >用户交流</Menu.Item>
                            <Menu.Item key='data' icon={<PieChartOutlined />} >
                                数据展示
                        </Menu.Item>
                        </Menu>
                        <div style={{
                            display: "flex",
                            justifyContent: "center"
                        }}>
                            <Tooltip placement="right" title={'logout'}>
                                <Button type="link" onClick={() => {
                                    let url = require('@/String.js').server + 'adminlogout'
                                    axios.get(url, { withCredentials: true, }).then(res => {
                                        const { data } = res
                                        if (!data) {
                                            message.warn('发生错误')
                                        } else {
                                            if (data.success) {
                                                //登出成功
                                                backToLogin()
                                                message.success(data.msg)
                                            } else {
                                                message.warn(data.msg)
                                            }
                                        }
                                    })
                                }}>
                                    <CloseCircleOutlined />
                                </Button>
                            </Tooltip>
                        </div>
                    </Sider>
                    <Layout className='site-layout'>
                        <Header className='site-layout-background' style={{ padding: 0, paddingLeft: '10%', fontSize: "2rem", fontWeight: '600' }}>
                            Spider Admin
                    </Header>
                        <Content style={{ margin: mainmargin }}>
                            <AdminBreadcrub location={props.location} />
                            <div
                                className='site-layout-background'
                                style={{ padding: 24, minHeight: 360 }}
                            >
                                <div>
                                    <AnimatedSwith>
                                        <Route path='/admin/control' exact component={AdminControl} />
                                        <Route path='/admin/data' exact component={AdminData} />
                                        <Route path='/admin/artlist' exact component={AdminArtList} />
                                        <Route path='/admin/stocklist' exact component={AdminStockList} />
                                        <Route path='/admin/platelist' exact component={AdminPlateList} />
                                        <Route path='/admin/plates/:plateId' exact component={AdminPlate} />
                                        <Route path='/admin/message' exact component={AdminMessage} />
                                        <Route path='/admin/userlist' exact component={AdminUserList} />
                                        <Route path='/admin/:compony' exact component={AdminCompony} />
                                        <Route path='/admin/user/:Id' exact component={AdminUserMain} />
                                        <Route path='/admin/:compony/:code' exact component={AdminArticle} />
                                        <Redirect from='/admin' to='/admin/control' />
                                    </AnimatedSwith>
                                    <style>{`
                                            .switch-wrapper {
                                                position: relative;
                                            }
                                            
                                            `}</style>
                                </div>
                            </div>
                        </Content>
                        <Footer style={{ textAlign: "center", color: '#aaa' }}>
                            @admin-spider
                    </Footer>
                    </Layout>
                </Layout>
            </>
    );
}
export default connect(mapState, mapDispatch)(Home);