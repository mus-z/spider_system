import io from 'socket.io-client'
import {server, host} from '@/String'
export const socket = io(host+':7001/admin', {
    // query: {
    //     room: 'demo',
    //     userId: `client_${Math.random()}`,
    // },
    autoConnect: false,//默认不开启
    transports: ['websocket'],
})

export function initSocket() {
    let socket_id
    console.log('init')
    socket.on('connect', () => {
        let id = socket ?.id
        socket_id = id
        console.log({
            socket,
            id
        })
        socket.emit('server', id)
    })
    socket.on('disconnect', (s) => {
        console.log('disconnect', s)
    })
    socket.on('res', (...res) => {
        console.log(res)
    })
}
initSocket()
socket.disconnect()
export default socket