import React from 'react';
import ReactDOM from 'react-dom';
//import 'antd/dist/antd.less' //配置了按需引入
//import TestRematch from './test_rematch/testRematch'
import Main from '@/pages/Main'
import { Provider } from 'react-redux'
import store from '@/redux/store'
import { getPersistor } from '@rematch/persist'
import { PersistGate } from 'redux-persist/es/integration/react'
import Spinner from 'react-spinkit'
import 'emoji-mart/css/emoji-mart.css'
const persistor = getPersistor()
ReactDOM.render(
  <>
    {/* <TestRematch/> */}
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={<Spinner />}>
        <Main  />
      </PersistGate>
    </Provider>
  </>,
  document.getElementById('root')
);

