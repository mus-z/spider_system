import axios from 'axios'
export default function LogCheck(openId) {
    return new Promise((resolve) => {
        let url = require('@/String.js').server + 'adminlogcheck'
        try {
            axios.post(url, { openId }, { withCredentials: true, }).then(res => {
                const { data } = res
                if (!data) {
                    resolve(0)
                } else {
                    // console.log(data)
                    resolve(data.success)
                }
            }, (e) => {
                console.error(e)
                resolve(0)
            })
        } catch (e) {
            console.error(e)
            resolve(0)
        }
    })

}