import dayjs from 'dayjs'
export function TimeString(time){
    let t = dayjs(parseInt(time))
    return t.format('YYYY/MM/DD HH:mm:ss')
}
export default TimeString
export function DayjsFormate(timeS,FormateS){
    return dayjs(parseInt(timeS)).format(FormateS)
}
export let day_js = dayjs