import {
  init
} from '@rematch/core'
import * as models from './models'
import immerPlugin from '@rematch/immer';
import createRematchPersist from '@rematch/persist'
import storage from 'redux-persist/lib/storage'
const persistPlugin = createRematchPersist({
  key: 'rematch',
  storage,
  version: 1,
})

const store = init({
  models,
  plugins: [
    persistPlugin,
    immerPlugin()]
})

export default store