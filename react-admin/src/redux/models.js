export const admin = {
  state: {
    
  }, // 默认变量
  reducers: {
    // 修改state的纯函数
    setAdmin(state, newState) {
      //console.log({state,newState})
      return newState
    },
  },
  effects: {
    // handle state changes with impure functions.
    // 处理异步action
    async testAsync(payload, rootState , k) {
      console.log({payload, rootState , k}) //只能传两个参数过来
    }
  }
}
export const breakpoint={
  state: false,
  reducers: {
    // 修改state的纯函数
    setBreakPoint(state, newState) {
      //console.log({state,newState})
      return newState
    },
  },
}