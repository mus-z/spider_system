import React, { useState, useEffect } from "react";
import { Breadcrumb, Divider } from "antd";
export default function AdminBreadcrub(props) {
    const [path, setPath] = useState([])//面包屑随路径变化
    useEffect(() => {
        let path = props.location.pathname.split('/').slice(2)
        //console.log(props.location,path)
        setPath(path)
    }, [props.location])
    return (
        <>
            <Divider />
            <Breadcrumb style={{ padding: '1rem 2rem', borderRadius: '1.5rem', marginBottom: '2rem' }}>
                <Breadcrumb.Item>爬虫管理系统</Breadcrumb.Item>
                {path.map((item, index) => {
                    return (
                        <Breadcrumb.Item key={index}>{item}</Breadcrumb.Item>
                    )
                })}
            </Breadcrumb>
        </>
    )
}