import { Collapse, Row, Divider, Typography, message, } from 'antd';
import { useEffect, useRef, useState, useMemo } from 'react';
import axios from 'axios';
import { server } from '@/String';
import MyComment from './MyComment'
import dayjs from 'dayjs';
const { Panel } = Collapse

function makeComments(comments) {
    //把mysql返回的数组转化成一层和二层的形式
    let c = []
    let p = {}
    for (let item of comments) {
        //father_id就是之后提交评论记录的楼id
        if (!item.head_id) {
            item.father_id = item.Id
            c.push(item)
        } else {
            item.father_id = item.head_id
            p[item.head_id] = [...(p[item.head_id] || []), item]
        }
    }
    return [c, p]
}
function CommentBoard(props) {

    const { user_id, art_code, router } = props
    const [ansTo, setansTo] = useState({})
    const [comments, setcomments] = useState([])
    const [minutes, setminutes] = useState(0)
    const text = useRef(null)
    const [c, p] = useMemo(() => {
        return makeComments(comments)
    })
    const handledelete = (item, user_id) => {
        if (item.user_id == user_id || user_id == 'admin') {
            axios({
                url: server + 'admindeletecomment',
                method: 'get',
                withCredentials: true,
                params: {
                    comment_id: item.Id,
                    comment_u_id: item.user_id,
                    user_id,
                    art_code: item.art_code,
                }
            }).then(res => {
                const { data } = res
                if (data.success) {
                    setcomments(data.comments)
                    message.success('删除成功')
                }
                console.log(data)
            })
        }
    }
    const getcomment = (item) => {
        if (user_id == "admin" && item.disabled > 0) return <span style={{
            textDecoration: "line-through"
        }}>{item.comment}</span>

        if (item.disabled == 1) {
            return <span style={{
                textDecoration: "line-through"
            }}>内容已被用户删除</span>
        }
        if (item.disabled == 2) {
            return <span style={{
                textDecoration: "line-through"
            }}>内容已被管理员删除</span>
        }
        return item.comment
    }
    useEffect(() => {
        let timer = setInterval(() => {
            setminutes(dayjs().minute())
        }, 60 * 1000)
        return () => {
            clearInterval(timer)
        }
    }, [])
    useEffect(() => {
        axios({
            method: 'get',
            url: server + 'admingetartcommentbycode',
            withCredentials: true,
            params: {
                art_code,
            }
        }).then(res => {
            const { data } = res
            console.log(data)
            setcomments(data.comments)
        })
        // console.log(props)
    }, [art_code])
    const MyCommentOptions = {
        text,
        setansTo,
        minutes,
        user_id,
        handledelete,
        getcomment,
    }

    return (<>
        <Collapse ghost activeKey={["comment"]}>
            <Panel header={<span style={{ color: "#aaa", fontSize: "1.2rem" }}>评论区({comments.length}条)</span>}
                style={{
                    borderTop: "1px solid #eaeaea",
                    borderBottom: "1px solid #eaeaea"
                }}
                showArrow={false}
                key="comment"
            >
                <div className="user-art-comment"
                    style={{
                        maxHeight: "50vh",
                        overflow: 'auto',
                        borderBottom: "1px solid #eaeaea"
                    }} >
                    {
                        c.map((item, index) => {
                            return (
                                <MyComment
                                    item={item}
                                    key={item.Id}
                                    {...MyCommentOptions}
                                    floor={index + 1}
                                >
                                    {p[item.Id] && <div><Collapse ghost>
                                        <Panel header={
                                            <span
                                                style={{
                                                    fontSize: '.5rem',
                                                    color: '#999',
                                                }}
                                            >对{item.user_nickname}的回复({p[item.Id].length})：
                                        </span>} showArrow={false}>
                                            {
                                                p[item.Id] && p[item.Id].map(item => {
                                                    return (
                                                        <MyComment
                                                            item={item}
                                                            key={item.Id}
                                                            {...MyCommentOptions}
                                                        />)
                                                })
                                            }
                                        </Panel>
                                    </Collapse>
                                        <style>{`
                                    .inner_comment .ant-collapse-item{
                                        background-color:#eceeec;
                                    }
                                    `}</style>
                                    </div>
                                    }
                                </MyComment>
                            )
                        })
                    }
                </div>
                <Divider />
                {ansTo.Id && <Row style={{ width: "100%", marginBottom: '.2rem' }}>
                    <div style={{
                        color: "#aaa",
                        fontSize: '.5rem',
                        width: "100%"
                    }}>回复: {ansTo.user_nickname}：<Typography.Text
                        ellipsis
                        style={{
                            whiteSpace: "nowrap",
                            width: "70%",
                        }}>{getcomment(ansTo)}</Typography.Text></div>
                </Row>}
            </Panel>
        </Collapse>
        <style global="true" >{`
                .ant-collapse-header{
                    border-bottom:1px solid #eaeaea;
                }
                .user-art-comment .ant-comment-nested{
                    margin-left:0!important;
                }
                .user-art-comment .ant-comment-inner{
                    border-bottom:1px solid #eaeaea;
                }
                .ant-comment-content-detail{
                    overflow:auto;
                    background: #bfbfbf70;
                    padding: .5rem;
                    white-space: break-spaces;
                    margin:1rem 0 0 1rem;
                }
        `}</style>
    </>)
}
export default CommentBoard