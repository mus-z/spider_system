import { Row, Comment, Empty } from 'antd'
import dayjs from 'dayjs'
export default function ChatList(props) {
    const { msgList, user_id, user_nickname } = props
    return (<>
        {user_id ?
            msgList.map((item, key) => {
                return (<Row key={item.msg_time}>
                    {item.isAdmin ?
                        <div style={{ width: '100%' }} className="right" >
                            <Comment
                                style={{
                                    float: 'right',
                                    maxWidth: '50%',
                                    minWidth: "40%"
                                }}
                                author={<span style={{ fontSize: "1.2rem", color: "#fff", cursor: 'pointer' }}>{'admin'}</span>}
                                content={<div
                                    style={{
                                        width: '100%',
                                        whiteSpace: 'break-spaces',
                                        backgroundColor: '#fff',
                                        padding: '.5rem 1rem',
                                        borderRadius: "1rem",
                                        marginTop: '1rem',
                                        minHeight:'2rem',
                                    }}
                                >
                                    {item.text}
                                </div>}
                                datetime={<span style={{ color: '#888' }}>{dayjs(item.msg_time).format('YYYY-MM-DD HH:mm')}</span>}
                            />
                        </div>
                        :
                        <div style={{ width: '100%' }} className="left">
                            <Comment
                                style={{
                                    float: 'left',
                                    maxWidth: '50%',
                                    minWidth: "40%"
                                }}
                                author={<span style={{ fontSize: "1.2rem", color: "#fff", cursor: 'pointer', width: '100%' }}>{user_nickname}</span>}
                                content={<div
                                    style={{
                                        width: '100%',
                                        whiteSpace: 'break-spaces',
                                        backgroundColor: '#fff',
                                        padding: '.5rem 1rem',
                                        borderRadius: "1rem",
                                        marginTop: '1rem',
                                        minHeight:'2rem',
                                    }}
                                >
                                    {item.text}
                                </div>}
                                datetime={<span style={{ color: '#888' }}>{dayjs(item.msg_time).format('YYYY-MM-DD HH:mm')}</span>}
                            />
                        </div>
                    }
                    <style>{`
                            .right .ant-comment-content-author{
                                justify-content: flex-end;
                            }
                            .left .ant-comment-content-author-name{
                                margin-right:100%;
                            }
                            .right .ant-comment-content-author-time{
                                margin-left:100%;
                            }
                            `}</style>
                </Row>)
            }) : <Empty description={""} />
        }
    </>)
}