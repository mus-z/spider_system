import { Table, Space } from "antd"
import axios from "axios"
import dayjs from "dayjs";
import { useEffect, useState } from "react"
import { server } from "../String"
import { NavLink } from "react-router-dom";
const { Column, ColumnGroup } = Table;
function AdminUserTable(props) {
    const [users, setusers] = useState([])
    useEffect(() => {
        axios({
            method: 'get',
            withCredentials: true,
            url: server + 'admingetallusers',
        }).then(res => {
            const { data } = res
            if (data.success) {
                // console.log(data)
                setusers(data.users)
            }
        })
    }, [])
    return (<>
        <div className="admin-userlist-table">
            <Table dataSource={users} 
            bordered
            footer={()=><div style={{color:'#aaa'}}>共{users.length}个</div>}
            rowKey={(item)=>{return item.Id}}
            pagination={{
                pageSize:5
            }}>
                <ColumnGroup title="唯一性账号">
                    <Column title="用户名" dataIndex="user_name" key="user_name" 
                    />
                    <Column title="邮箱" dataIndex="user_email" key="user_email" />
                </ColumnGroup>
                <Column title="昵称" dataIndex="user_nickname" key="user_nickname" />
                <Column title="注册时间" dataIndex="user_creattime" key="user_creattime" 
                render={(v)=>dayjs(+v).format('YYYY-MM-DD')} 
                sorter={(a, b) => a.user_creattime - b.user_creattime}
                />
                <Column
                    title="Action"
                    key="action"
                    render={(text, record) => (
                        <Space size="middle">
                            <NavLink to={`/admin/user/${record.Id}`}>查看</NavLink>
                        </Space>
                    )}
                />
            </Table>
        </div>
    </>)
}
export default AdminUserTable