import { Row, Col, Input, Button, Tooltip } from 'antd'
import dayjs from 'dayjs'
import ChatList from "@/components/ChatList"
import { SmileOutlined } from "@ant-design/icons"
import { useEffect, useState ,useRef} from 'react'

import { Picker } from 'emoji-mart'

export default function ChatMain(props) {
    const [text, settext] = useState("")
    const { item, user_id, socket } = props
    // console.log(item)
    const [showEmojiModal, setshowEmojiModal] = useState(false)
    const ref = useRef(null)
    const handleMag = () => {
        // console.log(text,user_id)
        socket.emit('chat', {
            user_id,
            text,
            msg_time: +dayjs(),
        })
        settext("")
    }
    let searchEmoji = (emoji, e) => {
        // console.log("emoji, event=====", emoji, e)
        const native = emoji.native
        settext(text + native)
    }
    useEffect(()=>{
        // console.log(ref)
        ref.current?.scrollIntoView()
    },[item])
    return (<>
        {user_id ?
            <main style={{
                flex: 1,
                height: "100%",
                border: "1px solid #eaeaea",
                backgroundColor: 'rgb(244 201 124 / 53%)',
                boxShadow: 'rgb(0 0 0 / 8%) 3px 2px 9px 4px',
                display: 'flex',
                flexDirection: 'column'
            }}>
                <header
                    style={{
                        backgroundColor: "#f5f5f5",
                        padding: "0.9rem 2rem",
                        fontSize: "1.2rem",
                        fontWeight: 'bold',
                        color: '#888',
                        borderLeft: '1.5rem solid #495866',
                        borderBottom: '1px solid #eaeaea',
                        cursor: 'pointer'
                    }}
                >
                    与{item.user_nickname}的对话
            </header>
                <main
                    style={{
                        height: '25rem',
                        border: '2px solid #eaeaea',
                        overflow: 'auto',
                    }}
                >
                    <Row>
                        <Col style={{ width: '100%', padding: '.5rem .5rem .2rem .5rem' }}>

                            <ChatList msgList={item.msgs} user_id={user_id} user_nickname={item.user_nickname} />
                            <div id="msg_end" style={{height:'0px', overflow:'hidden'}} ref={ref}></div>
                        </Col>
                    </Row>

                </main>
                <div className="emoji_container" style={{position:'absolute'}}>
                    {showEmojiModal && <Picker
                        // set='apple'
                        emoji=''
                        showPreview={false}
                        onClick={(emoji, e) => searchEmoji(emoji, e)} />}
                </div>
                {/* emoji=''设置preview默认状态下不显示图片 */}
                <div style={{ padding: '.5rem', cursor: 'pointer' ,backgroundColor:'#ddd'}} 
                onClick={() => setshowEmojiModal(!showEmojiModal)}>
                    <Tooltip title="表情">
                        <SmileOutlined style={{ fontSize: 20, color: '#787878' }} />
                    </Tooltip>
                </div>
                <div
                    style={{
                        flex: 1,
                        backgroundColor: "#eee",
                        display: 'flex'
                    }}
                ><Input.TextArea style={{ height: "100%" }}
                    bordered
                    value={text}
                    onChange={(e) => { settext(e.target.value) }}
                    />
                    <Button style={{ height: "100%" }} type="primary"
                        onClick={() => {
                            handleMag()
                        }}
                    >发送</Button>
                </div>
            </main> : <main style={{
                flex: 1,
                height: "100%",
                border: "1px solid #eaeaea",
                backgroundColor: '#eae3d399',
                boxShadow: 'rgb(0 0 0 / 8%) 3px 2px 9px 4px',
                display: 'flex',
                flexDirection: 'column'
            }}>
                <span style={{ color: "#aaa", margin: '2rem' }}>点击左侧会话列表...</span>
            </main>
        }

    </>)
}