import React, { useState, useEffect } from 'react';
import {Table, Tooltip, Typography, Space } from 'antd';
import axios from 'axios'
import { server } from '@/String';
import dayjs from 'dayjs';

const { Column } = Table;
function ArtHistoryTable(props) {

    const { user_id } = props
    const [views, setviews] = useState([])
    // console.log(props)
    useEffect(() => {
        console.log(props)
        axios({
            url: server + 'usergetviewarthistory',
            withCredentials: true,
            method: 'get',
            params: {
                user_id,
            }
        }).then(res => {
            const { data } = res
            if (data.success) {
                setviews(data.views)
                console.log(data)
            }
        })
    }, [])
    return (<>
        <div className="visitor-arthistory-table">
            <Table dataSource={views}
                bordered
                footer={() => <div style={{ color: '#aaa' }}>共{views.length}个</div>}
                rowKey={(item) => { return item.art_code }}
                pagination={{
                    pageSize: 5
                }}
            >
                <Column title="公司" dataIndex="short_name" key="short_name"
                    render={(v, item) => {
                        return (
                            <Tooltip title={item.stock_code} placement="topLeft" >
                                <Typography.Text ellipsis={false} >
                                     <a 
                                     href={`/company/${item.stock_code}`} 
                                     style={{whiteSpace:'nowrap'}}
                                     >{item.short_name}
                                     </a>
                                     </Typography.Text>
                            </Tooltip>
                        )
                    }}
                />
                <Column title="文章" dataIndex="title" key="title"
                width="100px"
                    render={(v, item) => {
                        return (
                            <div>
                            <Tooltip title={<a target="_blank" href={item.url} >{item.title}</a>} placement="topLeft" >
                                <Typography.Text ellipsis={true} style={{width:'100%'}}> <span>{item.title}</span></Typography.Text>
                            </Tooltip>
                            </div>
                        )
                    }}
                />
                <Column title="浏览时间" dataIndex="last_view_time" key="last_view_time"
                    render={(v) => <span style={{whiteSpace:'nowrap'}}>{dayjs(+v).format('YYYY-MM-DD:HH:mm:ss')}</span>}
                    sorter={(a, b) => a.last_view_time - b.last_view_time}
                />
                <Column
                    title="Action"
                    key="action"
                    render={(text, item) => (
                        <Space size="middle">
                            <a href={`/admin/${item.stock_code}/${item.art_code}`}>查看</a>
                        </Space>
                    )}
                />
            </Table>
        </div>
    </>)
}

export default (ArtHistoryTable)