import React, { useState, useEffect, useMemo } from 'react';
import { Button, Table, Tooltip, Input, Row, Col, DatePicker, Typography, message, notification } from 'antd';
import { DayjsFormate } from '@/utils/TimeString'
import axios from 'axios'
import { NavLink } from "react-router-dom";
import dayjs from 'dayjs';
import { connect } from 'react-redux'
import {
    RedoOutlined,
    SearchOutlined,
} from "@ant-design/icons";
const { RangePicker } = DatePicker;

function AdminArtTable(props) {
    const [data, setData] = useState([])
    const [total, setTotal] = useState(0)
    const { breakpoint,admin,setAdmin } = props
    const [isLoading, setIsLoading] = useState(false)
    const [pre, setPre] = useState(null)
    const [next, setNext] = useState(null)
    const [searchValue, setSearchValue] = useState('')
    const [md5, setmd5_] = useState(sessionStorage.getItem('md5') || '')
    const setmd5 = (md5) => { sessionStorage.setItem('md5', md5); setmd5_(md5); }
    const storage = sessionStorage
    const stock = props.stock || null
    let option = useMemo(() => {
        return {
            searchValue, pre, next, stock
        }
    }, [searchValue, pre, next, stock])
    const [page, setPage] = useState(+sessionStorage.getItem(JSON.stringify(option) + 'page') || 0)
    const [pagesize, setPagesize] = useState(+sessionStorage.getItem(JSON.stringify(option) + 'pagesize') || 10)

    const pageInit = (page, pagesize) => {
        setPage(page - 1)
        setPagesize(pagesize)
        sessionStorage.setItem(JSON.stringify(option) + 'page', page - 1)
        sessionStorage.setItem(JSON.stringify(option) + 'pagesize', pagesize)
    }
    const columnsx = [
        {
            title: '标题',
            dataIndex: 'title',
            render: (value, obj) => {
                return (
                    <Tooltip placement="topLeft" title={() => <div><h4 style={{ color: '#fff' }} >{'(' + obj.art_code + ')'}</h4> {obj.title}</div>} key={value} >
                        <NavLink to={{
                            pathname: `/admin/${obj.stock_code}/${obj.art_code}`, state: {
                                ref: obj
                            }
                        }} >
                            <Typography.Text ellipsis={true} style={{ width: 400 }} className={'ant-btn-link'}> {value}</Typography.Text>
                        </NavLink>
                    </Tooltip>
                )
            }
        },
        {
            title: '公司',
            dataIndex: 'short_name',
            render: (value, obj) => {
                return (
                    <Tooltip placement="topRight" title={obj.stock_code} key={value} >
                        <NavLink to={{
                            pathname: `/admin/${obj.stock_code}`, state: {
                                ref: obj
                            }
                        }} >
                            <Button type="link" size="small">{value}</Button>
                        </NavLink>
                    </Tooltip>
                )
            }
        },
        {
            title: '公告日期',
            dataIndex: 'notice_date',
            render: (e) => {
                return DayjsFormate(e, 'YYYY-MM-DD')
            },
        },
        {
            title: '爬取时间',
            dataIndex: 'crawl_time',
            render: (e) => {
                return DayjsFormate(e, 'YYYY-MM-DD HH:mm')
            },
        },
    ]
    function setList(data, page, pagesize, md5, option) {
        try {
            storage.setItem(JSON.stringify([page, pagesize, md5, option]), JSON.stringify(data))
        } catch (e) {
            if (e.name == 'QuotaExceededError') {
                console.log('超出本地存储限额！');
                //如果历史信息不重要了，可清空后再设置
                clearList(md5, 3)//保留三个
                storage.setItem(JSON.stringify([page, pagesize, md5, option]), JSON.stringify(data))
            }
        }
    }
    function getList(page, pagesize, md5, option) {
        return JSON.parse(storage.getItem(JSON.stringify([page, pagesize, md5, option])))
    }
    function clearList(md5, n = 0) {
        for (let i = 0; i < storage.length - n; i++) {
            let key = storage.key(i)
            if (storage[key].indexOf(md5) > 0) {
                // console.log(key)
                storage.removeItem(key)
            }
        }
    }
    const tableSearch = async (page, pagesize, option) => {
        setIsLoading(true)
        let url = require('@/String.js').server + 'admingetlist'
        async function getlist() {
            try {
                let res = await axios.post(url, { page, pagesize, md5, option, openId: localStorage.getItem('openId') }, { withCredentials: true, })
                const { data } = res
                setData(data.arr)
                setTotal(data.total)
                setmd5(data.md5)
                setList(data, page, pagesize, data.md5, option)
                // console.log(data)
            } catch (e) {
                console.trace({ e })
            }
        }
        setData([])
        setTotal(0)
        await axios.get(require('@/String.js').server + 'admincheckmd5',
            { withCredentials: true, })
            .then(async (res) => {
                const { data } = res
                if (md5 == data.md5) {
                    //没有更新
                    let data = getList(page, pagesize, md5, option)
                    if (!data) {
                        await getlist()
                    } else {
                        setData(data.arr)
                        setTotal(data.total)
                        setmd5(data.md5)
                        setList(data, page, pagesize, md5, option)
                    }
                    // console.log(data)
                } else {
                    //更新了
                    clearList(md5)
                    await getlist()
                }
            })
        setIsLoading(false)
    }
    useEffect(() => {
        tableSearch(page, pagesize, {
            searchValue,
            dateRange: pre ? [(+dayjs(pre)), (+dayjs(next))] : null,
            stock,
        })
        // await axios.post(url, { page, pagesize, openId: localStorage.getItem('openId') }, { withCredentials: true, }).then(res => {
        //     const { data } = res
        //     setData(data.arr)
        //     setTotal(data.total)
        //     //console.log(data)
        // }, e => {
        //     console.trace(e)
        // })
    }, [sessionStorage.getItem('md5'), admin, page, pagesize])
    const spiderOnce = () => {
        pageInit(1, 10)
        setData([])
        setTotal(0)
        setIsLoading(true)
        let url = require('@/String.js').server + 'spideronce'
        axios.get(url, { withCredentials: true, }).then(res => {
            const { data } = res
            if (data.success) {
                if (data.arr && data.arr.length > 0) {
                    // message.success(`更新了(${data.arr.length||0})：\n` + data.arr.join(';\n'))
                    // data.arr.map(item => {
                    //     notification.success({
                    //         message: 'new Art',
                    //         description: item + '',
                    //         // duration: 3,
                    //     })
                    // })
                } else {
                    notification.info({
                        message: '无更新'
                    })
                }
                setAdmin(data.admin)
            } else {
                message.warn('Error')
            }

            // console.log(data)
        }, e => {
            console.trace(e)
        })
    }
    const onSearch = () => {
        pageInit(1, 10)
        setData([])
        // dateRangeInit() //不需要清空
        tableSearch(page, pagesize, {
            searchValue,
            dateRange: pre ? [(+dayjs(pre)), (+dayjs(next))] : null,
            stock
        })
    }
    return (
        <div style={{ width: '100%' }} className="admin-tablelist-div" >
            <Row style={{ marginLeft: breakpoint ? 0 : '5%' }} gutter={{ xs: 2, sm: 20 }}>
                <Col sm={8} xs={20}>
                    <Tooltip placement="topRight" title={'search by title/compony'}>
                        <Input
                            value={searchValue}
                            onChange={(e) => { setSearchValue(e.target.value) }}
                            onPressEnter={async () => {
                                await onSearch()
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col sm={8} xs={20} style={{
                    display: 'flex',
                    alignItems: 'space-between'
                }}>
                    <Tooltip placement="topRight" title={'search by notice_date'}>
                        <div>
                            <RangePicker
                                value={[pre, next]}
                                style={{ width: '100%', height: '100%' }}
                                onChange={function (_, v) {
                                    // console.log(arguments)
                                    if (_) {
                                        //选择了日期范围
                                        const [pre, next] = _
                                        // console.log(pre, next)
                                        // console.log(dayjs(pre), dayjs(next), pre, next, +(dayjs(pre)), +(dayjs(next)))
                                        setPre(pre)
                                        setNext(next)
                                    } else {
                                        setPre('')
                                        setNext('')
                                        //取消选择默认不变动
                                    }

                                }}
                            />
                        </div>
                    </Tooltip>

                </Col>
                <Col sm={8} xs={20}>
                    <Button icon={<SearchOutlined />}
                        onClick={async () => {
                            // console.log({ searchValue, pre, next });
                            await onSearch()
                        }}
                        style={{
                            height: '100%'
                        }}
                    />
                    <Tooltip placement="right" title={'执行一次爬虫'}>
                        <Button onClick={spiderOnce} type="link" size='large'><RedoOutlined />crawling</Button>
                    </Tooltip>
                </Col>
            </Row>
            <Table
                columns={columnsx.filter((value, index) => {
                    if (index == 1 && stock) return false
                    else return true
                })}
                dataSource={data}
                style={{}}
                bordered
                className="admin-tablelist-table"
                loading={isLoading}
                scroll
                size="small"
                style={{
                    marginTop: '1rem',
                    maxWidth: 1000,
                    borderTop: '2px double #ac8901',
                    marginLeft: breakpoint ? 0 : '5%',
                }}

                pagination={{
                    total: total || 0,
                    simple: true,
                    onChange: async (page, pagesize) => {
                        pageInit(page, pagesize)
                        // setData([])
                        // let url = require('@/String.js').server + 'admingetlist'
                        // setIsLoading(true)
                        // await axios.post(url, { page: page - 1, pagesize, openId: localStorage.getItem('openId') }, { withCredentials: true, }).then(res => {
                        //     const { data } = res
                        //     setData(data.arr)
                        //     console.log(data)
                        // }, e => {
                        //     console.trace(e)
                        // })
                        // setIsLoading(false)
                    },
                    showQuickJumper: true,
                    defaultCurrent: +page + 1,
                    current: +page + 1
                }}
                rowKey={(row) => { return row.art_code }}
            >

            </Table>

        </div>)
}
const mapState = state => ({
    admin: state.admin,
    breakpoint: state.breakpoint,
})

const mapDispatch = ({ admin: { setAdmin } }) => ({
    setAdmin: (obj) => setAdmin(obj),
})
export default connect(mapState, mapDispatch)(AdminArtTable)