import React, { useState, useEffect, useMemo } from 'react';
import { Button, Table, Tooltip, Input, Row, Col, Tag } from 'antd';
import axios from 'axios'
import { NavLink } from "react-router-dom";
import {
    ContactsFilled,
    SearchOutlined,
} from "@ant-design/icons";

function AdminStockTable(props) {
    const [data, setData] = useState([])
    const [total, setTotal] = useState(0)
    const { breakpoint } = props
    const [isLoading, setIsLoading] = useState(false)
    const [page, setPage] = useState(+sessionStorage.getItem('page') || 0)
    const [pagesize, setPagesize] = useState(+sessionStorage.getItem('pagesize') || 10)

    const [searchValue, setSearchValue] = useState('')
    const [md5, setmd5_] = useState(sessionStorage.getItem('md5') || '')
    const setmd5 = (md5) => { sessionStorage.setItem('md5', md5); setmd5_(md5); }
    const storage = sessionStorage
    const { plateId, platename } = props
    let option = useMemo(() => {
        return {
            searchValue, plateId
        }
    }, [searchValue, plateId])
    useEffect(() => {
        const sv = props.history?.location?.state?.searchValue
        if (sv) {
            setSearchValue(sv)
        }
    }, [])
    const pageInit = (page, pagesize) => {
        setPage(page - 1)
        setPagesize(pagesize)
        sessionStorage.setItem(JSON.stringify(option) + 'page', page - 1)
        sessionStorage.setItem(JSON.stringify(option) + 'pagesize', pagesize)
    }
    const clickChangeValue = (value) => { setSearchValue(value) }
    // code,company,type,plates
    const columnsx = [
        {
            title: '股票代码',
            dataIndex: 'code',
            render: (value, obj) => {
                return (
                    <div
                        key={value}
                    >
                        {value}
                    </div>
                )
            }
        },
        {
            title: '公司',
            dataIndex: 'company',
            render: (value, obj) => {
                return (
                    <NavLink to={{
                        pathname: `/admin/${obj.code}`, state: {
                            ref: obj
                        }
                    }}
                        key={value}
                    >
                        <Button type="link" size="small">{value}</Button>
                    </NavLink>
                )
            }
        },
        {
            title: '上市类型',
            dataIndex: 'type',
            render: (value) => {
                return (<Tooltip
                    placement="right"
                    key={value}
                    title={() => {
                        switch (value) {
                            case "SH": {
                                return '沪市'
                            }
                            case 'SZ': {
                                return '深市'
                            }
                        }
                    }} ><Tag
                        style={{
                            cursor: 'alias'
                        }}
                        onClick={() => {
                            // console.log(value)
                            clickChangeValue(value)
                        }}
                        icon={<ContactsFilled />}
                        color="#cd201f"
                    ><span>{value}</span></Tag></Tooltip>)
            },
        },
        {
            title: '所属板块',
            dataIndex: 'stock_plates',
            render: (_, obj) => {
                const plates = obj['plates']
                return (
                    <div key={obj.code}>
                        {
                            plates.map(plate => {
                                return (<Tag
                                    color={(() => {
                                        if (plate == searchValue) return 'green'
                                        return (platename == plate ? "red" : 'gold')
                                    })()}
                                    key={_ + plate}
                                ><span
                                    style={{
                                        cursor: 'alias'
                                    }}
                                    onClick={() => {
                                        clickChangeValue(plate)
                                    }}
                                >{plate}</span></Tag>)
                            })
                        }
                    </div>
                )
            },
        },
    ]
    function setList(data, page, pagesize, md5, option) {
        try {
            storage.setItem(JSON.stringify([page, pagesize, md5, option]), JSON.stringify(data))
        } catch (e) {
            if (e.name == 'QuotaExceededError') {
                console.log('超出本地存储限额！');
                //如果历史信息不重要了，可清空后再设置
                clearList(md5, 3)//保留三个
                storage.setItem(JSON.stringify([page, pagesize, md5, option]), JSON.stringify(data))
            }
        }
    }
    function getList(page, pagesize, md5, option) {
        return JSON.parse(storage.getItem(JSON.stringify([page, pagesize, md5, option])))
    }
    function clearList(md5, n = 0) {
        for (let i = 0; i < storage.length - n; i++) {
            let key = storage.key(i)
            if (storage[key].indexOf(md5) > 0) {
                // console.log(key)
                storage.removeItem(key)
            }
        }
    }
    const tableSearch = async (page, pagesize, option) => {
        setIsLoading(true)
        let url = require('@/String.js').server + 'admingetcompanylist'
        async function getlist() {
            try {
                let res = await axios.post(url, { page, pagesize, md5, option, openId: localStorage.getItem('openId') }, { withCredentials: true, })
                const { data } = res
                setData(data.arr)
                setTotal(data.total)
                setmd5(data.md5)
                setList(data, page, pagesize, data.md5, option)
                // console.log(data)
            } catch (e) {
                console.trace({ e })
            }
        }
        setData([])
        setTotal(0)
        await axios.get(require('@/String.js').server + 'admincheckmd5',
            { withCredentials: true, })
            .then(async (res) => {
                const { data } = res
                if (md5 == data.md5) {
                    //没有更新
                    let data = getList(page, pagesize, md5, option)
                    if (!data) {
                        await getlist()
                    } else {
                        setData(data.arr)
                        setTotal(data.total)
                        setmd5(data.md5)
                        setList(data, page, pagesize, md5, option)
                    }
                    // console.log(data)
                } else {
                    //更新了
                    clearList(md5)
                    await getlist()
                }
            })
        setIsLoading(false)
    }
    useEffect(async () => {
        await tableSearch(page, pagesize, option)
    }, [props.admin, page, pagesize])

    const onSearch = async () => {
        pageInit(1, 10)
        setData([])
        await tableSearch(page, pagesize, option)
    }

    return (
        <div style={{ width: '100%' }} className="admin-tablelist-div" >
            <Row style={{ marginLeft: breakpoint ? 0 : '5%' }} >
                <Col sm={8} xs={20}>
                    <Tooltip placement="topRight" title={'search by stock/compony/plates'}>
                        <Input
                            value={searchValue}
                            onChange={(e) => { setSearchValue(e.target.value) }}
                            onPressEnter={async () => {
                                await onSearch()
                            }}
                        />
                    </Tooltip>
                </Col>
                <Col sm={8} xs={4}>
                    <Button icon={<SearchOutlined />}
                        onClick={async () => {
                            await onSearch()
                        }}
                        style={{
                            height: '100%'
                        }}
                    />
                </Col>
            </Row>
            <Table
                columns={columnsx}
                dataSource={data}
                title={() => <span style={{ color: '#aaa' }}>{`共${total}个`}</span>}
                bordered
                className="admin-tablelist-table"
                loading={isLoading}
                scroll
                size="small"
                style={{
                    marginTop: '1rem',
                    maxWidth: 1000,
                    borderTop: '2px double #ac8901',
                    marginLeft: breakpoint ? 0 : '5%',
                }}

                pagination={{
                    total: total || 0,
                    simple: true,
                    onChange: async (page, pagesize) => {
                        pageInit(page, pagesize)
                    },
                    showQuickJumper: true,
                    defaultCurrent: +page + 1
                }}
                rowKey={(row) => { return row.code }}
            >

            </Table>

        </div>)
}
export default AdminStockTable