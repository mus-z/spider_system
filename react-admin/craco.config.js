const CracoLessPlugin = require('craco-less');
const path = require('path')
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');
const pathResolve = pathUrl => path.join(__dirname, pathUrl)
module.exports = {
  webpack: {
    alias: {
      '@@': pathResolve('.'),
      '@': pathResolve('src'),
      // 此处是一个示例，实际可根据各自需求配置
    },
    plugins: [new AntdDayjsWebpackPlugin(), ],
  },

  extensions: ['.js', '.jsx', '.json', '.ts'], //表示在import 文件时文件后缀名可以不写
  babel: {
    plugins: [
      ['import', {
        libraryName: 'antd',
        style: true
      }],
      ['@babel/plugin-proposal-decorators', {
        legacy: true
      }],
    ]
  },
  plugins: [{
    plugin: CracoLessPlugin,
    options: {
      lessLoaderOptions: {
        lessOptions: {
          modifyVars: {
            '@primary-color': '#c7a519'
          },
          javascriptEnabled: true,
        },
      },
    },
  }, ],
};